-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : ven. 06 nov. 2020 à 14:48
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `bddkompai`
--

-- --------------------------------------------------------

--
-- Structure de la table `duration`
--

DROP TABLE IF EXISTS `duration`;
CREATE TABLE IF NOT EXISTS `duration` (
  `id_duration` int(11) NOT NULL AUTO_INCREMENT,
  `serialnumber` varchar(255) NOT NULL,
  `date` date NOT NULL,
  `round` int(11) NOT NULL,
  `battery` int(11) NOT NULL,
  `patrol` int(11) NOT NULL,
  `walk` int(11) NOT NULL,
  PRIMARY KEY (`id_duration`),
  KEY `fk_duration_robot` (`serialnumber`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `duration`
--

INSERT INTO `duration` (`id_duration`, `serialnumber`, `date`, `round`, `battery`, `patrol`, `walk`) VALUES
(4, 'TEST', '2009-05-25', 15, 52, 60, 18),
(5, 'TEST', '2020-11-01', 42, 10, 50, 13),
(6, 'TEST', '2020-11-02', 4, 5, 7, 6),
(8, 'TEST', '2020-11-05', 3, 0, 0, 0),
(14, 'TEST', '2020-11-06', 50, 0, 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `mail`
--

DROP TABLE IF EXISTS `mail`;
CREATE TABLE IF NOT EXISTS `mail` (
  `id_mail` int(11) NOT NULL AUTO_INCREMENT,
  `serialnumber` varchar(255) NOT NULL,
  `mail` varchar(255) NOT NULL,
  PRIMARY KEY (`id_mail`),
  KEY `fk_mail_robot` (`serialnumber`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `mail`
--

INSERT INTO `mail` (`id_mail`, `serialnumber`, `mail`) VALUES
(4, 'TEST', 'laetitia.lerandy@kompai.com');

-- --------------------------------------------------------

--
-- Structure de la table `phonenumber`
--

DROP TABLE IF EXISTS `phonenumber`;
CREATE TABLE IF NOT EXISTS `phonenumber` (
  `id_phonenumber` int(11) NOT NULL AUTO_INCREMENT,
  `serialnumber` varchar(255) NOT NULL,
  `phonenumber` varchar(255) NOT NULL,
  PRIMARY KEY (`id_phonenumber`),
  KEY `fk_phonenumber_robot` (`serialnumber`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `phonenumber`
--

INSERT INTO `phonenumber` (`id_phonenumber`, `serialnumber`, `phonenumber`) VALUES
(2, 'TEST', '+33642517645');

-- --------------------------------------------------------

--
-- Structure de la table `robot`
--

DROP TABLE IF EXISTS `robot`;
CREATE TABLE IF NOT EXISTS `robot` (
  `serialnumber` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `mailrobot` varchar(255) NOT NULL,
  `maildata` varchar(255) NOT NULL,
  `tokenmail` varchar(255) NOT NULL,
  `tokendata` varchar(255) NOT NULL,
  `langage` varchar(255) NOT NULL,
  `localhost` varchar(255) NOT NULL,
  `allowspeech` tinyint(1) NOT NULL DEFAULT '1',
  `idpatrol1` int(11) NOT NULL,
  `idpatrol2` int(11) NOT NULL,
  `idround1` int(11) NOT NULL,
  `idround2` int(11) NOT NULL,
  PRIMARY KEY (`serialnumber`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `robot`
--

INSERT INTO `robot` (`serialnumber`, `name`, `mailrobot`, `maildata`, `tokenmail`, `tokendata`, `langage`, `localhost`, `allowspeech`, `idpatrol1`, `idpatrol2`, `idround1`, `idround2`) VALUES
('TEST', 'robot', 'kompai.izarbel@gmail.com', 'data@kompai.com', '9a90c0f4-ac80-4e37-9644-7aab5088bbe9', 'baa54d7d-10f4-4a29-9763-1f58e6cf29bf', 'fr-FR', '192.168.1.2:7007', 1, 2, 3, 2, 3);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `duration`
--
ALTER TABLE `duration`
  ADD CONSTRAINT `fk_duration_robot` FOREIGN KEY (`serialnumber`) REFERENCES `robot` (`serialnumber`) ON DELETE CASCADE;

--
-- Contraintes pour la table `mail`
--
ALTER TABLE `mail`
  ADD CONSTRAINT `fk_mail_robot` FOREIGN KEY (`serialnumber`) REFERENCES `robot` (`serialnumber`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Contraintes pour la table `phonenumber`
--
ALTER TABLE `phonenumber`
  ADD CONSTRAINT `fk_phonenumber_robot` FOREIGN KEY (`serialnumber`) REFERENCES `robot` (`serialnumber`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
