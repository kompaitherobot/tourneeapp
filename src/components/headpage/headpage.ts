import { Component, Input, OnInit } from '@angular/core';
import { HomePage } from '../../pages/home/home';
import { TutoPage } from '../../pages/tuto/tuto';
import { NavController } from 'ionic-angular';
import { ApiService } from '../../services/api.service';
import { AlertService } from '../../services/alert.service';
import { PopupService } from '../../services/popup.service';
import { SpeechService } from '../../services/speech.service';
import { ParamService } from '../../services/param.service';
import { ParamPage } from '../../pages/param/param';
import { MailPage } from '../../pages/mail/mail';
import { ToastController } from 'ionic-angular';
import { DomSanitizer } from '@angular/platform-browser';
import { AutoLaunchPage } from '../../pages/autolaunch/autolaunch';

@Component({
  selector: 'headpage',
  templateUrl: 'headpage.html'
})

export class HeadpageComponent implements OnInit {
  homePage = HomePage;
  tutoPage = TutoPage;
  paramPage = ParamPage;
  mailPage = MailPage;
  autolaunchPage = AutoLaunchPage;
  lowbattery: any;
  cpt_battery: number;
  cpt_open: number;
  microon: boolean;
  cptsos: number;
  update: any;
  optiondateofday: any;
  textBoutonHeader: string; // bouton 's text (Return / Exit), to the left of the header, depends on the page you are on
  textHeadBand: string;
  volumeState: number; //0 mute,  1 low,  2 hight
  source: any;

  @Input() pageName: string;
  constructor(public speech: SpeechService, public toastCtrl: ToastController, public navCtrl: NavController, public param: ParamService, public api: ApiService, public alert: AlertService, public popup: PopupService, public sanitizer: DomSanitizer) {
    this.optiondateofday = { weekday: 'long', day: 'numeric', month: 'long' };
    this.microon = false;
    this.cptsos = 0;
    this.cpt_battery = 0;
    this.cpt_open = 0;

    if (this.param.allowspeech == 1) {
      this.volumeState = 1;
    } else {
      this.volumeState = 0;
    }
  }

  ionViewWillLeave() {
    clearInterval(this.update);
    clearInterval(this.lowbattery);
    clearInterval(this.source);
  }


  ngOnDestroy(): void {
    clearInterval(this.update);
    clearInterval(this.lowbattery);
    clearInterval(this.source);
  }

  SOSsend() {
    if (this.cptsos === 1) {
      this.alert.SOS_c();
      this.alert.SOS(this.api.mailAddInformationBasic());
    }
    if (this.api.sos_pressed) {
      this.cptsos += 1;
    }
    if (this.cptsos === 15) {
      this.api.sos_pressed = false;
      this.cptsos = 0;
    }
  }

  updateBattery() {

    if (this.api.statusRobot === 2) {
      // battery logo will not apppear
      this.api.batteryState = 10;
    }
    else if (this.api.battery_status.status < 2) {
      //charging
      this.api.batteryState = 4;

    } else if (
      this.api.battery_status.status === 3 ||
      this.api.battery_status.remaining <= this.param.battery.critical
    ) {
      //critical
      this.api.batteryState = 0;
    } else if (this.api.battery_status.remaining > this.param.battery.high) {
      //hight
      this.api.batteryState = 3;
    } else if (this.api.battery_status.remaining > this.param.battery.low) {
      //mean
      this.api.batteryState = 2;
    } else if (this.api.battery_status.remaining <= this.param.battery.low) {
      //low
      this.api.batteryState = 1;
    } else {
      // battery logo will not apppear
      this.api.batteryState = 10;
    }
  }


  onTouchWifi(ev) {
    ev.preventDefault();
    if (!this.api.wifiok) {
      this.okToast(this.param.datatext.nointernet);
    } else {
      this.okToast(this.param.datatext.AlertConnectedToI);
    }
  }

  ngOnInit() {
    this.popup.onSomethingHappened1(this.accessparam.bind(this));
    this.popup.onSomethingHappened3(this.accessautolaunch.bind(this));
    // bouton 's text (Return / Exit), to the left of the header, depends on the page you are on
    if (this.pageName === "home") {
      this.update = setInterval(() => this.getUpdate(), 1000);//update hour, battery every 1/2 secondes
      this.lowbattery = setInterval(() => this.monitoringBattery(), 60000);
      this.source = setInterval(() => this.api.jetsonOK(), 2000);
      this.textBoutonHeader = this.param.datatext.quit;
      this.textHeadBand = this.param.datatext.round;
    } else if (this.pageName === "param") {
      this.textBoutonHeader = this.param.datatext.return;
      this.textHeadBand = this.param.datatext.param;
    } else if (this.pageName === "sms") {
      this.textBoutonHeader = this.param.datatext.return;
      this.textHeadBand = this.param.datatext.receivesms;
    } else if (this.pageName === "mail") {
      this.textBoutonHeader = this.param.datatext.return;
      this.textHeadBand = this.param.datatext.receivemail;
    } else if (this.pageName === "langue") {
      this.textBoutonHeader = this.param.datatext.return;
      this.textHeadBand = this.param.datatext.changelangage;
    }
    else if (this.pageName === "annonce") {
      this.textBoutonHeader = this.param.datatext.return;
      this.textHeadBand = this.param.datatext.announceEdit;
    }
    else if (this.pageName === "password") {
      this.textBoutonHeader = this.param.datatext.return;
      this.textHeadBand = this.param.datatext.editpswd;
    } else if (this.pageName === "behaviour") {
      this.textBoutonHeader = this.param.datatext.return;
      this.textHeadBand = this.param.datatext.robotBehaviour;
    } else if (this.pageName === "autolaunch") {
      this.textBoutonHeader = this.param.datatext.return;
      this.textHeadBand = this.param.datatext.autolaunchHeadPage;
    } else if (this.pageName === "radio") {
      this.textBoutonHeader = this.param.datatext.return;
      this.textHeadBand = this.param.datatext.radioHeadPage;
    }
    else {
      this.textBoutonHeader = this.param.datatext.return;
      this.textHeadBand = this.param.datatext.tutorial;
    }
  }

  accessparam() {
    this.navCtrl.push(this.paramPage);
  }

  accessautolaunch() {
    this.navCtrl.push(this.autolaunchPage);
  }

  onTouchAutoLaunch(ev) {
    ev.preventDefault();
    if (this.pageName === "home")
    this.popup.askpswd("autolaunch");
    else if (this.pageName == "autolaunch")
      console.log("already");
    else
      this.accessautolaunch();
  }

  onTouchParam(ev) {
    ev.preventDefault();
    if (!this.api.roundActive && !this.api.towardDocking) {//robot is not moving
      if (this.pageName === "home") {
        this.popup.askpswd("param");
      } else if (this.pageName == "autolaunch")
        this.accessparam();
      else {
        //this.navCtrl.push(this.paramPage);
        console.log("already");;
      }
    }
  }

  onTouchStatus(ev) {
    ev.preventDefault();
    if (this.api.statusRobot === 2)// if status red
    {
      this.popup.statusRedPresent();
    } else {
      this.okToast(this.param.datatext.statusGreenPresent_message);
    }


  }

  okToast(m: string) {
    const toast = this.toastCtrl.create({
      message: m,
      duration: 3000,
      position: 'middle',
      cssClass: "toastok"
    });
    toast.present();
  }

  monitoringBattery() {

    if (this.api.batteryState === 0) {

      this.popup.lowBattery();
      this.alert.lowBattery(this.api.mailAddInformationBasic());
      this.alert.Battery_c();
      if (!this.api.towardDocking) {

        this.api.pauseHttp(false);
        this.api.towardDocking = true;
        setTimeout(
          () => {
            this.api.reachHttp("docking");
          }, 2000
        );
      }
      clearInterval(this.lowbattery);
    }
  }

  onTouchVolume(ev) {
    ev.preventDefault();
    this.speech.synth.cancel();
    if (this.volumeState === 0) {
      this.volumeState = 1;
      this.param.allowspeech = 1;

    } else {
      this.volumeState = 0;
      this.param.allowspeech = 0;
    }
  }

  onTouchBattery(ev) {
    ev.preventDefault();
    if (!this.api.roundActive && !this.api.towardDocking) {
      if (this.pageName === "home") {
        if (this.api.docking_status.status != 3) { // if not on docking

          if (this.api.batteryState === 0) {
            this.popup.lowBattery();
          }
          else {
            this.popup.goDockingConfirm();
          }

        }
        if (this.api.docking_status.status === 3) { //if on docking
          this.popup.leaveDockingConfirm();
        }
      } else {
        this.clicOnMenu();
      }
    } else {
      this.okToast(this.param.datatext.battery + " : " + this.api.battery_status.remaining + "%")
    }
  }


  getUpdate() {
    //update hour, battery
    this.SOSsend();
    this.updateBattery();
    this.updateBand();
    var now = new Date().toLocaleString('fr-FR', { hour: 'numeric', minute: 'numeric' });

    this.api.hour = now;
    //console.log(this.batteryState);
    if (this.api.statusRobot === 0 && this.api.batteryState === 10) { // importante to know if the application has run correctly
      this.cpt_battery += 1;
    }
    else {
      this.cpt_battery = 0;
    }
    if (this.cpt_battery > 10) {
      this.api.statusRobot = 2;
    }

  }

  updateBand() {
    if (this.api.is_blocked || this.api.is_high) {
      this.textHeadBand = this.param.datatext.errorBlocked_title.toUpperCase();
    }
    else if (this.api.towardDocking) {
      this.textHeadBand = this.param.datatext.godocking;
    }

    else if (this.api.roundActive) {
      this.textHeadBand = this.param.datatext.roundInProgress;
    }
    else if (this.pageName === "home") {
      this.textHeadBand = this.param.datatext.round;
    }
  }


  onTouchHelp(ev) {
    ev.preventDefault();
    if (!this.api.roundActive && !this.api.towardDocking) { //robot is not moving
      if (!(this.pageName === "tuto")) {
        this.alert.displayTuto(this.api.mailAddInformationBasic());
        this.navCtrl.push(this.tutoPage);
      }

    }
  }

  onquit(ev) {
    ev.preventDefault();
    this.clicOnMenu();
  }

  clicOnMenu() {
    if (this.pageName === 'tuto' || this.pageName === 'param' || (this.pageName === 'autolaunch' && !this.api.addMission)) {
      this.navCtrl.popToRoot();
    }
    else if (this.pageName === 'sms' || this.pageName === 'mail' || this.pageName === 'langue' || this.pageName === 'annonce' || this.pageName === "password" || this.pageName === "behaviour" || this.pageName === 'radio') {
      this.navCtrl.pop();
    }
    else if (this.pageName === 'autolaunch' && this.api.addMission)
      this.api.addMission = !this.api.addMission;
    else {

      if (this.api.roundActive || this.api.towardDocking) {
        this.api.abortNavHttp();
        this.api.roundActive = false;
        this.api.towardDocking = false;
        this.popup.quitConfirm();
      }
      else {
        this.api.close_app = true;
        this.alert.appClosed(this.api.mailAddInformationBasic());
        //stop the round and quit the app
        this.api.abortHttp();
        this.api.deleteEyesHttp(23);
        this.api.startPeopleDetection(false);
        this.api.startMaskDetection(false);
        setTimeout(
          () => {
            window.close();
          }, 1000
        );
      }


    }

  }
}
