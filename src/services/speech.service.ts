import { ParamService } from "./param.service";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable()
export class SpeechService {
  dataweather: any;
  msg: any;
  synth:any;
  constructor(private httpClient: HttpClient, public param: ParamService) {
    this.msg = new SpeechSynthesisUtterance();
    this.msg.volume = parseFloat("1");
    this.msg.rate = parseFloat("0.9");
    this.msg.pitch = parseFloat("1");
    this.msg.localService = true;
    this.synth=window.speechSynthesis;
    //this.msg.voice = speechSynthesis.getVoices().filter(function(voice) { return voice.name ; })[10];
  }

  getVoice() {
    if(this.param.robot.os=="linux"){
      this.msg.voice = speechSynthesis
      .getVoices()
      .filter(
        (voice) =>
          voice.name.includes("pico-generic") && voice.name.includes(this.param.langage)
      )[0];
    }else{
       
    var voices = speechSynthesis
    .getVoices()
    .filter(
      (voice) =>
        voice.localService == true && voice.lang == this.param.langage
    );

    if(voices.length > 1)
    {
      this.msg.voice = voices[1];
    }
    else{
      this.msg.voice = voices[0];
    }}
   
  }

  getDataWeather() {
    this.httpClient
      .get(
        "http://api.openweathermap.org/data/2.5/weather?lat=" +
          this.param.gpslat +
          "&lon=" +
          this.param.gpslong +
          "&units=metric&lang=" +
          this.param.weatherlang +
          "&appid=" +
          this.param.weatherapikey
      )
      .subscribe(
        (data) => {
          //console.log(data);
          this.dataweather = data;
          //console.log(this.dataweather.weather[0].description);
          //console.log(Math.round(this.dataweather.main.temp));
          this.speak(
            this.param.datatext.announce3 +
              this.dataweather.weather[0].description +
              this.param.datatext.announce4 +
              Math.round(this.dataweather.main.temp) +
              this.param.datatext.announce5
          );
        },
        (err) => {
          console.log(err);
        }
      );
  }

  // Create a new utterance for the specified text and add it to
  // the queue.
  speak(text: string) {
    console.log(text);
    this.synth.cancel();
    this.msg.lang = this.param.langage;

    this.getVoice();
    // Create a new instance of SpeechSynthesisUtterance.
    // Set the text.
    //console.log(this.msg.name);
    this.msg.text = text;

    //console.log(this.msg);
    // Queue this utterance.
    this.synth.speak(this.msg);
  }
}
