// the parameter script
import frtext from "../langage/fr-FR.json";
import entext from "../langage/en-GB.json";
import estext from "../langage/es-ES.json";
import detext from "../langage/de-DE.json";
import grtext from "../langage/el-GR.json";
import ittext from "../langage/it-IT.json";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { DomSanitizer } from "@angular/platform-browser";
import { cloneDeep } from 'lodash';

@Injectable()
export class ParamService {
  localhost: string;
  robotmail: string;
  datamail: string;
  langage: string;
  phonenumberlist: string[];
  maillist: string[];
  datamaillist: string[];
  serialnumber: string;
  gpslat: string;
  gpslong: string;
  weatherapikey: string;
  weatherlang: string;
  mailrobotpassw: string;
  maildatapassw: string;
  allowspeech: number;
  sayweather: number;
  sayhour: number;
  name: string;
  speak_freq: number;
  allowskip: boolean;
  datatext: any;
  robot: any;
  paramround:any;
  announcement: any = {};
  duration: any;
  battery: any;
  tableduration: any = {};
  tablerobot: any = {};
  tableparamround:any={};
  tablereservation: any = {};
  mail: any = {};
  phone: any = {};
  tableannonce: any = {};
  date: any;
  currentduration: any = {};
  cpt: number;

  sendduration: boolean;
  playlist: any = {};
  source: any;
  autolaunch: boolean = false;
  reservations: any;
  missionsList: any = [];
  missionsDisplay: any;
  missionsDisplayToday: any;
  tablepatrol: any = {};
  patrolInfo: any = {};
  durationNS: any = {};
  tabledurationNS: any = {};

  radiolist: any = {};
  radio: any = {};
  musiclist: any = {};
  musicfolderlist: string[];
  musicfolderlistJ: any = {};
  baseUrl = "http://localhost/Music/";
  urlLocal: string;

  constructor(private httpClient: HttpClient, public sanitizer: DomSanitizer) {
    this.maillist = [];
    this.phonenumberlist = [];
    this.cpt = 0;
    this.sendduration = false;

  }

  updatePatrol() {
    this.tablepatrol.action = "update";
    this.tablepatrol.patrol_id = this.patrolInfo.patrol_id;
    this.tablepatrol.round_fall_detection = this.patrolInfo.round_fall_detection;
    this.tablepatrol.round_mask_detection = this.patrolInfo.round_mask_detection;
    this.httpClient
      .post(
        "http://localhost/ionicDB/updatepatrolround.php",
        JSON.stringify(this.tablepatrol)
      )
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  getDataPatrol() {
    this.httpClient
      .get("http://localhost/ionicDB/getpatrol.php")
      .subscribe(
        (data) => {
          //console.log(data);
          this.patrolInfo = data[0];

        },
        (err) => {
          console.log(err);
        }
      );
  }

  getreservations() {

    this.httpClient.get("http://localhost/ionicDB/autolaunch/getreservations.php").subscribe(
      (data) => {
        this.reservations = data[0];
        console.log(this.reservations);
      },
      (err) => {
        console.log(err);
      }
    );
  }

  getDataRobot() {
    this.httpClient.get("http://localhost/ionicDB/getrobot.php").subscribe(
      (data) => {
        //console.log(data);
        this.robot = data[0];
      },
      (err) => {
        console.log(err);
      }
    );
  }

  getParamRound(){
    this.httpClient.get("http://localhost/ionicDB/round/getparamround.php").subscribe(
      (data) => {
        //console.log(data);
        this.paramround = data[0];
      },
      (err) => {
        console.log(err);
      }
    );
  }

  getDataAnnouncement() {
    this.httpClient
      .get("http://localhost/ionicDB/getannouncement.php")
      .subscribe(
        (data) => {
          //console.log("annonces");
          //console.log(data);
          this.announcement = data;
        },
        (err) => {
          console.log(err);
        }
      );
  }

  getDataPlaylist() {
    this.httpClient.get("http://localhost/ionicDB/getplaylist.php").subscribe(
      (data) => {
        //console.log(data);
        this.playlist = data;
      },
      (err) => {
        console.log(err);
      }
    );
  }



  getPhone() {
    this.httpClient.get("http://localhost/ionicDB/getphone.php").subscribe(
      (data) => {
        if (this.phonenumberlist.length === 0) {
          for (let value in data) {
            this.phonenumberlist.push(data[value].phonenumber);
          }
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }

  getDataReservationPatrol(roundList: any) {
    this.httpClient
      .get("http://localhost/ionicDB/getreservationroundapp.php")
      .subscribe(
        (data) => {
          console.log(data);
          this.missionsList = data;
          this.missionsList.forEach(mission => {
            var date = mission.date
            mission.date = new Date(date.substring(0, 10) + "T" + date.substring(11, date.length));
          });
          this.updateDisplayMission(roundList);
        },
        (err) => {
          console.log(err);
        }
      )
  }

  addMail(m: string) {
    this.mail.action = "insert";
    this.mail.mail = m;
    this.mail.serialnumber = this.serialnumber;
    this.httpClient
      .post("http://localhost/ionicDB/addmail.php", JSON.stringify(this.mail))
      .subscribe(
        (data) => {
          //console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  addDuration() {
    this.tableduration.action = "insert";
    this.tableduration.serialnumber = this.serialnumber;
    this.tableduration.round = this.currentduration.round;
    this.tableduration.battery = this.currentduration.battery;
    this.tableduration.walk = this.currentduration.walk;
    this.tableduration.patrol = this.currentduration.patrol;
    this.tableduration.date = this.currentduration.date;
    this.httpClient
      .post(
        "http://localhost/ionicDB/addduration.php",
        JSON.stringify(this.tableduration)
      )
      .subscribe(
        (data) => {
          //console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  getDurationNS() {
    this.httpClient.get("http://localhost/ionicDB/duration/getduration.php").subscribe(
      (data) => {

        this.durationNS = data;
        this.duration = data;
        console.log(data);
        console.log(this.durationNS.length);
      },
      (err) => {
        console.log(err);
      }
    );
  }

  updateDurationNS(id: number) {


    this.tabledurationNS.action = "update";
    this.tabledurationNS.id_duration = id;
    this.httpClient
      .post(
        "http://localhost/ionicDB/duration/updateduration.php",
        JSON.stringify(this.tabledurationNS)
      )
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }
  addMission(mission: any) {
    this.tablereservation.action = "insert";
    this.tablereservation.date = mission.date.substring(0, 10) + " " + mission.date.substring(11, 19);
    this.tablereservation.idRound = mission.idRound;
    this.tablereservation.type = mission.type;
    this.tablereservation.tpsReserv = mission.tpsReserv;
    this.tablereservation.idApp = 2;
    this.httpClient
      .post(
        "http://localhost/ionicDB/addreservation.php",
        JSON.stringify(this.tablereservation)
      )
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  updateDuration() {
    this.tableduration.action = "update";
    this.tableduration.round = this.currentduration.round;
    this.tableduration.date = this.currentduration.date;
    this.httpClient
      .post(
        "http://localhost/ionicDB/duration/updatedurationround.php",
        JSON.stringify(this.tableduration)
      )
      .subscribe(
        (data) => {
          //console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  updateAnnonce(m: any) {
    console.log("update annonce");
    this.tableannonce.action = "update";
    this.tableannonce.speech = m.speech;
    this.tableannonce.activated = m.activated;
    this.tableannonce.id_annonce = m.id_annonce;
    this.httpClient
      .post(
        "http://localhost/ionicDB/updateannouncement.php",
        JSON.stringify(this.tableannonce)
      )
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  addAnnonce(m: string) {
    this.tableannonce.action = "insert";
    this.tableannonce.speech = m;
    this.tableannonce.activated = 1;
    this.tableannonce.serialnumber = this.serialnumber;
    this.httpClient
      .post(
        "http://localhost/ionicDB/addannouncement.php",
        JSON.stringify(this.tableannonce)
      )
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }
  updateRobot() {
    this.tablerobot.action = "update";
    this.tablerobot.langage = this.robot.langage;
    this.tablerobot.serialnumber = this.serialnumber;
    this.tablerobot.name = this.name;
    this.tablerobot.send_pic = this.robot.send_pic;
    this.tablerobot.password=this.robot.password;
    this.httpClient
      .post(
        "http://localhost/ionicDB/updaterobotpassword.php",
        JSON.stringify(this.tablerobot)
      )
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }


  updateRoundParam() {
    this.tableparamround.action = "update";
    this.tableparamround.sayhour = this.sayhour;
    this.tableparamround.sayweather = this.sayweather;
    this.tableparamround.allowspeech = this.robot.allowspeech;
    this.tableparamround.speak_freq = this.speak_freq;

    this.httpClient
      .post(
        "http://localhost/ionicDB/round/updateparamround.php",
        JSON.stringify(this.tableparamround)
      )
      .subscribe(
        (data) => {
          //console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  deleteMail(m: string) {
    this.mail.action = "delete";
    this.mail.mail = m;
    this.httpClient
      .post(
        "http://localhost/ionicDB/deletemail.php",
        JSON.stringify(this.mail)
      )
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  addPhone(m: string) {
    this.phone.action = "insert";
    this.phone.phonenumber = m;
    this.phone.serialnumber = this.serialnumber;
    this.httpClient
      .post("http://localhost/ionicDB/addphone.php", JSON.stringify(this.phone))
      .subscribe(
        (data) => {
          //console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  deletePhone(m: string) {
    this.phone.action = "delete";
    this.phone.phonenumber = m;
    this.httpClient
      .post(
        "http://localhost/ionicDB/deletephone.php",
        JSON.stringify(this.phone)
      )
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  deleteAnnonce(id: number) {
    this.tableannonce.action = "delete";
    this.tableannonce.id_annonce = id;
    this.httpClient
      .post(
        "http://localhost/ionicDB/deleteannouncement.php",
        JSON.stringify(this.tableannonce)
      )
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  deleteMission(id: number) {
    this.tablereservation.action = "delete"
    this.tablereservation.id = id;
    this.httpClient
      .post(
        "http://localhost/ionicDB/deletereservation.php",
        JSON.stringify(this.tablereservation)
      )
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  updateDisplayMission(roundList: any) {
    this.missionsDisplay = cloneDeep(this.missionsList);
    this.missionsDisplay.forEach(mission => {
      mission.name = this.getRoundName(mission.idRound, roundList);
      if (mission.name == undefined) {
        this.deleteMission(mission.id);
        this.getDataReservationPatrol(roundList);
      } else {
        var date = mission.date.toLocaleDateString(this.langage);
        var time = mission.date.toLocaleTimeString(this.langage, { hour: '2-digit', minute: '2-digit' });
        mission.date = date;
        mission.time = time;
        mission.type = this.getReservationType(mission);
      }
    });
    this.missionsDisplayToday = this.missionsDisplay.filter(
      (mission) => mission.date == new Date().toLocaleDateString(this.langage)
    );
    console.log(this.missionsDisplay);
  }

  updatelocation() {
    this.tablerobot.action = "update";
    this.tablerobot.gpslat = this.paramround.gpslat;
    this.tablerobot.gpslong = this.paramround.gpslong;
   
    this.httpClient
      .post(
        "http://localhost/ionicDB/round/updatelocation.php",
        JSON.stringify(this.tablerobot)
      )
      .subscribe(
        (data) => {
          //console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  getMail() {
    this.httpClient.get("http://localhost/ionicDB/getmail.php").subscribe(
      (data) => {
        if (this.maillist.length === 0) {
          for (let value in data) {
            this.maillist.push(data[value].mail);
          }
        }
      },
      (err) => {
        console.log(err);
      }
    );
  }

  getBattery() {
    this.httpClient.get("http://localhost/ionicDB/getbattery.php").subscribe(
      (data) => {
        this.battery = data[0];
      },
      (err) => {
        console.log(err);
      }
    );
  }

  fillData() {
    this.name = this.robot.name;
    this.allowspeech = this.robot.allowspeech;
    this.langage = this.robot.langage;
    this.maildatapassw = atob(this.robot.maildatapass);
    this.mailrobotpassw = atob(this.robot.mailrobotpass);
    this.localhost = this.robot.localhost;
    this.serialnumber = this.robot.serialnumber;
    this.robotmail = this.robot.mailrobot;
    this.datamail = this.robot.maildata;
    this.datamaillist = ["data@kompai.com"];
    this.gpslat = this.paramround.gpslat;
    this.gpslong = this.paramround.gpslong;
    this.weatherapikey = this.paramround.weatherapikey;
    this.speak_freq = this.paramround.speak_freq;
    this.allowskip = this.robot.allowskip;
    this.weatherlang = this.langage.substring(0, 2);
    this.sayweather = this.paramround.sayweather;
    this.sayhour = this.paramround.sayhour;

    if (this.langage === "fr-FR") {
      this.datatext = frtext;
    } else if (this.langage === "en-GB") {
      this.datatext = entext;
    } else if (this.langage === "es-ES") {
      this.datatext = estext;
    } else if (this.langage === "de-DE") {
      this.datatext = detext;
    } else if (this.langage === "el-GR") {
      this.datatext = grtext;
    } else if (this.langage === "it-IT") {
      this.datatext = ittext;
    }

    this.source = this.sanitizer.bypassSecurityTrustResourceUrl(this.datatext.URL_tourneeapp)

    if (this.duration.length > 0) {
      if (
        this.duration[this.duration.length - 1].date ===
        new Date().toLocaleDateString("fr-CA")
      ) {
        this.currentduration.date = this.duration[this.duration.length - 1].date;
        this.currentduration.round = this.duration[
          this.duration.length - 1
        ].round;
        this.currentduration.battery = this.duration[
          this.duration.length - 1
        ].battery;
        this.currentduration.patrol = this.duration[
          this.duration.length - 1
        ].patrol;
        this.currentduration.walk = this.duration[this.duration.length - 1].walk;
        this.currentduration.toolbox = this.duration[this.duration.length - 1].toolbox;
        this.currentduration.logistic = this.duration[this.duration.length - 1].logistic;
      } else {

        if (!this.sendduration) {
          this.sendduration = true;
          this.init_currentduration();
          this.addDuration();
        }

      }
    } else {

      if (!this.sendduration) {
        this.sendduration = true;
        this.init_currentduration();
        this.addDuration();
      }

    }
  }

  cptDuration() {
    this.autolaunch = false;
    if (this.currentduration.date === new Date().toLocaleDateString("fr-CA")) {
      this.cpt = parseInt(this.currentduration.round);
      this.currentduration.round = this.cpt + 2;
      this.updateDuration();
    } else {
      this.init_currentduration();
      this.addDuration();
    }
  }

  getRoundName(id: string, roundList: any) {
    var round = roundList.filter(
      (round) => round.Id == parseInt(id)
    );
    var name;
    if (round.length > 0)
      name = round[0].Name;
    else {
      name = undefined;
    }
    return name;
  }

  getReservationType(round: any) {
    var type;
    if (round.type == 1)
      type = "x 1";
    else if (round.type == 2)
      type = "x " + this.datatext.dailyLetter;
    else
      type = "x " + this.datatext.weeklyLetter;
    return type;
  }

  init_currentduration() {
    this.currentduration.round = 0;
    this.currentduration.battery = 0;
    this.currentduration.patrol = 0;
    this.currentduration.walk = 0;
    this.currentduration.toolbox = 0;
    this.currentduration.logistic = 0;
    this.currentduration.date = new Date().toLocaleDateString("fr-CA");
  }

  getradio() {
    this.httpClient.get("http://localhost/ionicDB/radio/getRadio.php").subscribe(
      (data) => {
        //console.log(data)
        this.radiolist = data;
      },
      (err) => {
        console.log(err);
      }
    );
  }

  deleteRadio(idRadio: number) {
    this.radio.action = "delete";
    this.radio.idRadio = idRadio;
    this.httpClient
      .post(
        "http://localhost/ionicDB/radio/deleteRadio.php",
        JSON.stringify(this.radio)
      )
      .subscribe(
        (data) => {
          console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  addRadio(nameRadio: string, urlRadio: string) {
    this.radio.action = "insert";
    this.radio.nameRadio = nameRadio;
    this.radio.urlRadio = urlRadio;
    this.httpClient
      .post("http://localhost/ionicDB/radio/addRadio.php", JSON.stringify(this.radio))
      .subscribe(
        (data) => {
          //console.log(data);
        },
        (err) => {
          console.log(err);
        }
      );
  }

  getMusicFolders() {
    this.httpClient.get(this.baseUrl, { responseType: 'text' }).subscribe(
      (data) => {
        const parser = new DOMParser();
        const htmlDocument = parser.parseFromString(data, 'text/html');
        const folderElements = htmlDocument.querySelectorAll('a');

        this.musicfolderlist = Array.from(folderElements)
          .map((element) => element.textContent.trim())
          .filter((name) => name.endsWith('/'))
          .map((name) => name.slice(0, -1));

      },
      (err) => {
        console.log(err);
      }
    );
  }

  getMusic(folderName: string) {
    this.urlLocal = this.baseUrl +folderName+"/";
  
    this.httpClient.get(this.urlLocal, { responseType: 'text' }).subscribe(
      (data) => {
        const parser = new DOMParser();
        const htmlDocument = parser.parseFromString(data, 'text/html');
        const folderElements = htmlDocument.querySelectorAll('a');
  
        this.musiclist = Array.from(folderElements)
          .map((element) => element.getAttribute('href'))
          .filter((name) => name.endsWith('.mp3'))
          .map((name) => name.slice(0, -4));
      },
      (err) => {
        console.log(err);
      }
    );
  }
  
}