import { Component, OnInit, ViewChild } from '@angular/core';
import { ParamService } from '../../services/param.service';
import {ToastController} from 'ionic-angular';
import { SpeechService } from '../../services/speech.service';
import { LoadingController } from 'ionic-angular';
import { Select, Content } from 'ionic-angular';

@Component({
  selector: 'page-annonce',
  templateUrl: 'annonce.html'
})

export class AnnoncePage implements OnInit {
  value:string="";
  selectFreq:any;
  SelectFreq:Number;
  loading:any;
  @ViewChild("select1") select1: Select;
  @ViewChild("content1") content1: Content;


  ngOnInit() {

  }

  constructor(public loadingCtrl:LoadingController,public param:ParamService, public speech:SpeechService, public toastCtrl: ToastController ) {
    this.selectFreq = {
      title: this.param.datatext.freqAnnounce,

    };

    this.SelectFreq=this.param.speak_freq;
   
            
  } 

  errorToast(m:string){
    const toast=this.toastCtrl.create({
      message:m,
      duration:3000,
      position: 'middle',
      cssClass:"toast"
    });
    toast.present();
  }

   
  onFreqChange(event){
   
    this.param.speak_freq=event;
    this.param.updateRoundParam();
    
   
  }
  
  okToast(m: string){
    const toast=this.toastCtrl.create({
      message:m,
      duration:3000,
      position: 'middle',
      cssClass:"toastok"
    });
    toast.present();
  }

  onToggleWeather(){
    if(this.param.sayweather==1){
      this.param.sayweather=0;
    }else{
      this.param.sayweather=1;
    }
    
    this.param.updateRoundParam();

   
  }

  showLoading() {
    if(!this.loading){
        this.loading = this.loadingCtrl.create({
            content: this.param.datatext.addLoading,
        });
        this.loading.present();
    }
}

  onToggleHour(){
    if(this.param.sayhour==1){
      this.param.sayhour=0;
    }else{
      this.param.sayhour=1;
    }
    this.param.updateRoundParam();
    
  }

  onToggleAnnonce(annonce:any){
    const index: number = this.param.announcement.indexOf(annonce);
    if(this.param.announcement[index].activated==1){
      this.param.announcement[index].activated=0;
    }else{
      this.param.announcement[index].activated=1;
    }

    console.log(this.param.announcement[index]);
    this.param.updateAnnonce(this.param.announcement[index]);
    
  }

  removeAnnonce(ev,annonce:any){
    ev.preventDefault();
      this.param.deleteAnnonce(annonce.id_annonce);
      const index: number = this.param.announcement.indexOf(annonce);
      if (index !== -1) {
        this.param.announcement.splice(index, 1);
        this.okToast(this.param.datatext.mailRemove);
  }

  }

 is_a_text(){
  
  return this.value.length>2;
 }

 sayAnnonce(ev)
 {
   ev.preventDefault();
  this.value=this.value.replace(new RegExp("[<>&$§µ£#*_|`¤~(){}°²@%/]", "g"),'');
  this.value=this.value.replace(new RegExp("[\"]", "g"),'');
  this.value=this.value.replace(new RegExp(/\\/, "g"),'');
  this.value=this.value.replace(new RegExp(/[\[\]]/, "g"),'');
  console.log("sayannonce");
  this.speech.speak(this.value);

 }

 dismissLoading(){
  if(this.loading){
      this.loading.dismiss();
      this.loading=null;
      
  }
}

  addAnnonce(ev)
  {
    ev.preventDefault();
    this.showLoading();
    //this.value=this.value.replace(new RegExp("[<>&$µ£#~`¤|()-_=+}{/*§}°²@%€]", "g"),'');
    this.value=this.value.replace(new RegExp("[<>&$§µ£#*_|`¤~(){}°²@%/]", "g"),'');
    this.value=this.value.replace(new RegExp("[\"]", "g"),'');
    this.value=this.value.replace(new RegExp(/\\/, "g"),'');
    this.value=this.value.replace(new RegExp(/[\[\]]/, "g"),'');
    this.param.addAnnonce(this.value);
    
    setTimeout(
      () => {
        this.param.getDataAnnouncement();
        this.dismissLoading();
        this.value='';
        this.okToast(this.param.datatext.addDone);
        setTimeout(() => {
          this.content1.scrollToBottom();
        }, 300
        );
      }, 3000
    );
  }

  editAnnonce(ev,annonce:any){
    ev.preventDefault()
  this.value=annonce.speech;
  this.sayAnnonce(ev);
  }

  onSliderRelease(ev, id: Select){
    ev.preventDefault();
    id.open();
  }


}