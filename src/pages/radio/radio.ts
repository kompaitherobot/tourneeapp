import { Component, OnInit, ViewChild } from '@angular/core';
import { ParamService } from '../../services/param.service';
import {ToastController, Content} from 'ionic-angular';
import { PopupService } from '../../services/popup.service';

@Component({
  selector: 'page-radio',
  templateUrl: 'radio.html'
})

export class RadioPage implements OnInit {

 
  ngOnInit() {
    
  }

  inputname:string;
  inputurl:string;
  @ViewChild("content1") content1: Content;

  constructor(public param:ParamService, public popup: PopupService,public toastCtrl: ToastController ) {

    this.inputurl="";
    this.inputname="";
    
  } 

  errorToast(m: string){
    const toast=this.toastCtrl.create({
      message:m,
      duration:3000,
      position: 'middle',
      cssClass:"toast"
    });
    toast.present();
  }

  okToast(m: string){
    const toast=this.toastCtrl.create({
      message:m,
      duration:3000,
      position: 'middle',
      cssClass:"toastok"
    });
    toast.present();
  }


  removeRadio(ev,m:any){
    ev.preventDefault();
    this.param.deleteRadio(m.idRadio);
    const index: number = this.param.radiolist.indexOf(m);
    console.log(index);
    if (index !== -1) {
      this.param.radiolist.splice(index, 1);
      this.okToast(this.param.datatext.radioRemove);
    }
    
  }

 is_a_url(url: string){
  var urlformat="^(http|https)://.*$";
  return url.match(urlformat);
 }
  
  
  addRadio(ev)
  {
    ev.preventDefault();
    if(this.is_a_url(this.inputurl) && this.inputname.length>1){
      if (this.param.radiolist.find(x=>x.nameRadio==this.inputname)) {
        this.errorToast(this.param.datatext.radioexist);
      }else{
          this.param.addRadio(this.inputname,this.inputurl);
          this.okToast(this.param.datatext.radioadd);
          this.inputname="";
          this.inputurl="";
          setTimeout(() => {
            this.content1.scrollToBottom();
          }, 2000
          );
          setTimeout(() => {
            this.param.getradio();
          }, 1000
          );
        }
    }else{
        this.errorToast(this.param.datatext.radioincorrect);
    }
  }

}
