import { Component, OnInit } from "@angular/core";
import { NavController } from "ionic-angular";
import { LoadingController } from "ionic-angular";
import { ParamService } from "../../services/param.service";
import { ApiService } from "../../services/api.service";
import { AlertService } from "../../services/alert.service";
import { HomePage } from "../home/home";
import { AnnoncePage } from "../annonce/annonce";
import { PopupService } from "../../services/popup.service";
import { SpeechService } from "../../services/speech.service";


@Component({
  selector: "page-first",
  templateUrl: "first.html",
})
export class FirstPage implements OnInit {
  lowbattery: any;
  hour: any;
  homePage = HomePage;
  annoncePage = AnnoncePage;
  appopen: any;
  cpt_battery: number;
  microon: boolean;
  batteryState: number;
  cpt_open: number;
  update: any;
  textBoutonHeader: string; // bouton 's text (Return / Exit), to the left of the header, depends on the page you are on
  textHeadBand: string;
  volumeState: number; //0 mute,  1 low,  2 hight
  popup_battery: boolean;
  loading: any;
  lat: number;
  long: number;

  constructor(
    public loadingCtrl: LoadingController,
    public popup: PopupService,
    public param: ParamService,
    public alert: AlertService,
    public speech: SpeechService,
    public api: ApiService,
    public navCtrl: NavController
  ) {
    this.cpt_open = 0;
    this.cpt_battery = 0;
    this.loading = this.loadingCtrl.create({});
    this.loading.present();
  }

  ngOnInit() {
    this.appopen = setInterval(() => this.appOpen(), 1000);
    navigator.geolocation.getCurrentPosition(position => {
      const { latitude, longitude } = position.coords;
      // Show a map centered at latitude / longitude.
      this.lat = latitude;
      this.long = longitude;


    });
   
  }

  appOpen() {
    if (this.param.localhost) {
      this.api.checkrobot();
    }
    this.api.checkInternet();
    this.alert.checkwifi(this.api.wifiok);
    this.cpt_open += 1;
    if (this.cpt_open === 15 && !this.api.robotok) {
      this.speech.speak(this.param.datatext.cantstart);
      this.popup.startFailedAlert();
    } else if (this.cpt_open === 15 && this.api.robotok) {
      this.popup.relocAlert();
      this.speech.speak(this.param.datatext.relocAlert_title + " " + this.param.datatext.relocAlert_message);
    }
    if (!this.api.appOpened) {
      this.param.getDataRobot();
      this.param.getMail();
      this.param.getradio();
      this.param.getMusicFolders();
      this.param.getParamRound();
      //this.param.getPhone();
      this.param.getBattery();
      this.param.getDataAnnouncement();
      this.param.getDurationNS();
      this.param.getDataPatrol();
      this.param.getDataPlaylist();
      if (
        this.param.announcement &&
        this.param.playlist &&
        this.param.duration &&
        this.param.robot &&
        this.param.maillist
      ) {
        this.param.fillData();
        this.speech.getVoice();
        if (this.param.robot.httpskomnav == 0) {
          this.api.httpskomnav = "http://";
          this.api.wsskomnav = "ws://";
        } else {
          this.api.httpskomnav = "https://";
          this.api.wsskomnav = "wss://";
        }
        if (this.param.robot.httpsros == 0) {
          this.api.wssros = "ws://";
        } else {
          this.api.wssros = "wss://";
        }

      }
      if (this.api.robotok && this.param.langage) {
        if (!this.api.socketok) {
          this.api.instanciate();
        }
        this.api.getCurrentMap();
        this.api.getRoundList();

        this.param.getreservations();
        if (this.api.mapdata) {
          this.api.eyesHttp(4);
          this.api.appOpened = true;
        }
      }
    } else if (this.api.appOpened && this.cpt_open >= 5) {
      if (this.lat) {
        this.param.paramround.gpslat = this.lat;
        this.param.paramround.gpslong = this.long;
        this.param.updatelocation();
      }
      this.api.abortNavHttp();
      this.alert.appOpen(this.api.mailAddInformationBasic());
      if (this.api.round_current_map) {

        if (this.param.patrolInfo.round_mask_detection == 1) {

          this.api.startMaskDetection(true);
        }
        if (this.param.patrolInfo.round_fall_detection == 1) {
          this.api.startPeopleDetection(true);
        }
        //console.log(this.param.announcement);
        this.navCtrl.setRoot(this.homePage);
        this.loading.dismiss();
        //console.log(this.popup.alert_blocked);
        if (this.popup.alert_blocked) {
          this.popup.alert_blocked.dismiss();
        }
        clearInterval(this.appopen);
        console.log(this.cpt_open);
      } else {
        this.api.getRoundList();
      }
    }
  }
}
