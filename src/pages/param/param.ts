import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ApiService } from '../../services/api.service';
import { MailPage } from '../../pages/mail/mail';
import { SMSPage } from '../sms/sms';
import { AnnoncePage } from '../annonce/annonce';
import { LanguePage } from '../langue/langue';
import { ParamService } from '../../services/param.service';
import { PasswordPage } from '../password/password';
import { RobotBehaviourPage } from '../robotBehaviour/robotBehaviour';
import { RadioPage } from '../radio/radio';


@Component({
  selector: 'page-param',
  templateUrl: 'param.html'
})

export class ParamPage implements OnInit {

  mailPage = MailPage;
  languePage = LanguePage;
  smsPage = SMSPage;
  annoncePage = AnnoncePage;
  passwordPage = PasswordPage;
  behaviourPage = RobotBehaviourPage;
  radioPage = RadioPage;
  ngOnInit() {

  }

  constructor(public navCtrl: NavController, public api: ApiService, public param: ParamService) {


  }

  goMail(ev) {
    ev.preventDefault();
    this.navCtrl.push(this.mailPage);
  }
  goSms(ev) {
    ev.preventDefault();
    this.navCtrl.push(this.smsPage);
  }
  goLangue(ev) {
    ev.preventDefault();
    this.navCtrl.push(this.languePage);
  }
  goPassword(ev) {
    ev.preventDefault();
    this.navCtrl.push(this.passwordPage);
  }
  goAnnonce(ev) {
    ev.preventDefault();
    this.navCtrl.push(this.annoncePage);
  }
  goRobotBehaviour(ev){
    ev.preventDefault();
    this.navCtrl.push(this.behaviourPage);
  }
  goRadio(ev) {
    ev.preventDefault();
    this.navCtrl.push(this.radioPage);
  }

}
