import { Component, OnInit } from '@angular/core';
import { ParamService } from '../../services/param.service';
import { LoadingController } from 'ionic-angular';
import { PopupService } from '../../services/popup.service';
import { ToastController } from 'ionic-angular';
import { ApiService } from '../../services/api.service';



@Component({
  selector: 'page-robotBehaviour',
  templateUrl: 'robotBehaviour.html'
})

export class RobotBehaviourPage implements OnInit {
  ngOnInit() {
    this.param.getDataPatrol();
  }



  constructor( public param:ParamService,  public api:ApiService, public loadingCtrl:LoadingController, public popup:PopupService, public toastCtrl: ToastController) {

  }
  
 

  update_fall_detection(){
    if(this.param.patrolInfo.round_fall_detection == 1){
      this.param.patrolInfo.round_fall_detection = 0;
    } else {
      this.param.patrolInfo.round_fall_detection = 1;
    }
    this.param.updatePatrol();
  }


  update_mask_detection(){
    if(this.param.patrolInfo.round_mask_detection == 1){
      this.param.patrolInfo.round_mask_detection = 0;
      this.api.startMaskDetection(false);
    } else {
      this.api.startMaskDetection(true)
      this.param.patrolInfo.round_mask_detection = 1;
    }
    this.param.updatePatrol();
  }

}
