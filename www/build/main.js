webpackJsonp([1],{

/***/ 10:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ParamService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__langage_fr_FR_json__ = __webpack_require__(314);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__langage_fr_FR_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__langage_fr_FR_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__langage_en_GB_json__ = __webpack_require__(315);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__langage_en_GB_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__langage_en_GB_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__langage_es_ES_json__ = __webpack_require__(316);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__langage_es_ES_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__langage_es_ES_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__langage_de_DE_json__ = __webpack_require__(317);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__langage_de_DE_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__langage_de_DE_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__langage_el_GR_json__ = __webpack_require__(318);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__langage_el_GR_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__langage_el_GR_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__langage_it_IT_json__ = __webpack_require__(319);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__langage_it_IT_json___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5__langage_it_IT_json__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_common_http__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_platform_browser__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_lodash__ = __webpack_require__(320);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_lodash__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
// the parameter script










var ParamService = /** @class */ (function () {
    function ParamService(httpClient, sanitizer) {
        this.httpClient = httpClient;
        this.sanitizer = sanitizer;
        this.announcement = {};
        this.tableduration = {};
        this.tablerobot = {};
        this.tableparamround = {};
        this.tablereservation = {};
        this.mail = {};
        this.phone = {};
        this.tableannonce = {};
        this.currentduration = {};
        this.playlist = {};
        this.autolaunch = false;
        this.missionsList = [];
        this.tablepatrol = {};
        this.patrolInfo = {};
        this.durationNS = {};
        this.tabledurationNS = {};
        this.radiolist = {};
        this.radio = {};
        this.musiclist = {};
        this.musicfolderlistJ = {};
        this.baseUrl = "http://localhost/Music/";
        this.maillist = [];
        this.phonenumberlist = [];
        this.cpt = 0;
        this.sendduration = false;
    }
    ParamService.prototype.updatePatrol = function () {
        this.tablepatrol.action = "update";
        this.tablepatrol.patrol_id = this.patrolInfo.patrol_id;
        this.tablepatrol.round_fall_detection = this.patrolInfo.round_fall_detection;
        this.tablepatrol.round_mask_detection = this.patrolInfo.round_mask_detection;
        this.httpClient
            .post("http://localhost/ionicDB/updatepatrolround.php", JSON.stringify(this.tablepatrol))
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.getDataPatrol = function () {
        var _this = this;
        this.httpClient
            .get("http://localhost/ionicDB/getpatrol.php")
            .subscribe(function (data) {
            //console.log(data);
            _this.patrolInfo = data[0];
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.getreservations = function () {
        var _this = this;
        this.httpClient.get("http://localhost/ionicDB/autolaunch/getreservations.php").subscribe(function (data) {
            _this.reservations = data[0];
            console.log(_this.reservations);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.getDataRobot = function () {
        var _this = this;
        this.httpClient.get("http://localhost/ionicDB/getrobot.php").subscribe(function (data) {
            //console.log(data);
            _this.robot = data[0];
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.getParamRound = function () {
        var _this = this;
        this.httpClient.get("http://localhost/ionicDB/round/getparamround.php").subscribe(function (data) {
            //console.log(data);
            _this.paramround = data[0];
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.getDataAnnouncement = function () {
        var _this = this;
        this.httpClient
            .get("http://localhost/ionicDB/getannouncement.php")
            .subscribe(function (data) {
            //console.log("annonces");
            //console.log(data);
            _this.announcement = data;
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.getDataPlaylist = function () {
        var _this = this;
        this.httpClient.get("http://localhost/ionicDB/getplaylist.php").subscribe(function (data) {
            //console.log(data);
            _this.playlist = data;
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.getPhone = function () {
        var _this = this;
        this.httpClient.get("http://localhost/ionicDB/getphone.php").subscribe(function (data) {
            if (_this.phonenumberlist.length === 0) {
                for (var value in data) {
                    _this.phonenumberlist.push(data[value].phonenumber);
                }
            }
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.getDataReservationPatrol = function (roundList) {
        var _this = this;
        this.httpClient
            .get("http://localhost/ionicDB/getreservationroundapp.php")
            .subscribe(function (data) {
            console.log(data);
            _this.missionsList = data;
            _this.missionsList.forEach(function (mission) {
                var date = mission.date;
                mission.date = new Date(date.substring(0, 10) + "T" + date.substring(11, date.length));
            });
            _this.updateDisplayMission(roundList);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.addMail = function (m) {
        this.mail.action = "insert";
        this.mail.mail = m;
        this.mail.serialnumber = this.serialnumber;
        this.httpClient
            .post("http://localhost/ionicDB/addmail.php", JSON.stringify(this.mail))
            .subscribe(function (data) {
            //console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.addDuration = function () {
        this.tableduration.action = "insert";
        this.tableduration.serialnumber = this.serialnumber;
        this.tableduration.round = this.currentduration.round;
        this.tableduration.battery = this.currentduration.battery;
        this.tableduration.walk = this.currentduration.walk;
        this.tableduration.patrol = this.currentduration.patrol;
        this.tableduration.date = this.currentduration.date;
        this.httpClient
            .post("http://localhost/ionicDB/addduration.php", JSON.stringify(this.tableduration))
            .subscribe(function (data) {
            //console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.getDurationNS = function () {
        var _this = this;
        this.httpClient.get("http://localhost/ionicDB/duration/getduration.php").subscribe(function (data) {
            _this.durationNS = data;
            _this.duration = data;
            console.log(data);
            console.log(_this.durationNS.length);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.updateDurationNS = function (id) {
        this.tabledurationNS.action = "update";
        this.tabledurationNS.id_duration = id;
        this.httpClient
            .post("http://localhost/ionicDB/duration/updateduration.php", JSON.stringify(this.tabledurationNS))
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.addMission = function (mission) {
        this.tablereservation.action = "insert";
        this.tablereservation.date = mission.date.substring(0, 10) + " " + mission.date.substring(11, 19);
        this.tablereservation.idRound = mission.idRound;
        this.tablereservation.type = mission.type;
        this.tablereservation.tpsReserv = mission.tpsReserv;
        this.tablereservation.idApp = 2;
        this.httpClient
            .post("http://localhost/ionicDB/addreservation.php", JSON.stringify(this.tablereservation))
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.updateDuration = function () {
        this.tableduration.action = "update";
        this.tableduration.round = this.currentduration.round;
        this.tableduration.date = this.currentduration.date;
        this.httpClient
            .post("http://localhost/ionicDB/duration/updatedurationround.php", JSON.stringify(this.tableduration))
            .subscribe(function (data) {
            //console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.updateAnnonce = function (m) {
        console.log("update annonce");
        this.tableannonce.action = "update";
        this.tableannonce.speech = m.speech;
        this.tableannonce.activated = m.activated;
        this.tableannonce.id_annonce = m.id_annonce;
        this.httpClient
            .post("http://localhost/ionicDB/updateannouncement.php", JSON.stringify(this.tableannonce))
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.addAnnonce = function (m) {
        this.tableannonce.action = "insert";
        this.tableannonce.speech = m;
        this.tableannonce.activated = 1;
        this.tableannonce.serialnumber = this.serialnumber;
        this.httpClient
            .post("http://localhost/ionicDB/addannouncement.php", JSON.stringify(this.tableannonce))
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.updateRobot = function () {
        this.tablerobot.action = "update";
        this.tablerobot.langage = this.robot.langage;
        this.tablerobot.serialnumber = this.serialnumber;
        this.tablerobot.name = this.name;
        this.tablerobot.send_pic = this.robot.send_pic;
        this.tablerobot.password = this.robot.password;
        this.httpClient
            .post("http://localhost/ionicDB/updaterobotpassword.php", JSON.stringify(this.tablerobot))
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.updateRoundParam = function () {
        this.tableparamround.action = "update";
        this.tableparamround.sayhour = this.sayhour;
        this.tableparamround.sayweather = this.sayweather;
        this.tableparamround.allowspeech = this.robot.allowspeech;
        this.tableparamround.speak_freq = this.speak_freq;
        this.httpClient
            .post("http://localhost/ionicDB/round/updateparamround.php", JSON.stringify(this.tableparamround))
            .subscribe(function (data) {
            //console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.deleteMail = function (m) {
        this.mail.action = "delete";
        this.mail.mail = m;
        this.httpClient
            .post("http://localhost/ionicDB/deletemail.php", JSON.stringify(this.mail))
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.addPhone = function (m) {
        this.phone.action = "insert";
        this.phone.phonenumber = m;
        this.phone.serialnumber = this.serialnumber;
        this.httpClient
            .post("http://localhost/ionicDB/addphone.php", JSON.stringify(this.phone))
            .subscribe(function (data) {
            //console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.deletePhone = function (m) {
        this.phone.action = "delete";
        this.phone.phonenumber = m;
        this.httpClient
            .post("http://localhost/ionicDB/deletephone.php", JSON.stringify(this.phone))
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.deleteAnnonce = function (id) {
        this.tableannonce.action = "delete";
        this.tableannonce.id_annonce = id;
        this.httpClient
            .post("http://localhost/ionicDB/deleteannouncement.php", JSON.stringify(this.tableannonce))
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.deleteMission = function (id) {
        this.tablereservation.action = "delete";
        this.tablereservation.id = id;
        this.httpClient
            .post("http://localhost/ionicDB/deletereservation.php", JSON.stringify(this.tablereservation))
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.updateDisplayMission = function (roundList) {
        var _this = this;
        this.missionsDisplay = Object(__WEBPACK_IMPORTED_MODULE_9_lodash__["cloneDeep"])(this.missionsList);
        this.missionsDisplay.forEach(function (mission) {
            mission.name = _this.getRoundName(mission.idRound, roundList);
            if (mission.name == undefined) {
                _this.deleteMission(mission.id);
                _this.getDataReservationPatrol(roundList);
            }
            else {
                var date = mission.date.toLocaleDateString(_this.langage);
                var time = mission.date.toLocaleTimeString(_this.langage, { hour: '2-digit', minute: '2-digit' });
                mission.date = date;
                mission.time = time;
                mission.type = _this.getReservationType(mission);
            }
        });
        this.missionsDisplayToday = this.missionsDisplay.filter(function (mission) { return mission.date == new Date().toLocaleDateString(_this.langage); });
        console.log(this.missionsDisplay);
    };
    ParamService.prototype.updatelocation = function () {
        this.tablerobot.action = "update";
        this.tablerobot.gpslat = this.paramround.gpslat;
        this.tablerobot.gpslong = this.paramround.gpslong;
        this.httpClient
            .post("http://localhost/ionicDB/round/updatelocation.php", JSON.stringify(this.tablerobot))
            .subscribe(function (data) {
            //console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.getMail = function () {
        var _this = this;
        this.httpClient.get("http://localhost/ionicDB/getmail.php").subscribe(function (data) {
            if (_this.maillist.length === 0) {
                for (var value in data) {
                    _this.maillist.push(data[value].mail);
                }
            }
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.getBattery = function () {
        var _this = this;
        this.httpClient.get("http://localhost/ionicDB/getbattery.php").subscribe(function (data) {
            _this.battery = data[0];
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.fillData = function () {
        this.name = this.robot.name;
        this.allowspeech = this.robot.allowspeech;
        this.langage = this.robot.langage;
        this.maildatapassw = atob(this.robot.maildatapass);
        this.mailrobotpassw = atob(this.robot.mailrobotpass);
        this.localhost = this.robot.localhost;
        this.serialnumber = this.robot.serialnumber;
        this.robotmail = this.robot.mailrobot;
        this.datamail = this.robot.maildata;
        this.datamaillist = ["data@kompai.com"];
        this.gpslat = this.paramround.gpslat;
        this.gpslong = this.paramround.gpslong;
        this.weatherapikey = this.paramround.weatherapikey;
        this.speak_freq = this.paramround.speak_freq;
        this.allowskip = this.robot.allowskip;
        this.weatherlang = this.langage.substring(0, 2);
        this.sayweather = this.paramround.sayweather;
        this.sayhour = this.paramround.sayhour;
        if (this.langage === "fr-FR") {
            this.datatext = __WEBPACK_IMPORTED_MODULE_0__langage_fr_FR_json___default.a;
        }
        else if (this.langage === "en-GB") {
            this.datatext = __WEBPACK_IMPORTED_MODULE_1__langage_en_GB_json___default.a;
        }
        else if (this.langage === "es-ES") {
            this.datatext = __WEBPACK_IMPORTED_MODULE_2__langage_es_ES_json___default.a;
        }
        else if (this.langage === "de-DE") {
            this.datatext = __WEBPACK_IMPORTED_MODULE_3__langage_de_DE_json___default.a;
        }
        else if (this.langage === "el-GR") {
            this.datatext = __WEBPACK_IMPORTED_MODULE_4__langage_el_GR_json___default.a;
        }
        else if (this.langage === "it-IT") {
            this.datatext = __WEBPACK_IMPORTED_MODULE_5__langage_it_IT_json___default.a;
        }
        this.source = this.sanitizer.bypassSecurityTrustResourceUrl(this.datatext.URL_tourneeapp);
        if (this.duration.length > 0) {
            if (this.duration[this.duration.length - 1].date ===
                new Date().toLocaleDateString("fr-CA")) {
                this.currentduration.date = this.duration[this.duration.length - 1].date;
                this.currentduration.round = this.duration[this.duration.length - 1].round;
                this.currentduration.battery = this.duration[this.duration.length - 1].battery;
                this.currentduration.patrol = this.duration[this.duration.length - 1].patrol;
                this.currentduration.walk = this.duration[this.duration.length - 1].walk;
                this.currentduration.toolbox = this.duration[this.duration.length - 1].toolbox;
                this.currentduration.logistic = this.duration[this.duration.length - 1].logistic;
            }
            else {
                if (!this.sendduration) {
                    this.sendduration = true;
                    this.init_currentduration();
                    this.addDuration();
                }
            }
        }
        else {
            if (!this.sendduration) {
                this.sendduration = true;
                this.init_currentduration();
                this.addDuration();
            }
        }
    };
    ParamService.prototype.cptDuration = function () {
        this.autolaunch = false;
        if (this.currentduration.date === new Date().toLocaleDateString("fr-CA")) {
            this.cpt = parseInt(this.currentduration.round);
            this.currentduration.round = this.cpt + 2;
            this.updateDuration();
        }
        else {
            this.init_currentduration();
            this.addDuration();
        }
    };
    ParamService.prototype.getRoundName = function (id, roundList) {
        var round = roundList.filter(function (round) { return round.Id == parseInt(id); });
        var name;
        if (round.length > 0)
            name = round[0].Name;
        else {
            name = undefined;
        }
        return name;
    };
    ParamService.prototype.getReservationType = function (round) {
        var type;
        if (round.type == 1)
            type = "x 1";
        else if (round.type == 2)
            type = "x " + this.datatext.dailyLetter;
        else
            type = "x " + this.datatext.weeklyLetter;
        return type;
    };
    ParamService.prototype.init_currentduration = function () {
        this.currentduration.round = 0;
        this.currentduration.battery = 0;
        this.currentduration.patrol = 0;
        this.currentduration.walk = 0;
        this.currentduration.toolbox = 0;
        this.currentduration.logistic = 0;
        this.currentduration.date = new Date().toLocaleDateString("fr-CA");
    };
    ParamService.prototype.getradio = function () {
        var _this = this;
        this.httpClient.get("http://localhost/ionicDB/radio/getRadio.php").subscribe(function (data) {
            //console.log(data)
            _this.radiolist = data;
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.deleteRadio = function (idRadio) {
        this.radio.action = "delete";
        this.radio.idRadio = idRadio;
        this.httpClient
            .post("http://localhost/ionicDB/radio/deleteRadio.php", JSON.stringify(this.radio))
            .subscribe(function (data) {
            console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.addRadio = function (nameRadio, urlRadio) {
        this.radio.action = "insert";
        this.radio.nameRadio = nameRadio;
        this.radio.urlRadio = urlRadio;
        this.httpClient
            .post("http://localhost/ionicDB/radio/addRadio.php", JSON.stringify(this.radio))
            .subscribe(function (data) {
            //console.log(data);
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.getMusicFolders = function () {
        var _this = this;
        this.httpClient.get(this.baseUrl, { responseType: 'text' }).subscribe(function (data) {
            var parser = new DOMParser();
            var htmlDocument = parser.parseFromString(data, 'text/html');
            var folderElements = htmlDocument.querySelectorAll('a');
            _this.musicfolderlist = Array.from(folderElements)
                .map(function (element) { return element.textContent.trim(); })
                .filter(function (name) { return name.endsWith('/'); })
                .map(function (name) { return name.slice(0, -1); });
        }, function (err) {
            console.log(err);
        });
    };
    ParamService.prototype.getMusic = function (folderName) {
        var _this = this;
        this.urlLocal = this.baseUrl + folderName + "/";
        this.httpClient.get(this.urlLocal, { responseType: 'text' }).subscribe(function (data) {
            var parser = new DOMParser();
            var htmlDocument = parser.parseFromString(data, 'text/html');
            var folderElements = htmlDocument.querySelectorAll('a');
            _this.musiclist = Array.from(folderElements)
                .map(function (element) { return element.getAttribute('href'); })
                .filter(function (name) { return name.endsWith('.mp3'); })
                .map(function (name) { return name.slice(0, -4); });
        }, function (err) {
            console.log(err);
        });
    };
    ParamService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_6__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_7__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_8__angular_platform_browser__["c" /* DomSanitizer */]])
    ], ParamService);
    return ParamService;
}());

//# sourceMappingURL=param.service.js.map

/***/ }),

/***/ 115:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_api_service__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_alert_service__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_popup_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_param_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_speech_service__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__angular_platform_browser__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_nipplejs__ = __webpack_require__(321);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_nipplejs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_nipplejs__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var HomePage = /** @class */ (function () {
    function HomePage(sanitizer, toastCtrl, popup, api, speech, param, alert, renderer) {
        var _this = this;
        this.sanitizer = sanitizer;
        this.toastCtrl = toastCtrl;
        this.popup = popup;
        this.api = api;
        this.speech = speech;
        this.param = param;
        this.alert = alert;
        this.renderer = renderer;
        this.size = 70;
        this.maxrad = 0.08;
        this.maxlin = 0.12;
        this.maxback = 0.08;
        this.imgWidth = 0;
        this.imgHeight = 0;
        this.SelectDuration = 60;
        this.volume = 30;
        this.wait = false;
        this.detection_url = "http://" + this.param.localhost + "/detections/detection_image.png";
        this.pause = false;
        this.radio = true;
        this.localindex = 0;
        this.isRadioActive = true;
        this.isLocalActive = false;
        this.playNextTrack = function () {
            if (_this.localindex < _this.param.musiclist.length) {
                _this.localindex++;
            }
            if (_this.localindex == _this.param.musiclist.length) {
                _this.localindex = 0;
            }
            if (_this.param.musiclist.length == 0) {
                _this.popup.showToastRed(_this.param.datatext.NoMusic, 2000, "bottom");
                _this.myIframe.src = _this.param.urlLocal + '/' + " ";
                console.log(_this.myIframe.src);
                _this.TextArrow();
                _this.pauseLocal();
            }
            else {
                _this.currentLocalFile = _this.param.musiclist[_this.localindex].substring(0, 12);
                var element = _this.param.musiclist[_this.localindex];
                _this.myIframe.src = _this.param.urlLocal + '/' + element + ".mp3";
                console.log(_this.myIframe.src);
                _this.TextArrow();
                _this.playLocal();
                _this.pause = false;
            }
        };
        this.playPreviousTrack = function () {
            if (_this.localindex > 0) {
                _this.localindex--;
            }
            else {
                _this.localindex = _this.param.musiclist.length - 1;
            }
            if (_this.param.musiclist.length == 0) {
                _this.popup.showToastRed(_this.param.datatext.NoMusic, 2000, "bottom");
                _this.myIframe.src = _this.param.urlLocal + '/' + " ";
                _this.TextArrow();
                _this.pauseLocal();
            }
            else {
                _this.currentLocalFile = _this.param.musiclist[_this.localindex].substring(0, 12);
                var element = _this.param.musiclist[_this.localindex];
                _this.myIframe.src = _this.param.urlLocal + '/' + element + ".mp3";
                console.log(_this.myIframe.src);
                _this.TextArrow();
                _this.playLocal();
                _this.pause = false;
            }
        };
        this.update = setInterval(function () { return _this.getUpdate(); }, 500); // update the status and the trajectory every 1/2 secondes
        this.cptduration = setInterval(function () { return _this.mail(); }, 120000); // toutes les 2 min actualise time
        this.is_abort = true;
        this.start_from_docking = false;
        this.round_start = false;
        this.cpt_locked = 0;
        this.sum_blocked = 0;
        this.time_blocked = 0;
        this.cpt_round_start = 0;
        this.cpt_annonce = 0;
        this.cam = false;
        this.timer_annonce = new Date();
        this.annonces_activated = this.param.announcement.filter(function (x) { return x.activated == "1"; });
        // declenche la popup de permission camera si besoin
        console.log(this.param.robot.cam_USB);
        this.optiondateofday = { weekday: "long", day: "numeric", month: "long" };
        this.selectOptions = {
            title: this.param.datatext.rounds,
        };
        this.selectDuration = {
            title: this.param.datatext.roundDuration,
        };
        this.selectRadio = {
            title: this.param.datatext.radio,
        };
        this.selectLocal = {
            title: this.param.datatext.folder,
        };
        // CAMERA ROS
        //if( this.param.robot.cam_USB==0 ){
        var cameraros = this.api.ros;
        var listener_camera = new ROSLIB.Topic({
            ros: cameraros,
            //name : '/top_webcam/image_raw',
            name: '/fisheye_cam/compressed',
            //name : '/D435_camera_FWD/color/image_raw',
            messageType: 'sensor_msgs/CompressedImage'
        });
        listener_camera.subscribe(function (message) {
            //console.log('Received message on ' + listener_compressed.name + ': ');
            _this.imgRos = document.getElementById('imgcompressed');
            //console.log(message);
            var bufferdata = _this.convertDataURIToBinary2(message.data);
            var blob = new Blob([bufferdata], { type: 'image/jpeg' });
            var blobUrl = URL.createObjectURL(blob);
            _this.imgRos.src = blobUrl;
        });
        //}
        // CAMERA ROS FIN
    }
    HomePage.prototype.mail = function () {
        this.param.cptDuration(); //update duration
        if (this.param.durationNS.length > 1) {
            if (this.api.wifiok) {
                this.alert.duration(this.param.durationNS[0], this.api.mailAddInformationBasic());
                //this.param.updateDurationNS(this.param.durationNS[0].id_duration);
                //this.param.getDurationNS();
            }
        }
    };
    HomePage.prototype.ngOnInit = function () {
        var _this = this;
        this.speech.getVoice();
        this.speech.msg.onend = function (event) {
            //when the robot stop talking
            if (_this.pause == false) {
                if (_this.radio == true) {
                    _this.UnmuteRadio();
                }
                else {
                    _this.playLocal();
                }
            }
        };
        this.renderer.setProperty(this.canvas.nativeElement, "width", 640);
        this.renderer.setProperty(this.canvas.nativeElement, "height", 480);
        if (this.param.radiolist.length === 0) {
            this.currentRadio = {
                nameRadio: '',
                urlRadio: ''
            };
        }
        else {
            this.currentRadio = this.param.radiolist[0];
        }
        if (this.param.musicfolderlist.length === 0) {
        }
        else {
            this.SelectLocal = this.param.musicfolderlist[0];
        }
        this.myIframe = document.getElementById('iframe');
        this.myIframe.src = this.currentRadio.urlRadio;
        this.gifRadio = document.getElementById('gifRadio');
        this.gifPause = document.getElementById('gifPause');
        this.SelectRadio = this.currentRadio.idRadio;
        if (this.currentRadio.urlRadio == '') {
            this.gifRadio.src = "assets/imgs/RadioWaveStop.png";
            this.gifPause.src = "assets/imgs/Play.png";
        }
        else {
            this.gifRadio.src = "assets/imgs/RadioWave.gif";
            this.gifPause.src = "assets/imgs/Pause.png";
        }
        this.playLocal();
        this.myIframe.onended = function () {
            if (_this.radio == false) {
                _this.playNextTrack();
            }
        };
    };
    HomePage.prototype.convertDataURIToBinary2 = function (dataURI) {
        var raw = window.atob(dataURI);
        var rawLength = raw.length;
        var array = new Uint8Array(new ArrayBuffer(rawLength));
        for (var i = 0; i < rawLength; i++) {
            array[i] = raw.charCodeAt(i);
        }
        return array;
    };
    HomePage.prototype.joystickmove = function () {
        this.api.joystickHttp(this.vlin, this.vrad);
    };
    HomePage.prototype.joystickstop = function () {
        this.api.joystickHttp(0, 0);
    };
    HomePage.prototype.createjoystick = function () {
        var _this = this;
        this.options = {
            zone: document.getElementById("zone_joystick"),
            size: 3 * this.size,
        };
        this.manager = __WEBPACK_IMPORTED_MODULE_8_nipplejs___default.a.create(this.options);
        this.manager.on("move", function (evt, nipple) {
            _this.currentx = nipple.raw.position.x;
            _this.currenty = nipple.raw.position.y;
            _this.checkjoystick = true;
            if (!_this.stopjoystick) {
                if (nipple.direction && nipple.direction.angle && nipple.angle) {
                    //console.log(nipple.direction.angle);
                    if (nipple.direction.angle === "left") {
                        _this.vlin = 0;
                        _this.vrad = (0.3 * nipple.distance) / 50;
                    }
                    else if (nipple.direction.angle === "right") {
                        _this.vlin = 0;
                        _this.vrad = -(0.3 * nipple.distance) / 50;
                    }
                    else if (nipple.direction.angle === "up") {
                        _this.vlin = (_this.maxlin * nipple.distance) / 50;
                        _this.vrad = 0;
                        // if (Math.abs((nipple.angle.radian - Math.PI / 2) / 2) > 0.18) {
                        //   this.vrad = (nipple.angle.radian - Math.PI / 2) / 2 - 0.09;
                        // } else {
                        // this.vrad = 0;
                        // }
                    }
                    else if (nipple.direction.angle === "down") {
                        _this.vlin = -(_this.maxback * nipple.distance) / 50;
                        _this.vrad = 0;
                    }
                }
            }
            else {
                _this.vlin = 0;
                _this.vrad = 0;
            }
        });
        this.manager.on("added", function (evt, nipple) {
            console.log("added");
            _this.gamepadinterv = setInterval(function () { if (!_this.stopjoystick) {
                _this.joystickmove();
            } }, 500);
        });
        this.manager.on("end", function (evt, nipple) {
            console.log("end");
            _this.vlin = 0;
            _this.vrad = 0;
            clearInterval(_this.gamepadinterv);
            _this.checkjoystick = false;
            _this.stopjoystick = false;
            _this.joystickstop();
        });
    };
    HomePage.prototype.ngAfterViewInit = function () {
        console.log(document.getElementById("zone_joystick"));
        this.createjoystick();
    };
    HomePage.prototype.getRandomInt = function (max) {
        return Math.floor(Math.random() * Math.floor(max));
    };
    HomePage.prototype.ionViewWillLeave = function () {
        //clearInterval(this.update);
        //clearInterval(this.cptduration);
    };
    HomePage.prototype.btnPressed = function () {
        // stop the robot if bouton pressed
        if (this.api.is_background || this.api.btnPush || !this.api.is_connected) {
            if (this.api.is_high) {
                this.api.is_high = false;
                if (this.popup.alert_blocked) {
                    this.popup.alert_blocked.dismiss();
                }
                this.alert.manualintervention("<br> Blocked for : " +
                    this.calcDiffMin(this.date_blocked, new Date()) +
                    " <br>" +
                    this.api.mailAddInformationRound());
                this.time_blocked = 0;
            }
            this.api.foreground();
            if (this.api.roundActive) {
                this.onGo(false);
            }
            else if (this.api.towardDocking) {
                this.api.abortNavHttp();
                //this.api.abortDockingHttp();
                this.api.towardDocking = false;
                if (this.api.battery_status.remaining <= this.param.battery.critical) {
                    // lowBattery
                    this.popup.lowBattery();
                }
                else {
                    this.popup.goDockingConfirm();
                }
            }
            this.api.btnPush = false; // to say that we have received the btn pushed status
        }
    };
    HomePage.prototype.onRoundChange = function () {
        this.api.abortHttp();
        //console.log(this.monitor);
    };
    HomePage.prototype.onDurationChange = function () {
        this.api.hourStart = new Date();
    };
    HomePage.prototype.updateTrajectory = function () {
        // listen the round status to allow the robot to go to the next
        if (this.api.roundActive &&
            !this.api.towardDocking &&
            !this.api.is_blocked) {
            // round in progress
            if (this.api.round_status.status === 2 || this.is_abort) {
                this.is_abort = false;
                this.start_from_docking = false;
            }
            if (this.api.round_status.status === 2) {
                // round start correctly
                this.round_start = true;
                if (!this.api.start) {
                    this.api.start = true;
                    this.api.hourStart = new Date();
                }
            }
            else if (this.api.docking_status.status === 3) {
                // start from docking
                this.start_from_docking = true;
            }
            else if (this.api.round_status.status === 0 && this.round_start) {
                //round stoped remotely
                this.popup.RemoteConfirm();
                this.alert.remoteStop(this.api.mailAddInformationBasic());
                this.api.roundActive = false;
                this.round_start = false;
                this.is_abort = true;
                this.api.abortHttp();
            }
            else if (this.api.round_status.status === 0 && !this.round_start) {
                //round fail
                this.cpt_round_start += 1;
                if (this.cpt_round_start > 10) {
                    this.popup.errorNavAlert();
                    this.alert.naverror(this.api.mailAddInformationBasic());
                    this.api.roundActive = false;
                    this.round_start = false;
                    this.is_abort = true;
                    this.api.abortHttp();
                }
            }
            else if (this.api.round_status.status === 4) {
                //wait acknowledge
                if (!this.api.btnPush || !this.api.b_pressed) {
                    //verify if it is cause by a btn pushed
                    this.api.acknowledgeHttp();
                }
            }
            else if (this.api.round_status.status === 5) {
                // round completed
                this.startRound();
            }
            else if (this.api.round_status.status === 6) {
                //hight current
                this.api.roundActive = false;
                this.round_start = false;
                this.is_abort = true;
                this.api.pauseHttp(false);
                this.popup.errorNavAlert();
                this.alert.naverror(this.api.mailAddInformationRound());
                this.api.is_high = true;
                this.date_blocked = new Date();
                this.capture();
            }
            else if (this.api.navigation_status.status === 5 &&
                !(this.api.round_status.status === 6)) {
                // lost
                this.popup.lostAlert();
                this.alert.robotLost(this.api.mailAddInformationBasic());
                this.api.roundActive = false;
                this.round_start = false;
                this.is_abort = true;
                this.api.abortHttp();
            }
        }
        else if (this.api.towardDocking) {
            // if(this.api.differential_status.status===2){ // if current is hight
            //   this.round_start=false;
            //   this.api.towardDocking=false;
            //   this.api.abortNavHttp();
            //   this.popup.blockedAlert();
            //   this.alert.errorblocked(this.api.mailAddInformationBasic());
            //   this.capture();
            // }
            if (this.api.navigation_status.status === 5) {
                this.round_start = false;
                this.api.towardDocking = false;
                this.api.abortNavHttp();
                this.popup.errorNavAlert();
                this.alert.naverror(this.api.mailAddInformationBasic());
            }
            else if (this.api.docking_status.status === 3) {
                this.api.towardDocking = false;
                this.round_start = false;
                this.alert.charging(this.api.mailAddInformationBasic());
                this.api.close_app = true; //close application
                this.api.deleteEyesHttp(23);
                setTimeout(function () {
                    window.close();
                }, 1000);
            }
            else if (this.api.navigation_status.status === 0 &&
                this.api.docking_status.detected) {
                this.api.connectHttp();
            }
        }
        else if (this.api.docking_status.status === 3 && !this.api.roundActive) {
            if (!this.is_abort) {
                this.api.abortHttp();
            }
            this.is_abort = true;
            this.api.towardDocking = false;
            this.round_start = false;
        }
        else {
            if (this.api.round_status.status === 0 ||
                this.api.round_status.status === 3) {
                this.is_abort = true;
            }
            else if (!this.is_abort) {
                //this.is_abort=true;
                //this.api.pauseHttp(true);
            }
        }
    };
    HomePage.prototype.announcement = function () {
        var now = new Date().toLocaleString(this.param.langage, {
            hour: "numeric",
            minute: "numeric",
        });
        if (this.calcDiffSec(this.timer_annonce, new Date()) >=
            this.param.speak_freq &&
            this.param.allowspeech == 1) {
            this.annonces_activated = this.param.announcement.filter(function (x) { return x.activated == "1"; });
            this.timer_annonce = new Date();
            if (this.annonces_activated.length > 0 ||
                this.param.sayweather == 1 ||
                this.param.sayhour == 1) {
                if (this.radio == true) {
                    this.muteRadio();
                }
                else {
                    this.pauseLocal();
                }
            }
            if (this.cpt_annonce < this.annonces_activated.length) {
                // say the announcement that are activated
                this.speech.speak(this.annonces_activated[this.cpt_annonce].speech);
                this.cpt_annonce += 1;
                if (this.param.sayweather == 0 &&
                    this.param.sayhour == 0 &&
                    this.cpt_annonce === this.annonces_activated.length) {
                    this.cpt_annonce = 0;
                }
            }
            else if (this.cpt_annonce === this.annonces_activated.length &&
                this.param.sayhour == 1) {
                // say the hour if it is allowed
                this.speech.speak(this.param.datatext.announce1 +
                    new Date().toLocaleDateString(this.param.langage, this.optiondateofday) +
                    this.param.datatext.announce2 +
                    now);
                if (this.param.sayweather == 1) {
                    this.cpt_annonce += 1;
                }
                else {
                    this.cpt_annonce = 0;
                }
            }
            else if (this.api.wifiok &&
                this.cpt_annonce >= this.annonces_activated.length &&
                this.param.sayweather == 1) {
                // say the weather if it is allowed
                this.speech.getDataWeather();
                this.cpt_annonce = 0;
            }
            else {
                this.cpt_annonce = 0;
                if (this.pause == false) {
                    if (this.radio == true) {
                        this.UnmuteRadio();
                    }
                    else {
                        this.playLocal();
                    }
                }
            }
        }
    };
    HomePage.prototype.calcDiffSec = function (one, two) {
        var DateBegSec = one.getSeconds();
        var DateBegMin = one.getMinutes() * 60;
        var DateBegHours = one.getHours() * 60 * 60;
        var DateBeg = DateBegSec + DateBegMin + DateBegHours;
        var DateEndSec = two.getSeconds();
        var DateEndMin = two.getMinutes() * 60;
        var DateEndHours = two.getHours() * 60 * 60;
        return DateEndSec + DateEndMin + DateEndHours - DateBeg;
    };
    HomePage.prototype.getUpdate = function () {
        var _this = this;
        if (this.api.close_app) {
            clearInterval(this.update);
            clearInterval(this.cptduration);
        }
        //console.log(this.api.round_current_map.filter(r=> r.Id==this.param.reservations.idRound).length>0);
        if (this.param.reservations && this.param.reservations.idApp == 2 && this.calcDiffMin(new Date(), new Date(this.param.reservations.date)) === 0 && !this.param.autolaunch) {
            //console.log("auto launch");
            if (this.api.round_current_map.filter(function (r) { return r.Id == _this.param.reservations.idRound; }).length > 0) {
                var rname = this.api.round_current_map.filter(function (r) { return r.Id == _this.param.reservations.idRound; })[0].Name;
                this.param.autolaunch = true;
                this.api.hourStart = new Date();
                this.SelectRound = Number(this.param.reservations.idRound);
                this.SelectDuration = Number(this.param.reservations.tpsReserv);
                if (this.api.roundActive && this.api.round_status.round && this.api.round_status.round.Id == this.param.reservations.idRound) {
                    console.log("do nothing");
                }
                else {
                    this.speech.speak(this.param.datatext.autolaunch + rname);
                    this.popup.showToast(this.param.datatext.autolaunch + rname, 15000, "middle");
                    if (this.api.roundActive) {
                        this.onGo(false);
                    }
                    this.api.abortHttp();
                    if (this.api.anticollision_status.forward == 2) {
                        this.api.disconnectHttp();
                    }
                    setTimeout(function () {
                        _this.onGo(false);
                    }, 8000);
                }
            }
        }
        if (this.api.fdetected && this.param.patrolInfo.round_fall_detection == 1) {
            this.popup.showToastRed(this.param.datatext.FallConfirm_title, 500, "bottom");
        }
        else if (this.api.maskdetected && this.param.patrolInfo.mask_detection == 1) {
            this.popup.showToastRed(this.param.datatext.pleasemask, 500, "bottom");
            if (!this.wait) {
                this.wait = true;
                this.speech.speak(this.param.datatext.speechmask);
                setTimeout(function () {
                    _this.wait = false;
                }, 8000);
            }
        }
        if (this.api.falldetected) {
            // detect a fall
            //console.log(this.api.img_detection);
            console.log("getimgfallOK");
            this.api.falldetected = false;
            this.api.persondetected = false;
            this.round_start = false;
            this.is_abort = true;
            this.api.pauseHttp(false);
            if (!this.api.popupFall) {
                this.api.popupFall = true;
                this.popup.FallConfirm();
            }
            this.capturedetect(2);
            this.alert.fall(this.api.mailAddInformationBasic());
        }
        // update trajectory, butons input, round status
        if (!this.api.is_connected) {
            this.api.roundActive = false;
            this.api.towardDocking = false;
            this.round_start = false;
        }
        else {
            if (this.api.fct_onGo) {
                this.api.fct_onGo = false;
                this.onGo(false);
            }
            if (this.api.fct_startRound) {
                this.api.fct_startRound = false;
                this.startRound();
            }
            this.btnPressed();
            this.updateTrajectory();
            this.announcement();
            this.watchIfLocked();
            this.updateTimeRound();
        }
        // test joystick count
        if (this.checkjoystick) {
            if (this.currentx == this.lastx) {
                if (this.currenty == this.lasty) {
                    this.countjoystick = this.countjoystick + 1;
                    if (this.countjoystick >= 5) {
                        console.log("stopjoystick");
                        this.joystickstop();
                        this.stopjoystick = true;
                    }
                    if (this.countjoystick >= 15) {
                        clearInterval(this.gamepadinterv);
                        this.checkjoystick = false;
                        this.manager.destroy();
                        this.createjoystick();
                        this.checkjoystick = false;
                    }
                }
                else {
                    this.countjoystick = 0;
                    this.stopjoystick = false;
                    this.checkjoystick = true;
                    this.lastx = this.currentx;
                    this.lasty = this.currenty;
                }
            }
            else {
                this.countjoystick = 0;
                this.stopjoystick = false;
                this.checkjoystick = true;
                this.lastx = this.currentx;
                this.lasty = this.currenty;
            }
        }
    };
    HomePage.prototype.watchIfLocked = function () {
        if (this.api.roundActive || this.api.towardDocking) {
            if ((this.api.towardDocking && this.api.docking_status.detected) ||
                this.api.docking_status.status === 3 ||
                (this.api.anticollision_status.forward < 2 &&
                    this.api.anticollision_status.right < 2 &&
                    this.api.anticollision_status.left < 2)) {
                this.cpt_locked = 0;
                if (this.api.roundActive) {
                    //round automatic release
                    if (this.api.is_blocked) {
                        this.time_blocked = this.calcDiffMin(this.date_blocked, new Date());
                        this.sum_blocked += this.time_blocked;
                        this.alert.noLongerBlocked("<br> Blocked for : " +
                            this.time_blocked +
                            " <br>" +
                            this.api.mailAddInformationRound());
                        this.api.is_blocked = false;
                        //this.api.resumeHttp();
                        if (this.popup.alert_blocked) {
                            this.popup.alert_blocked.dismiss();
                        }
                        this.time_blocked = 0;
                    }
                }
                else {
                    //simple nav
                    if (this.api.is_blocked) {
                        this.api.is_blocked = false;
                        if (this.popup.alert_blocked) {
                            this.popup.alert_blocked.dismiss();
                        }
                    }
                }
            }
            else if (this.api.anticollision_status.forward === 2 ||
                this.api.anticollision_status.right === 2 ||
                this.api.anticollision_status.left === 2) {
                this.cpt_locked += 1;
            }
            if (this.cpt_locked > 20 && !this.api.is_blocked) {
                if (this.api.roundActive) {
                    this.date_blocked = new Date();
                    //this.api.pauseHttp(true);
                    this.api.skipHttp();
                    this.popup.blockedAlert();
                    this.api.is_blocked = true;
                    this.alert.blocking(this.api.mailAddInformationRound());
                }
                else if (this.api.towardDocking) {
                    this.popup.blockedAlert();
                    this.api.is_blocked = true;
                    this.alert.blockingdocking(this.api.mailAddInformationBasic());
                }
            }
            else if (this.cpt_locked === 30 && this.api.round_status.status === 5) {
                this.startRound();
            }
            else if (this.cpt_locked === 40 && this.api.is_blocked) {
                this.capture();
            }
        }
    };
    HomePage.prototype.startRound = function () {
        //start the right round
        if (this.SelectRound) {
            this.api.startRoundHttp(Number(this.SelectRound));
        }
    };
    HomePage.prototype.calcDiffMin = function (one, two) {
        var DateBegMin = one.getMinutes();
        var DateBegHours = one.getHours() * 60;
        var DateBeg = DateBegMin + DateBegHours;
        var DateEndMin = two.getMinutes();
        var DateEndHours = two.getHours() * 60;
        return DateEndMin + DateEndHours - DateBeg;
    };
    HomePage.prototype.updateTimeRound = function () {
        var _this = this;
        if (this.api.start) {
            if (this.calcDiffMin(this.api.hourStart, new Date()) > this.SelectDuration) {
                // after x min of round the robot must go to the docking
                this.alert.roundCompleted(this.api.mailAddInformationRound() +
                    "<br> Total blocking : " +
                    this.sum_blocked +
                    "<br>");
                this.api.abortHttp();
                this.api.roundActive = false;
                this.api.towardDocking = true;
                this.sum_blocked = 0;
                this.time_blocked = 0;
                this.api.hourStart = new Date();
                setTimeout(function () {
                    _this.api.reachHttp("docking");
                }, 2000);
            }
        }
    };
    HomePage.prototype.onclickGo = function (ev) {
        ev.preventDefault();
        this.onGo(false);
    };
    HomePage.prototype.onGo = function (btnpressed) {
        var _this = this;
        // buton go code
        this.cpt_round_start = 0;
        this.cpt_locked = 0;
        if (this.api.is_high) {
            this.api.is_high = false;
            if (this.popup.alert_blocked) {
                this.popup.alert_blocked.dismiss();
            }
            this.alert.manualintervention("<br> Blocked for : " +
                this.calcDiffMin(this.date_blocked, new Date()) +
                " <br>" +
                this.api.mailAddInformationRound());
            this.time_blocked = 0;
        }
        if (this.api.towardDocking) {
            // stop go docking
            this.round_start = false;
            this.api.towardDocking = false;
            this.api.abortNavHttp();
            this.api.abortDockingHttp();
            if (this.api.battery_status.remaining <= this.param.battery.critical) {
                // lowBattery
                this.popup.lowBattery();
            }
            else {
                this.popup.goDockingConfirm();
            }
        }
        else if (!this.api.roundActive && !this.api.towardDocking) {
            // run round
            this.round_start = false;
            if (this.api.anticollision_status.forward === 2 && this.api.battery_status.status != 0 && !this.param.autolaunch) {
                this.popup.StartBlockedAlert();
            }
            else if (!(this.api.docking_status.status === 3) &&
                this.api.battery_status.remaining <= this.param.battery.critical) {
                // lowBattery
                this.popup.lowBattery();
            }
            else if (this.api.docking_status.status === 3) {
                // to know if the robot is in the docking
                if (this.api.battery_status.remaining <= this.param.battery.critical) {
                    this.popup.robotmuststayondocking();
                }
                else {
                    if (this.param.autolaunch) {
                        this.api.roundActive = true;
                        this.api.fct_startRound = true;
                    }
                    else {
                        this.popup.presentAlert();
                    }
                } // pop up docking
            }
            else {
                if (this.api.round_status.status === 0 ||
                    this.api.round_status.status === 5) {
                    //ready to execute a round
                    this.api.roundActive = true;
                    this.startRound();
                    if (!this.api.start) {
                        this.api.hourStart = new Date();
                        this.sum_blocked = 0;
                        this.time_blocked = 0;
                        this.api.start = true;
                    }
                    setTimeout(function () {
                        _this.alert.roundLaunched(_this.api.mailAddInformationRound());
                    }, 2000);
                }
                else if (this.api.round_status.status === 3) {
                    this.api.resumeHttp();
                    this.api.roundActive = true;
                    setTimeout(function () {
                        _this.alert.roundLaunched(_this.api.mailAddInformationRound());
                    }, 2000);
                }
                else {
                    this.api.roundActive = false;
                    this.round_start = false;
                    this.start_from_docking = false;
                    //this.is_abort=true;
                    this.api.abortHttp();
                    this.alert.roundFailed(this.api.mailAddInformationBasic());
                    this.popup.errorlaunchAlert();
                }
            }
        }
        else {
            //stop round
            if (this.api.is_blocked) {
                this.api.roundActive = false;
                this.api.is_blocked = false;
                if (this.popup.alert_blocked) {
                    this.popup.alert_blocked.dismiss();
                }
                this.api.pauseHttp(true);
                this.alert.manualintervention("<br> Blocked for : " +
                    this.calcDiffMin(this.date_blocked, new Date()) +
                    " <br>" +
                    this.api.mailAddInformationRound());
                this.time_blocked = 0;
            }
            else {
                this.api.pauseHttp(false);
                this.alert.roundPaused(this.api.mailAddInformationRound());
            }
            this.round_start = false;
            if (this.start_from_docking) {
                this.api.abortNavHttp();
                //this.api.abortDockingHttp();
                this.start_from_docking = false;
            }
            if (btnpressed) {
                this.popup.presentConfirm(); //pop up round paused
            }
        }
    };
    HomePage.prototype.displaycam = function (ev) {
        ev.preventDefault();
        if (this.cam) {
            this.cam = false;
            this.api.deleteEyesHttp(23);
            var shand = document.getElementsByClassName("youtube");
            if (shand.length != 0) {
                shand[0].style.display = "inline";
            }
            var shend = document.getElementsByClassName("video");
            if (shend.length != 0) {
                shend[0].style.display = "none";
            }
        }
        else {
            this.api.eyesHttp(23);
            this.cam = true;
            var shand = document.getElementsByClassName("youtube");
            if (shand.length != 0) {
                shand[0].style.display = "none";
            }
            var shend = document.getElementsByClassName("video");
            if (shend.length != 0) {
                shend[0].style.display = "inline";
            }
        }
    };
    // function take snapshot and send mail if robot blocked
    HomePage.prototype.capture = function () {
        if (this.param.robot.send_pic == 1) {
            // capture via canvas Ros
            this.canvas.nativeElement
                .getContext("2d")
                .drawImage(this.imgRos, 0, 0);
            var url = this.canvas.nativeElement.toDataURL();
            this.alert.Blocked_c(url);
        }
        else {
            this.alert.Block_c();
        }
    };
    HomePage.prototype.capturedetect = function (capturewhat) {
        var _this = this;
        if (this.imgHeight == 0 || this.imgWidth == 0) {
            this.imge = document.getElementById("imgpersondetect");
            this.imgHeight = this.imge.naturalHeight;
            this.imgWidth = this.imge.naturalWidth;
            console.log(this.imgHeight);
            console.log(this.imgWidth);
        }
        if (capturewhat === 3) {
            this.imge.src = "http://" + this.param.localhost + "/detections/mask_detection_image.png?t=" + new Date().getTime();
        }
        else {
            this.imge.src = "http://" + this.param.localhost + "/detections/detection_image.png?t=" + new Date().getTime();
        }
        setTimeout(function () {
            _this.renderer.setProperty(_this.canvaspersondetect.nativeElement, "width", _this.imgWidth);
            _this.renderer.setProperty(_this.canvaspersondetect.nativeElement, "height", _this.imgHeight);
            _this.canvaspersondetect.nativeElement
                .getContext("2d")
                .drawImage(_this.imge, 0, 0);
            _this.url = _this.canvaspersondetect.nativeElement.toDataURL();
            if (capturewhat === 2) {
                if (_this.param.robot.send_pic == 1) {
                    _this.alert.PicFall_c(_this.url);
                }
                else {
                    _this.alert.Fall_c();
                }
            }
        }, 3500);
    };
    HomePage.prototype.onSliderRelease = function (ev, id) {
        ev.preventDefault();
        id.open();
    };
    HomePage.prototype.onRadioChange = function (event) {
        this.currentRadio = this.param.radiolist.find(function (x) { return x.idRadio == event; });
        this.myIframe.src = this.currentRadio.urlRadio;
    };
    HomePage.prototype.onVolumeChange = function (event) {
        this.pause = false;
        if (this.volume == 0) {
            this.muteRadio();
        }
        else {
            this.UnmuteRadio();
        }
    };
    HomePage.prototype.muteRadio = function () {
        this.myIframe.volume = 0;
        this.gifRadio.src = "assets/imgs/RadioWaveStop.png";
        this.gifPause.src = "assets/imgs/Play.png";
    };
    HomePage.prototype.UnmuteRadio = function () {
        if (this.currentRadio.urlRadio == '') {
            this.popup.showToastRed(this.param.datatext.NoRadio, 500, "bottom");
        }
        else {
            this.myIframe.volume = this.volume / 100;
            this.gifRadio.src = "assets/imgs/RadioWave.gif";
            this.gifPause.src = "assets/imgs/Pause.png";
        }
    };
    HomePage.prototype.pauseRadio = function () {
        if (this.radio == true) {
            if (this.pause == true) {
                this.UnmuteRadio();
                this.pause = false;
            }
            else {
                this.muteRadio();
                this.pause = true;
            }
        }
        else {
            if (this.pause == true) {
                if (this.param.musiclist.length == 0) {
                    this.popup.showToastRed(this.param.datatext.NoMusic, 500, "bottom");
                }
                else {
                    this.playLocal();
                    this.pause = false;
                }
            }
            else {
                this.pauseLocal();
                this.pause = true;
            }
        }
    };
    HomePage.prototype.pauseLocal = function () {
        this.myIframe.pause();
        this.gifRadio.src = "assets/imgs/RadioWaveStop.png";
        this.gifPause.src = "assets/imgs/Play.png";
    };
    HomePage.prototype.playLocal = function () {
        this.myIframe.play();
        this.gifRadio.src = "assets/imgs/RadioWave.gif";
        this.gifPause.src = "assets/imgs/Pause.png";
    };
    HomePage.prototype.changetoRadio = function () {
        this.radio = true;
        this.myIframe.src = this.currentRadio.urlRadio;
        this.isRadioActive = true;
        this.isLocalActive = false;
        this.playLocal();
        this.pause = false;
    };
    HomePage.prototype.changetoLocal = function () {
        var _this = this;
        this.radio = false;
        this.localindex = 0;
        this.UnmuteRadio();
        this.param.getMusic(this.SelectLocal);
        setTimeout(function () {
            if (_this.param.musiclist.length == 0) {
                _this.popup.showToastRed(_this.param.datatext.NoMusic, 2000, "bottom");
                _this.myIframe.src = _this.param.urlLocal + '/' + " ";
                _this.TextArrow();
                _this.pauseLocal();
                _this.pause = true;
            }
            else {
                _this.myIframe.src = _this.param.baseUrl + _this.SelectLocal + '/' + _this.param.musiclist[0] + ".mp3";
                _this.currentLocalFile = _this.param.musiclist[0].substring(0, 12);
                _this.TextArrow();
            }
        }, 200);
        this.isRadioActive = false;
        this.isLocalActive = true;
    };
    HomePage.prototype.onLocalChange = function (event) {
        var _this = this;
        this.param.getMusic(this.SelectLocal);
        this.localindex = 0;
        setTimeout(function () {
            if (_this.param.musiclist.length == 0) {
                _this.popup.showToastRed(_this.param.datatext.NoMusic, 2000, "bottom");
                _this.myIframe.src = _this.param.urlLocal + '/' + " ";
                _this.TextArrow();
                _this.pauseLocal();
                _this.pause = true;
            }
            else {
                _this.currentLocalFile = _this.param.musiclist[_this.localindex].substring(0, 12);
                var element = _this.param.musiclist[_this.localindex];
                _this.myIframe.src = _this.param.urlLocal + '/' + element + ".mp3";
                console.log(_this.myIframe.src);
                _this.TextArrow();
                _this.playLocal();
                _this.pause = false;
            }
        }, 100);
    };
    HomePage.prototype.TextArrow = function () {
        if (this.param.musiclist.length == 0) {
            this.nextLocalFile = " ";
            this.previousLocalFile = " ";
        }
        else if (this.param.musiclist.length == 1) {
            this.nextLocalFile = this.param.musiclist[0].substring(0, 12);
            this.previousLocalFile = this.param.musiclist[0].substring(0, 12);
        }
        else {
            if (this.localindex == 0) {
                this.nextLocalFile = this.param.musiclist[this.localindex + 1].substring(0, 12);
                this.previousLocalFile = this.param.musiclist[this.param.musiclist.length - 1].substring(0, 12);
            }
            else if (this.localindex == this.param.musiclist.length - 1) {
                this.nextLocalFile = this.param.musiclist[0].substring(0, 12);
                this.previousLocalFile = this.param.musiclist[this.localindex - 1].substring(0, 12);
            }
            else {
                this.nextLocalFile = this.param.musiclist[this.localindex + 1].substring(0, 12);
                this.previousLocalFile = this.param.musiclist[this.localindex - 1].substring(0, 12);
            }
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("imgpersondetect"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], HomePage.prototype, "imgElement", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("canvas"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], HomePage.prototype, "canvas", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("canvaspersondetect"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], HomePage.prototype, "canvaspersondetect", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("select1"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["j" /* Select */])
    ], HomePage.prototype, "select1", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("select2"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["j" /* Select */])
    ], HomePage.prototype, "select2", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("select3"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["j" /* Select */])
    ], HomePage.prototype, "select3", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("select4"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["j" /* Select */])
    ], HomePage.prototype, "select4", void 0);
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-home",template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\tourneeapp\src\pages\home\home.html"*/'<!-- home page html template -->\n\n\n\n<ion-header no-border>\n\n  <headpage pageName="home"></headpage>\n\n</ion-header>\n\n\n\n<ion-content padding>\n\n  <ion-card text-center class="big_card">\n\n    <ion-grid class="heightstyle">\n\n      <ion-row class="heightstyle">\n\n        <ion-col col-3 class="heightstyle2">\n\n          <ion-item *ngIf="!api.roundActive && !api.towardDocking" class="itemstyle" text-wrap>\n\n            <ion-select #select1 slot="end" okText="{{this.param.datatext.btn_ok}}"\n\n              cancelText="{{this.param.datatext.btn_cancel}}" [selectOptions]="selectOptions" class="myCustomSelect"\n\n              placeholder="{{this.param.datatext.selectRound}}" [(ngModel)]="SelectRound" (ionChange)="onRoundChange()"\n\n              (mouseup)="onSliderRelease($event,select1)">\n\n              <ion-option *ngFor="let round of this.api.round_current_map" value="{{round.Id}}">{{round.Name}}\n\n              </ion-option>\n\n            </ion-select>\n\n          </ion-item>\n\n\n\n          <ion-item *ngIf="!api.roundActive && !api.towardDocking" class="itemstyle">\n\n            <button class="myCustomSelect50" (mouseup)="changetoRadio()" [class.active]="isRadioActive">{{this.param.datatext.radio}}</button>\n\n            <button class="myCustomSelect50" (mouseup)="changetoLocal()" [class.active]="isLocalActive">{{this.param.datatext.mp3}}</button>\n\n          </ion-item>          \n\n\n\n          <ion-item *ngIf="!api.roundActive && !api.towardDocking && radio==true" class="itemstyle" text-wrap>\n\n            <ion-select #select2 slot="end" okText="{{this.param.datatext.btn_ok}}"\n\n              cancelText="{{this.param.datatext.btn_cancel}}" [selectOptions]="selectRadio" class="myCustomSelect"\n\n              placeholder="{{this.param.datatext.radio}}" [(ngModel)]="SelectRadio" \n\n              (ionChange)="onRadioChange($event)" (mouseup)="onSliderRelease($event,select2)">\n\n              <ion-option *ngFor="let m of this.param.radiolist" value="{{m.idRadio}}">{{m.nameRadio}}\n\n              </ion-option>\n\n            </ion-select>\n\n          </ion-item>\n\n\n\n          <ion-item *ngIf="!api.roundActive && !api.towardDocking && radio==false" class="itemstyle" text-wrap>\n\n            <ion-select #select4 slot="end" okText="{{this.param.datatext.btn_ok}}"\n\n              cancelText="{{this.param.datatext.btn_cancel}}" [selectOptions]="selectLocal" class="myCustomSelect"\n\n              placeholder="Local" [(ngModel)]="SelectLocal" \n\n              (ionChange)="onLocalChange($event)" (mouseup)="onSliderRelease($event,select4)">\n\n              <ion-option *ngFor="let m of this.param.musicfolderlist" [value]="m">{{m}}</ion-option>\n\n            </ion-select>\n\n          </ion-item>\n\n\n\n          <ion-item *ngIf="!api.roundActive && !api.towardDocking" class="itemstyle" text-wrap>\n\n            <ion-select #select3 slot="end" okText="{{this.param.datatext.btn_ok}}"\n\n              cancelText="{{this.param.datatext.btn_cancel}}" [selectOptions]="selectDuration" class="myCustomSelect"\n\n              placeholder="{{this.param.datatext.roundDuration}}" [(ngModel)]="SelectDuration"\n\n              (ionChange)="onDurationChange()" (mouseup)="onSliderRelease($event,select3)">\n\n              <ion-option value="10">10 min</ion-option>\n\n              <ion-option value="20">20 min</ion-option>\n\n              <ion-option value="30">30 min</ion-option>\n\n              <ion-option value="40">40 min</ion-option>\n\n              <ion-option value="50">50 min</ion-option>\n\n              <ion-option value="60">60 min</ion-option>\n\n              <ion-option value="90">1h30</ion-option>\n\n              <ion-option value="120">2h</ion-option>\n\n              <ion-option value="300">5h</ion-option>\n\n              <ion-option value="420">7h</ion-option>\n\n            </ion-select>\n\n          </ion-item>\n\n\n\n          <ion-item class="itemstyle">\n\n            <ion-range pin="true" class="myCustomSelect" [(ngModel)]="volume" (ionChange)="onVolumeChange($event)" [min]="1">\n\n              <ion-icon range-left name="volume-mute"></ion-icon>\n\n              <ion-icon range-right name="volume-up"></ion-icon>\n\n            </ion-range>\n\n          </ion-item>\n\n\n\n          <button ion-button color="primary" class="btn_cam" (mouseup)="displaycam($event)">\n\n            <ion-icon *ngIf="!cam" color="light" name="camera" class="icon_cam"></ion-icon>\n\n            <ion-icon *ngIf="cam" color="light" name="musical-note" class="icon_cam"></ion-icon>\n\n          </button>\n\n\n\n          <ion-buttons class="div_btn_go">\n\n            <!-- the Go button is disabled if the robot is not connected -->\n\n            <button *ngIf="!api.roundActive && !api.towardDocking" ion-button color="secondary"\n\n              [disabled]="!api.is_connected || !this.SelectRound" (mouseup)="onclickGo($event)" class="btn_go">\n\n              <ion-icon color="light" name="play" class="icon_style">\n\n                <p class="legende">{{this.param.datatext.btn_go}}</p>\n\n              </ion-icon>\n\n            </button>\n\n            <button *ngIf="api.roundActive || api.towardDocking" ion-button color="danger" (mouseup)="onclickGo($event)"\n\n              class="btn_go">\n\n              <ion-icon color="light" name="hand" class="icon_style">\n\n                <p class="legende">\n\n                  {{this.param.datatext.btn_stop}}\n\n                </p>\n\n              </ion-icon>\n\n            </button>\n\n          </ion-buttons>\n\n        </ion-col>\n\n\n\n        <ion-col col-9>\n\n          <div class="container vh-100">\n\n            <div class="d-flex flex-column align-items-center">\n\n              <div class="p-1">\n\n                <canvas #canvas class="vid" style="display: none"> </canvas>\n\n              </div>\n\n            </div>\n\n          </div>\n\n          <div id="mute"></div>\n\n          <div class="video-responsive">\n\n            \n\n            <p [hidden]="cam" *ngIf="radio==true" class="nameRadio">{{ this.currentRadio.nameRadio }}</p> \n\n            <p [hidden]="cam" *ngIf="radio==false" class="nameLocal">{{this.param.datatext.folder}} : {{ this.SelectLocal }}</p>\n\n            <p [hidden]="cam" *ngIf="radio==false && !this.param.musiclist.length == 0" class="nameLocal">{{this.param.datatext.music}} : {{ this.currentLocalFile }}</p>\n\n            <p [hidden]="cam" *ngIf="radio==false && this.param.musiclist.length == 0" class="nameLocal">{{this.param.datatext.NoMusic}}</p>\n\n            <img [hidden]="cam" id="gifRadio" src="" (mouseup)="pauseRadio()" class="gifRadio" alt="gif audio">\n\n            <img [hidden]="cam" id="gifPause" src="" (mouseup)="pauseRadio()" class="gifPause" alt="icon stop and play">\n\n            <img [hidden]="cam" *ngIf="radio==false" src="assets/imgs/leftskip.png" (mouseup)="playPreviousTrack()" class="leftArrow" alt="left arrow">\n\n            <p [hidden]="cam" *ngIf="radio==false" class="leftArrowText" alt="previous track">{{this.previousLocalFile}}</p>\n\n            <img [hidden]="cam" *ngIf="radio==false" src="assets/imgs/rightskip.png" (mouseup)="playNextTrack()" class="rightArrow" alt="right arrow">\n\n            <p [hidden]="cam" *ngIf="radio==false" class="rightArrowText" alt="next track">{{this.nextLocalFile}}</p>\n\n            <audio id="iframe" src="" autoplay></audio>\n\n\n\n            <img [hidden]="!cam" class="video" id="imgcompressed" />\n\n            <div [hidden]="!cam" class="joy" id="zone_joystick"></div>\n\n          </div>\n\n        </ion-col>\n\n\n\n      </ion-row>\n\n    </ion-grid>\n\n  </ion-card>\n\n  <img id="imgpersondetect" #imgpersondetect src={{detection_url}} crossorigin="anonymous" class="video"\n\n    style="display:none;" />\n\n  <canvas #canvaspersondetect style="display:none;"> </canvas>\n\n</ion-content>'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\tourneeapp\src\pages\home\home.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_7__angular_platform_browser__["c" /* DomSanitizer */],
            __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["k" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_3__services_popup_service__["a" /* PopupService */],
            __WEBPACK_IMPORTED_MODULE_1__services_api_service__["a" /* ApiService */],
            __WEBPACK_IMPORTED_MODULE_5__services_speech_service__["a" /* SpeechService */],
            __WEBPACK_IMPORTED_MODULE_4__services_param_service__["a" /* ParamService */],
            __WEBPACK_IMPORTED_MODULE_2__services_alert_service__["a" /* AlertService */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["W" /* Renderer2 */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 116:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AnnoncePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_param_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_speech_service__ = __webpack_require__(51);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AnnoncePage = /** @class */ (function () {
    function AnnoncePage(loadingCtrl, param, speech, toastCtrl) {
        this.loadingCtrl = loadingCtrl;
        this.param = param;
        this.speech = speech;
        this.toastCtrl = toastCtrl;
        this.value = "";
        this.selectFreq = {
            title: this.param.datatext.freqAnnounce,
        };
        this.SelectFreq = this.param.speak_freq;
    }
    AnnoncePage.prototype.ngOnInit = function () {
    };
    AnnoncePage.prototype.errorToast = function (m) {
        var toast = this.toastCtrl.create({
            message: m,
            duration: 3000,
            position: 'middle',
            cssClass: "toast"
        });
        toast.present();
    };
    AnnoncePage.prototype.onFreqChange = function (event) {
        this.param.speak_freq = event;
        this.param.updateRoundParam();
    };
    AnnoncePage.prototype.okToast = function (m) {
        var toast = this.toastCtrl.create({
            message: m,
            duration: 3000,
            position: 'middle',
            cssClass: "toastok"
        });
        toast.present();
    };
    AnnoncePage.prototype.onToggleWeather = function () {
        if (this.param.sayweather == 1) {
            this.param.sayweather = 0;
        }
        else {
            this.param.sayweather = 1;
        }
        this.param.updateRoundParam();
    };
    AnnoncePage.prototype.showLoading = function () {
        if (!this.loading) {
            this.loading = this.loadingCtrl.create({
                content: this.param.datatext.addLoading,
            });
            this.loading.present();
        }
    };
    AnnoncePage.prototype.onToggleHour = function () {
        if (this.param.sayhour == 1) {
            this.param.sayhour = 0;
        }
        else {
            this.param.sayhour = 1;
        }
        this.param.updateRoundParam();
    };
    AnnoncePage.prototype.onToggleAnnonce = function (annonce) {
        var index = this.param.announcement.indexOf(annonce);
        if (this.param.announcement[index].activated == 1) {
            this.param.announcement[index].activated = 0;
        }
        else {
            this.param.announcement[index].activated = 1;
        }
        console.log(this.param.announcement[index]);
        this.param.updateAnnonce(this.param.announcement[index]);
    };
    AnnoncePage.prototype.removeAnnonce = function (ev, annonce) {
        ev.preventDefault();
        this.param.deleteAnnonce(annonce.id_annonce);
        var index = this.param.announcement.indexOf(annonce);
        if (index !== -1) {
            this.param.announcement.splice(index, 1);
            this.okToast(this.param.datatext.mailRemove);
        }
    };
    AnnoncePage.prototype.is_a_text = function () {
        return this.value.length > 2;
    };
    AnnoncePage.prototype.sayAnnonce = function (ev) {
        ev.preventDefault();
        this.value = this.value.replace(new RegExp("[<>&$§µ£#*_|`¤~(){}°²@%/]", "g"), '');
        this.value = this.value.replace(new RegExp("[\"]", "g"), '');
        this.value = this.value.replace(new RegExp(/\\/, "g"), '');
        this.value = this.value.replace(new RegExp(/[\[\]]/, "g"), '');
        console.log("sayannonce");
        this.speech.speak(this.value);
    };
    AnnoncePage.prototype.dismissLoading = function () {
        if (this.loading) {
            this.loading.dismiss();
            this.loading = null;
        }
    };
    AnnoncePage.prototype.addAnnonce = function (ev) {
        var _this = this;
        ev.preventDefault();
        this.showLoading();
        //this.value=this.value.replace(new RegExp("[<>&$µ£#~`¤|()-_=+}{/*§}°²@%€]", "g"),'');
        this.value = this.value.replace(new RegExp("[<>&$§µ£#*_|`¤~(){}°²@%/]", "g"), '');
        this.value = this.value.replace(new RegExp("[\"]", "g"), '');
        this.value = this.value.replace(new RegExp(/\\/, "g"), '');
        this.value = this.value.replace(new RegExp(/[\[\]]/, "g"), '');
        this.param.addAnnonce(this.value);
        setTimeout(function () {
            _this.param.getDataAnnouncement();
            _this.dismissLoading();
            _this.value = '';
            _this.okToast(_this.param.datatext.addDone);
            setTimeout(function () {
                _this.content1.scrollToBottom();
            }, 300);
        }, 3000);
    };
    AnnoncePage.prototype.editAnnonce = function (ev, annonce) {
        ev.preventDefault();
        this.value = annonce.speech;
        this.sayAnnonce(ev);
    };
    AnnoncePage.prototype.onSliderRelease = function (ev, id) {
        ev.preventDefault();
        id.open();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("select1"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* Select */])
    ], AnnoncePage.prototype, "select1", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("content1"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* Content */])
    ], AnnoncePage.prototype, "content1", void 0);
    AnnoncePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-annonce',template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\tourneeapp\src\pages\annonce\annonce.html"*/'<!-- mail page html template -->\n\n<ion-header no-border>\n\n  <headpage pageName="annonce"></headpage>\n\n</ion-header>\n\n\n\n<ion-content class="big-ion-content" padding>\n\n  <ion-card text-center class="big_card">\n\n    <ion-grid class="heightstyle">\n\n      <ion-row>\n\n        <ion-col col-6>\n\n          <ion-item text-wrap>\n\n            <ion-label class="txt">{{this.param.datatext.freq}}</ion-label>\n\n            <ion-select #select1 slot="end" okText="{{this.param.datatext.btn_ok}}"\n\n              cancelText="{{this.param.datatext.btn_cancel}}" [selectOptions]="selectFreq" [(ngModel)]="SelectFreq"\n\n              placeholder="{{this.param.datatext.freqAnnounce}}" (ionChange)="onFreqChange($event)"\n\n              (mouseup)="onSliderRelease($event,select1)">\n\n              <ion-option value="30">{{this.param.datatext.every30sec}}</ion-option>\n\n              <ion-option value="60">{{this.param.datatext.every1mn}}</ion-option>\n\n              <ion-option value="120">{{this.param.datatext.every2mn}}</ion-option>\n\n              <ion-option value="180">{{this.param.datatext.every3mn}}</ion-option>\n\n              <ion-option value="240">{{this.param.datatext.every4mn}}</ion-option>\n\n              <ion-option value="300">{{this.param.datatext.every5mn}}</ion-option>\n\n            </ion-select>\n\n          </ion-item>\n\n        </ion-col>\n\n        <ion-col col-3>\n\n          <ion-item>\n\n            <ion-label>{{this.param.datatext.weather}}</ion-label>\n\n            <ion-toggle [checked]="this.param.sayweather ==1 " (ionChange)="onToggleWeather()" item-end></ion-toggle>\n\n\n\n            <ion-icon class="iconscss" name="sunny" item-start color="primary"></ion-icon>\n\n          </ion-item>\n\n        </ion-col>\n\n        <ion-col col-3>\n\n          <ion-item>\n\n            <ion-label>{{this.param.datatext.date}}</ion-label>\n\n            <ion-toggle [checked]="this.param.sayhour ==1 " (ionChange)="onToggleHour()" item-end></ion-toggle>\n\n            <ion-icon class="iconscss" name="time" item-start color="primary"></ion-icon>\n\n          </ion-item>\n\n        </ion-col>\n\n      </ion-row>\n\n\n\n      <ion-list class="separation">\n\n        <ion-item>\n\n        \n\n          <ion-input type="text" maxlength="3000" [(ngModel)]="value" placeholder="{{this.param.datatext.newAnnouncement}}"></ion-input>\n\n          <button *ngIf="is_a_text()" class="listen" (mouseup)="sayAnnonce($event)" item-end>\n\n            <ion-icon color="light" name="volume-down"></ion-icon>\n\n          </button>\n\n          <button *ngIf="is_a_text()" class="btn-add" (mouseup)="addAnnonce($event)" item-end>\n\n            <ion-icon color="light" name="add"></ion-icon>\n\n          </button>\n\n          <button *ngIf="!(is_a_text())" class="mailnotvalid" (mouseup)="addAnnonce($event)" item-end>\n\n            <ion-icon color="light" name="add"></ion-icon>\n\n          </button>\n\n        </ion-item>\n\n      </ion-list>\n\n\n\n      <ion-content #content1 style="background-color: transparent;">\n\n        <ion-row class="annoncedisplay" *ngFor="let m of this.param.announcement">\n\n          <ion-col col-1>\n\n            <ion-icon class="iconscss" name="chatboxes" item-start color="primary"></ion-icon>\n\n          </ion-col>\n\n          <ion-col class="colannonce" col-9>\n\n            <button class="btn-annonce" (mouseup)="editAnnonce($event,m)">\n\n              {{m.speech}}\n\n            </button>\n\n          </ion-col>\n\n          <ion-col col-1>\n\n            <button class="btn-trash" item-end (mouseup)="removeAnnonce($event,m)">\n\n              <ion-icon class="btn-trash" name="trash"></ion-icon>\n\n            </button>\n\n          </ion-col>\n\n          <ion-col col-1>\n\n            <ion-toggle [checked]="m.activated ==1 " (ionChange)="onToggleAnnonce(m)"></ion-toggle>\n\n          </ion-col>\n\n        </ion-row>\n\n      </ion-content>\n\n    </ion-grid>\n\n  </ion-card>\n\n</ion-content>'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\tourneeapp\src\pages\annonce\annonce.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1__services_param_service__["a" /* ParamService */], __WEBPACK_IMPORTED_MODULE_3__services_speech_service__["a" /* SpeechService */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* ToastController */]])
    ], AnnoncePage);
    return AnnoncePage;
}());

//# sourceMappingURL=annonce.js.map

/***/ }),

/***/ 120:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_param_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_popup_service__ = __webpack_require__(31);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var MailPage = /** @class */ (function () {
    function MailPage(param, popup, toastCtrl) {
        this.param = param;
        this.popup = popup;
        this.toastCtrl = toastCtrl;
        this.inputmail = "";
    }
    MailPage.prototype.ngOnInit = function () {
        this.popup.onSomethingHappened2(this.addMail.bind(this));
    };
    MailPage.prototype.errorToast = function (m) {
        var toast = this.toastCtrl.create({
            message: m,
            duration: 3000,
            position: 'middle',
            cssClass: "toast"
        });
        toast.present();
    };
    MailPage.prototype.okToast = function (m) {
        var toast = this.toastCtrl.create({
            message: m,
            duration: 3000,
            position: 'middle',
            cssClass: "toastok"
        });
        toast.present();
    };
    MailPage.prototype.removeMail = function (ev, m) {
        ev.preventDefault();
        var index = this.param.maillist.indexOf(m);
        if (index !== -1) {
            this.param.maillist.splice(index, 1);
            this.param.deleteMail(m);
            this.okToast(this.param.datatext.mailRemove);
        }
    };
    MailPage.prototype.is_a_mail = function (m) {
        var mailformat = "[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$";
        return m.match(mailformat);
    };
    MailPage.prototype.addMail = function (ev) {
        var _this = this;
        ev.preventDefault();
        if (this.is_a_mail(this.inputmail)) {
            var index = this.param.maillist.indexOf(this.inputmail);
            if (index !== -1) {
                this.errorToast(this.param.datatext.mailexist);
            }
            else {
                this.param.maillist.push(this.inputmail);
                this.param.addMail(this.inputmail);
                this.okToast(this.param.datatext.mailadd);
                setTimeout(function () {
                    _this.content1.scrollToBottom();
                }, 300);
            }
        }
        else {
            this.errorToast(this.param.datatext.mailincorrect);
        }
    };
    MailPage.prototype.update_send_pic = function () {
        if (this.param.robot.send_pic == 1) {
            this.param.robot.send_pic = 0;
        }
        else {
            this.param.robot.send_pic = 1;
        }
        this.param.updateRobot();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("content1"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* Content */])
    ], MailPage.prototype, "content1", void 0);
    MailPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-mail',template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\tourneeapp\src\pages\mail\mail.html"*/'<!-- mail page html template -->\n\n<ion-header no-border>\n\n  <headpage pageName="mail"></headpage>\n\n</ion-header>\n\n\n\n\n\n<ion-content class="big-ion-content" padding >\n\n  <ion-card text-center class="big_card" >\n\n    <ion-grid class="heightstyle">\n\n\n\n      \n\n      <ion-item >\n\n        <ion-label class="labelcss" stacked>{{param.datatext.receivepic}}</ion-label>\n\n        <ion-toggle\n\n              [checked]="this.param.robot.send_pic ==1 "\n\n              (ionChange)="update_send_pic()"\n\n              item-end\n\n            ></ion-toggle>\n\n            <ion-icon\n\n              class="iconscss"\n\n              name="camera"\n\n              item-start\n\n              color="primary"\n\n            ></ion-icon>\n\n      </ion-item>\n\n\n\n        <ion-item class="itemail">\n\n          <ion-label class="txt" stacked>{{param.datatext.newmail}}</ion-label>\n\n          <ion-input class="inputmail" inputmode="email" [(ngModel)]="inputmail" placeholder="kompai@gmail.com"></ion-input>\n\n          <button *ngIf="is_a_mail(inputmail)" class="btn-add" (mouseup)="popup.displayrgpd($event)" item-end>\n\n            <ion-icon  color="light" name="add"></ion-icon>\n\n          </button>\n\n          <button *ngIf="!(is_a_mail(inputmail))" class="mailnotvalid" (mouseup)="addMail($event)" item-end>\n\n            <ion-icon  color="light" name="add"></ion-icon>\n\n          </button>\n\n        </ion-item>\n\n       \n\n        <ion-content\n\n        #content1\n\n        style="height: 80%; width: 100%; background-color: transparent;"\n\n      >\n\n        \n\n          <ion-item *ngFor="let m of this.param.maillist">\n\n            <ion-icon class="iconscss" name="at" item-start color="primary"></ion-icon>{{m}}\n\n            <button class="btn-trash" item-end (mouseup)="removeMail($event,m)">\n\n              <ion-icon class="btn-trash" name="trash"></ion-icon>\n\n            </button>\n\n          </ion-item>\n\n        \n\n        </ion-content>\n\n    \n\n    </ion-grid> \n\n\n\n</ion-card>\n\n \n\n\n\n</ion-content>\n\n\n\n'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\tourneeapp\src\pages\mail\mail.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_param_service__["a" /* ParamService */], __WEBPACK_IMPORTED_MODULE_3__services_popup_service__["a" /* PopupService */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* ToastController */]])
    ], MailPage);
    return MailPage;
}());

//# sourceMappingURL=mail.js.map

/***/ }),

/***/ 131:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 131;

/***/ }),

/***/ 173:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 173;

/***/ }),

/***/ 216:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_api_service__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_first_first__ = __webpack_require__(218);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_alert_service__ = __webpack_require__(35);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var MyApp = /** @class */ (function () {
    //rootPage:any =  TestYoutubePage;
    function MyApp(platform, statusBar, splashScreen, api, alert) {
        var _this = this;
        this.api = api;
        this.alert = alert;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_5__pages_first_first__["a" /* FirstPage */];
        // This code loads the IFrame Player API code asynchronously.
        var tag = document.createElement('script');
        tag.src = "https://www.youtube.com/iframe_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
            platform.pause.subscribe(function () {
                _this.api.pauseHttp(false);
                _this.api.deleteEyesHttp(23);
                _this.api.background();
                window.close();
            });
            platform.resume.subscribe(function () {
            });
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\tourneeapp\src\app\app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n\n'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\tourneeapp\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_4__services_api_service__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_6__services_alert_service__["a" /* AlertService */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 218:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FirstPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_param_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_api_service__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_alert_service__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__home_home__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__annonce_annonce__ = __webpack_require__(116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_popup_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_speech_service__ = __webpack_require__(51);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var FirstPage = /** @class */ (function () {
    function FirstPage(loadingCtrl, popup, param, alert, speech, api, navCtrl) {
        this.loadingCtrl = loadingCtrl;
        this.popup = popup;
        this.param = param;
        this.alert = alert;
        this.speech = speech;
        this.api = api;
        this.navCtrl = navCtrl;
        this.homePage = __WEBPACK_IMPORTED_MODULE_5__home_home__["a" /* HomePage */];
        this.annoncePage = __WEBPACK_IMPORTED_MODULE_6__annonce_annonce__["a" /* AnnoncePage */];
        this.cpt_open = 0;
        this.cpt_battery = 0;
        this.loading = this.loadingCtrl.create({});
        this.loading.present();
    }
    FirstPage.prototype.ngOnInit = function () {
        var _this = this;
        this.appopen = setInterval(function () { return _this.appOpen(); }, 1000);
        navigator.geolocation.getCurrentPosition(function (position) {
            var _a = position.coords, latitude = _a.latitude, longitude = _a.longitude;
            // Show a map centered at latitude / longitude.
            _this.lat = latitude;
            _this.long = longitude;
        });
    };
    FirstPage.prototype.appOpen = function () {
        if (this.param.localhost) {
            this.api.checkrobot();
        }
        this.api.checkInternet();
        this.alert.checkwifi(this.api.wifiok);
        this.cpt_open += 1;
        if (this.cpt_open === 15 && !this.api.robotok) {
            this.speech.speak(this.param.datatext.cantstart);
            this.popup.startFailedAlert();
        }
        else if (this.cpt_open === 15 && this.api.robotok) {
            this.popup.relocAlert();
            this.speech.speak(this.param.datatext.relocAlert_title + " " + this.param.datatext.relocAlert_message);
        }
        if (!this.api.appOpened) {
            this.param.getDataRobot();
            this.param.getMail();
            this.param.getradio();
            this.param.getMusicFolders();
            this.param.getParamRound();
            //this.param.getPhone();
            this.param.getBattery();
            this.param.getDataAnnouncement();
            this.param.getDurationNS();
            this.param.getDataPatrol();
            this.param.getDataPlaylist();
            if (this.param.announcement &&
                this.param.playlist &&
                this.param.duration &&
                this.param.robot &&
                this.param.maillist) {
                this.param.fillData();
                this.speech.getVoice();
                if (this.param.robot.httpskomnav == 0) {
                    this.api.httpskomnav = "http://";
                    this.api.wsskomnav = "ws://";
                }
                else {
                    this.api.httpskomnav = "https://";
                    this.api.wsskomnav = "wss://";
                }
                if (this.param.robot.httpsros == 0) {
                    this.api.wssros = "ws://";
                }
                else {
                    this.api.wssros = "wss://";
                }
            }
            if (this.api.robotok && this.param.langage) {
                if (!this.api.socketok) {
                    this.api.instanciate();
                }
                this.api.getCurrentMap();
                this.api.getRoundList();
                this.param.getreservations();
                if (this.api.mapdata) {
                    this.api.eyesHttp(4);
                    this.api.appOpened = true;
                }
            }
        }
        else if (this.api.appOpened && this.cpt_open >= 5) {
            if (this.lat) {
                this.param.paramround.gpslat = this.lat;
                this.param.paramround.gpslong = this.long;
                this.param.updatelocation();
            }
            this.api.abortNavHttp();
            this.alert.appOpen(this.api.mailAddInformationBasic());
            if (this.api.round_current_map) {
                if (this.param.patrolInfo.round_mask_detection == 1) {
                    this.api.startMaskDetection(true);
                }
                if (this.param.patrolInfo.round_fall_detection == 1) {
                    this.api.startPeopleDetection(true);
                }
                //console.log(this.param.announcement);
                this.navCtrl.setRoot(this.homePage);
                this.loading.dismiss();
                //console.log(this.popup.alert_blocked);
                if (this.popup.alert_blocked) {
                    this.popup.alert_blocked.dismiss();
                }
                clearInterval(this.appopen);
                console.log(this.cpt_open);
            }
            else {
                this.api.getRoundList();
            }
        }
    };
    FirstPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: "page-first",template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\tourneeapp\src\pages\first\first.html"*/'<ion-header no-border>\n\n \n\n</ion-header>\n\n<ion-content padding >\n\n  <ion-card text-center class="big_card" >\n\n      <ion-grid class="heightstyle">\n\n       \n\n      </ion-grid> \n\n\n\n  </ion-card>\n\n \n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\tourneeapp\src\pages\first\first.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_7__services_popup_service__["a" /* PopupService */],
            __WEBPACK_IMPORTED_MODULE_2__services_param_service__["a" /* ParamService */],
            __WEBPACK_IMPORTED_MODULE_4__services_alert_service__["a" /* AlertService */],
            __WEBPACK_IMPORTED_MODULE_8__services_speech_service__["a" /* SpeechService */],
            __WEBPACK_IMPORTED_MODULE_3__services_api_service__["a" /* ApiService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */]])
    ], FirstPage);
    return FirstPage;
}());

//# sourceMappingURL=first.js.map

/***/ }),

/***/ 219:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HeadpageComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__pages_home_home__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_tuto_tuto__ = __webpack_require__(220);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_api_service__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_alert_service__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_popup_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_speech_service__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__services_param_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_param_param__ = __webpack_require__(231);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_mail_mail__ = __webpack_require__(120);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_platform_browser__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_autolaunch_autolaunch__ = __webpack_require__(237);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};














var HeadpageComponent = /** @class */ (function () {
    function HeadpageComponent(speech, toastCtrl, navCtrl, param, api, alert, popup, sanitizer) {
        this.speech = speech;
        this.toastCtrl = toastCtrl;
        this.navCtrl = navCtrl;
        this.param = param;
        this.api = api;
        this.alert = alert;
        this.popup = popup;
        this.sanitizer = sanitizer;
        this.homePage = __WEBPACK_IMPORTED_MODULE_1__pages_home_home__["a" /* HomePage */];
        this.tutoPage = __WEBPACK_IMPORTED_MODULE_2__pages_tuto_tuto__["a" /* TutoPage */];
        this.paramPage = __WEBPACK_IMPORTED_MODULE_9__pages_param_param__["a" /* ParamPage */];
        this.mailPage = __WEBPACK_IMPORTED_MODULE_10__pages_mail_mail__["a" /* MailPage */];
        this.autolaunchPage = __WEBPACK_IMPORTED_MODULE_12__pages_autolaunch_autolaunch__["a" /* AutoLaunchPage */];
        this.optiondateofday = { weekday: 'long', day: 'numeric', month: 'long' };
        this.microon = false;
        this.cptsos = 0;
        this.cpt_battery = 0;
        this.cpt_open = 0;
        if (this.param.allowspeech == 1) {
            this.volumeState = 1;
        }
        else {
            this.volumeState = 0;
        }
    }
    HeadpageComponent.prototype.ionViewWillLeave = function () {
        clearInterval(this.update);
        clearInterval(this.lowbattery);
        clearInterval(this.source);
    };
    HeadpageComponent.prototype.ngOnDestroy = function () {
        clearInterval(this.update);
        clearInterval(this.lowbattery);
        clearInterval(this.source);
    };
    HeadpageComponent.prototype.SOSsend = function () {
        if (this.cptsos === 1) {
            this.alert.SOS_c();
            this.alert.SOS(this.api.mailAddInformationBasic());
        }
        if (this.api.sos_pressed) {
            this.cptsos += 1;
        }
        if (this.cptsos === 15) {
            this.api.sos_pressed = false;
            this.cptsos = 0;
        }
    };
    HeadpageComponent.prototype.updateBattery = function () {
        if (this.api.statusRobot === 2) {
            // battery logo will not apppear
            this.api.batteryState = 10;
        }
        else if (this.api.battery_status.status < 2) {
            //charging
            this.api.batteryState = 4;
        }
        else if (this.api.battery_status.status === 3 ||
            this.api.battery_status.remaining <= this.param.battery.critical) {
            //critical
            this.api.batteryState = 0;
        }
        else if (this.api.battery_status.remaining > this.param.battery.high) {
            //hight
            this.api.batteryState = 3;
        }
        else if (this.api.battery_status.remaining > this.param.battery.low) {
            //mean
            this.api.batteryState = 2;
        }
        else if (this.api.battery_status.remaining <= this.param.battery.low) {
            //low
            this.api.batteryState = 1;
        }
        else {
            // battery logo will not apppear
            this.api.batteryState = 10;
        }
    };
    HeadpageComponent.prototype.onTouchWifi = function (ev) {
        ev.preventDefault();
        if (!this.api.wifiok) {
            this.okToast(this.param.datatext.nointernet);
        }
        else {
            this.okToast(this.param.datatext.AlertConnectedToI);
        }
    };
    HeadpageComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.popup.onSomethingHappened1(this.accessparam.bind(this));
        this.popup.onSomethingHappened3(this.accessautolaunch.bind(this));
        // bouton 's text (Return / Exit), to the left of the header, depends on the page you are on
        if (this.pageName === "home") {
            this.update = setInterval(function () { return _this.getUpdate(); }, 1000); //update hour, battery every 1/2 secondes
            this.lowbattery = setInterval(function () { return _this.monitoringBattery(); }, 60000);
            this.source = setInterval(function () { return _this.api.jetsonOK(); }, 2000);
            this.textBoutonHeader = this.param.datatext.quit;
            this.textHeadBand = this.param.datatext.round;
        }
        else if (this.pageName === "param") {
            this.textBoutonHeader = this.param.datatext.return;
            this.textHeadBand = this.param.datatext.param;
        }
        else if (this.pageName === "sms") {
            this.textBoutonHeader = this.param.datatext.return;
            this.textHeadBand = this.param.datatext.receivesms;
        }
        else if (this.pageName === "mail") {
            this.textBoutonHeader = this.param.datatext.return;
            this.textHeadBand = this.param.datatext.receivemail;
        }
        else if (this.pageName === "langue") {
            this.textBoutonHeader = this.param.datatext.return;
            this.textHeadBand = this.param.datatext.changelangage;
        }
        else if (this.pageName === "annonce") {
            this.textBoutonHeader = this.param.datatext.return;
            this.textHeadBand = this.param.datatext.announceEdit;
        }
        else if (this.pageName === "password") {
            this.textBoutonHeader = this.param.datatext.return;
            this.textHeadBand = this.param.datatext.editpswd;
        }
        else if (this.pageName === "behaviour") {
            this.textBoutonHeader = this.param.datatext.return;
            this.textHeadBand = this.param.datatext.robotBehaviour;
        }
        else if (this.pageName === "autolaunch") {
            this.textBoutonHeader = this.param.datatext.return;
            this.textHeadBand = this.param.datatext.autolaunchHeadPage;
        }
        else if (this.pageName === "radio") {
            this.textBoutonHeader = this.param.datatext.return;
            this.textHeadBand = this.param.datatext.radioHeadPage;
        }
        else {
            this.textBoutonHeader = this.param.datatext.return;
            this.textHeadBand = this.param.datatext.tutorial;
        }
    };
    HeadpageComponent.prototype.accessparam = function () {
        this.navCtrl.push(this.paramPage);
    };
    HeadpageComponent.prototype.accessautolaunch = function () {
        this.navCtrl.push(this.autolaunchPage);
    };
    HeadpageComponent.prototype.onTouchAutoLaunch = function (ev) {
        ev.preventDefault();
        if (this.pageName === "home")
            this.popup.askpswd("autolaunch");
        else if (this.pageName == "autolaunch")
            console.log("already");
        else
            this.accessautolaunch();
    };
    HeadpageComponent.prototype.onTouchParam = function (ev) {
        ev.preventDefault();
        if (!this.api.roundActive && !this.api.towardDocking) {
            if (this.pageName === "home") {
                this.popup.askpswd("param");
            }
            else if (this.pageName == "autolaunch")
                this.accessparam();
            else {
                //this.navCtrl.push(this.paramPage);
                console.log("already");
                ;
            }
        }
    };
    HeadpageComponent.prototype.onTouchStatus = function (ev) {
        ev.preventDefault();
        if (this.api.statusRobot === 2) {
            this.popup.statusRedPresent();
        }
        else {
            this.okToast(this.param.datatext.statusGreenPresent_message);
        }
    };
    HeadpageComponent.prototype.okToast = function (m) {
        var toast = this.toastCtrl.create({
            message: m,
            duration: 3000,
            position: 'middle',
            cssClass: "toastok"
        });
        toast.present();
    };
    HeadpageComponent.prototype.monitoringBattery = function () {
        var _this = this;
        if (this.api.batteryState === 0) {
            this.popup.lowBattery();
            this.alert.lowBattery(this.api.mailAddInformationBasic());
            this.alert.Battery_c();
            if (!this.api.towardDocking) {
                this.api.pauseHttp(false);
                this.api.towardDocking = true;
                setTimeout(function () {
                    _this.api.reachHttp("docking");
                }, 2000);
            }
            clearInterval(this.lowbattery);
        }
    };
    HeadpageComponent.prototype.onTouchVolume = function (ev) {
        ev.preventDefault();
        this.speech.synth.cancel();
        if (this.volumeState === 0) {
            this.volumeState = 1;
            this.param.allowspeech = 1;
        }
        else {
            this.volumeState = 0;
            this.param.allowspeech = 0;
        }
    };
    HeadpageComponent.prototype.onTouchBattery = function (ev) {
        ev.preventDefault();
        if (!this.api.roundActive && !this.api.towardDocking) {
            if (this.pageName === "home") {
                if (this.api.docking_status.status != 3) {
                    if (this.api.batteryState === 0) {
                        this.popup.lowBattery();
                    }
                    else {
                        this.popup.goDockingConfirm();
                    }
                }
                if (this.api.docking_status.status === 3) {
                    this.popup.leaveDockingConfirm();
                }
            }
            else {
                this.clicOnMenu();
            }
        }
        else {
            this.okToast(this.param.datatext.battery + " : " + this.api.battery_status.remaining + "%");
        }
    };
    HeadpageComponent.prototype.getUpdate = function () {
        //update hour, battery
        this.SOSsend();
        this.updateBattery();
        this.updateBand();
        var now = new Date().toLocaleString('fr-FR', { hour: 'numeric', minute: 'numeric' });
        this.api.hour = now;
        //console.log(this.batteryState);
        if (this.api.statusRobot === 0 && this.api.batteryState === 10) {
            this.cpt_battery += 1;
        }
        else {
            this.cpt_battery = 0;
        }
        if (this.cpt_battery > 10) {
            this.api.statusRobot = 2;
        }
    };
    HeadpageComponent.prototype.updateBand = function () {
        if (this.api.is_blocked || this.api.is_high) {
            this.textHeadBand = this.param.datatext.errorBlocked_title.toUpperCase();
        }
        else if (this.api.towardDocking) {
            this.textHeadBand = this.param.datatext.godocking;
        }
        else if (this.api.roundActive) {
            this.textHeadBand = this.param.datatext.roundInProgress;
        }
        else if (this.pageName === "home") {
            this.textHeadBand = this.param.datatext.round;
        }
    };
    HeadpageComponent.prototype.onTouchHelp = function (ev) {
        ev.preventDefault();
        if (!this.api.roundActive && !this.api.towardDocking) {
            if (!(this.pageName === "tuto")) {
                this.alert.displayTuto(this.api.mailAddInformationBasic());
                this.navCtrl.push(this.tutoPage);
            }
        }
    };
    HeadpageComponent.prototype.onquit = function (ev) {
        ev.preventDefault();
        this.clicOnMenu();
    };
    HeadpageComponent.prototype.clicOnMenu = function () {
        if (this.pageName === 'tuto' || this.pageName === 'param' || (this.pageName === 'autolaunch' && !this.api.addMission)) {
            this.navCtrl.popToRoot();
        }
        else if (this.pageName === 'sms' || this.pageName === 'mail' || this.pageName === 'langue' || this.pageName === 'annonce' || this.pageName === "password" || this.pageName === "behaviour" || this.pageName === 'radio') {
            this.navCtrl.pop();
        }
        else if (this.pageName === 'autolaunch' && this.api.addMission)
            this.api.addMission = !this.api.addMission;
        else {
            if (this.api.roundActive || this.api.towardDocking) {
                this.api.abortNavHttp();
                this.api.roundActive = false;
                this.api.towardDocking = false;
                this.popup.quitConfirm();
            }
            else {
                this.api.close_app = true;
                this.alert.appClosed(this.api.mailAddInformationBasic());
                //stop the round and quit the app
                this.api.abortHttp();
                this.api.deleteEyesHttp(23);
                this.api.startPeopleDetection(false);
                this.api.startMaskDetection(false);
                setTimeout(function () {
                    window.close();
                }, 1000);
            }
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], HeadpageComponent.prototype, "pageName", void 0);
    HeadpageComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'headpage',template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\tourneeapp\src\components\headpage\headpage.html"*/'<!-- Html component of the HMI header with the Back / Exit button, the time, the sound buttons, microphone, parameters etc ...-->\n\n\n\n<ion-navbar hideBackButton>\n\n\n\n  <ion-buttons left class="btn_menu_size">\n\n    <button ion-button solid large class="btn_menu" (mouseup)="onquit($event)">\n\n      <font class="font_menu_size">{{this.textBoutonHeader}}</font>\n\n    </button>\n\n  </ion-buttons>\n\n\n\n  <ion-title text-center>\n\n    <font class="hour_size">{{api.hour}}</font>\n\n  </ion-title>\n\n\n\n  <ion-buttons right>\n\n    <button ion-button class="btn_header" (mouseup)="onTouchAutoLaunch($event)" >\n\n      <img class="imgicon" src="./assets/imgs/autolaunch.png" />\n\n    </button>\n\n    <button ion-button class="btn_header" (mouseup)="onTouchHelp($event)" >\n\n      <img class="imgicon" src="./assets/imgs/help.png" />\n\n    </button>\n\n    <button ion-button class="btn_header" (mouseup)="onTouchParam($event)" >\n\n      <img class="imgicon" src="./assets/imgs/parameter.png" />\n\n    </button>\n\n    <!-- <button ion-button disabled class="btn_header"  >\n\n      <img *ngIf="!microon" class="imgicon" src="./assets/imgs/microoff.png"/>\n\n      <img *ngIf="microon" class="imgicon" src="./assets/imgs/microon.png"/>\n\n    </button> -->\n\n\n\n    <button ion-button class="btn_header" (mouseup)="onTouchWifi($event)" >\n\n      <img *ngIf="!api.wifiok" class="imginternet" src="./assets/imgs/wifioff.png" />\n\n      <img *ngIf="api.wifiok" class="imginternet" src="./assets/imgs/wifion.png" />\n\n    </button>\n\n\n\n    <button ion-button class="btn_header" (mouseup)="onTouchVolume($event)" >\n\n      <img *ngIf="volumeState === 0" class="imgicon" src="./assets/imgs/volumemute.png" />\n\n      <img *ngIf="volumeState === 1" class="imgicon" src="./assets/imgs/volumelow.png" />\n\n      <img *ngIf="volumeState === 2" class="imgicon" src="./assets/imgs/volumehight.png" />\n\n    </button>\n\n    <button ion-button class="btn_header" (mouseup)="onTouchStatus($event)" >\n\n      <img *ngIf="api.statusRobot===0 " class="imgwifi" src="./assets/imgs/statusgreen.png" />\n\n      <img *ngIf="api.statusRobot ===1" class="imgwifi" src="./assets/imgs/statusorange.png" />\n\n      <img *ngIf="api.statusRobot===2" class="imgwifi" src="./assets/imgs/statusred.png" />\n\n    </button>\n\n    <button ion-button class="btn_header" (mouseup)="onTouchBattery($event)" >\n\n      <img *ngIf="api.batteryState === 0" class="imgbattery" src="./assets/imgs/batteryoff.png" />\n\n      <img *ngIf="api.batteryState === 1" class="imgbattery" src="./assets/imgs/batterylow.png" />\n\n      <img *ngIf="api.batteryState === 2" class="imgbattery" src="./assets/imgs/batterymean.png" />\n\n      <img *ngIf="api.batteryState === 3" class="imgbattery" src="./assets/imgs/batteryhight.png" />\n\n      <img *ngIf="api.batteryState === 4" class="imgbattery" src="./assets/imgs/batterycharge.png" />\n\n    </button>\n\n  </ion-buttons>\n\n\n\n</ion-navbar>\n\n<ion-toolbar no-padding>\n\n  <div class="scroll_parent">\n\n    <ion-title class="scroll">\n\n      <font class="scroll_text">{{this.textHeadBand}}</font>\n\n    </ion-title>\n\n  </div>\n\n\n\n</ion-toolbar>'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\tourneeapp\src\components\headpage\headpage.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_7__services_speech_service__["a" /* SpeechService */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["k" /* ToastController */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_8__services_param_service__["a" /* ParamService */], __WEBPACK_IMPORTED_MODULE_4__services_api_service__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_5__services_alert_service__["a" /* AlertService */], __WEBPACK_IMPORTED_MODULE_6__services_popup_service__["a" /* PopupService */], __WEBPACK_IMPORTED_MODULE_11__angular_platform_browser__["c" /* DomSanitizer */]])
    ], HeadpageComponent);
    return HeadpageComponent;
}());

//# sourceMappingURL=headpage.js.map

/***/ }),

/***/ 220:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TutoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_param_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_pdfjs_dist_webpack_js__ = __webpack_require__(322);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_pdfjs_dist_webpack_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_pdfjs_dist_webpack_js__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var TutoPage = /** @class */ (function () {
    function TutoPage(sanitizer, param) {
        this.sanitizer = sanitizer;
        this.param = param;
        this.pageNum = 1;
        this.PDFJSViewer = __WEBPACK_IMPORTED_MODULE_3_pdfjs_dist_webpack_js__;
        this.currPage = 1; //Pages are 1-based not 0-based
        this.numPages = 0;
        this.thePDF = null;
        this.pageRendering = false;
        this.pageNumPending = null;
    }
    TutoPage.prototype.ngOnInit = function () {
    };
    TutoPage.prototype.ionViewDidEnter = function () {
        // deuxième méthode
        this.launchdoc();
    };
    TutoPage.prototype.launchdoc = function () {
        var _this = this;
        //This is where you start
        __WEBPACK_IMPORTED_MODULE_3_pdfjs_dist_webpack_js__["getDocument"](this.param.datatext.URL_tourneeapp).then(function (pdf) {
            //Set PDFJS global object (so we can easily access in our page functions
            _this.thePDF = pdf;
            //How many pages it has
            _this.numPages = pdf.numPages;
            //Start with first page
            pdf.getPage(1).then(function (page) { _this.handlePages(page); });
        });
    };
    TutoPage.prototype.handlePages = function (page) {
        var _this = this;
        //This gives us the page's dimensions at full scale
        var viewport = page.getViewport(2);
        //We'll create a canvas for each page to draw it on
        var canvas = document.createElement("canvas");
        canvas.style.display = "block";
        var context = canvas.getContext('2d');
        canvas.height = viewport.height;
        canvas.width = viewport.width;
        //Draw it on the canvas
        page.render({ canvasContext: context, viewport: viewport });
        //Add it to the web page
        //document.body.appendChild( canvas );
        //console.log(document.getElementById("ionbody"));
        var elem = document.getElementById("ionbody");
        if (elem != null) {
            elem.appendChild(canvas);
            var line = document.createElement("hr");
            elem.appendChild(line);
        }
        //console.log(this.currPage);
        //Move to next page
        this.currPageAdd();
        if (this.thePDF !== null && this.currPage <= this.numPages) {
            this.thePDF.getPage(this.currPage).then(function (page) { _this.handlePages(page); });
        }
    };
    TutoPage.prototype.currPageAdd = function () {
        this.currPage = this.currPage + 1;
    };
    TutoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-tuto',template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\tourneeapp\src\pages\tuto\tuto.html"*/'<!-- home page html template -->\n\n<ion-header no-border>\n\n  <headpage pageName="tuto"></headpage>\n\n</ion-header>\n\n\n\n<ion-content padding >\n\n  <ion-scroll scrollY="true" style="height:100%;width:80%;left:50%;transform:translateX(-50%); " >\n\n    <div class="center" style="height:100%;">\n\n      <div id="ionbody" style="display: flex;flex-grow: 1;  flex-direction: column;background: #ddd;overflow-y: auto;" width="60%" height="100%">\n\n\n\n      </div>\n\n    </div>\n\n  </ion-scroll>\n\n</ion-content>\n\n'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\tourneeapp\src\pages\tuto\tuto.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["c" /* DomSanitizer */], __WEBPACK_IMPORTED_MODULE_2__services_param_service__["a" /* ParamService */]])
    ], TutoPage);
    return TutoPage;
}());

//# sourceMappingURL=tuto.js.map

/***/ }),

/***/ 231:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ParamPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_api_service__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_mail_mail__ = __webpack_require__(120);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__sms_sms__ = __webpack_require__(232);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__annonce_annonce__ = __webpack_require__(116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__langue_langue__ = __webpack_require__(233);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_param_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__password_password__ = __webpack_require__(234);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__robotBehaviour_robotBehaviour__ = __webpack_require__(235);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__radio_radio__ = __webpack_require__(236);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var ParamPage = /** @class */ (function () {
    function ParamPage(navCtrl, api, param) {
        this.navCtrl = navCtrl;
        this.api = api;
        this.param = param;
        this.mailPage = __WEBPACK_IMPORTED_MODULE_3__pages_mail_mail__["a" /* MailPage */];
        this.languePage = __WEBPACK_IMPORTED_MODULE_6__langue_langue__["a" /* LanguePage */];
        this.smsPage = __WEBPACK_IMPORTED_MODULE_4__sms_sms__["a" /* SMSPage */];
        this.annoncePage = __WEBPACK_IMPORTED_MODULE_5__annonce_annonce__["a" /* AnnoncePage */];
        this.passwordPage = __WEBPACK_IMPORTED_MODULE_8__password_password__["a" /* PasswordPage */];
        this.behaviourPage = __WEBPACK_IMPORTED_MODULE_9__robotBehaviour_robotBehaviour__["a" /* RobotBehaviourPage */];
        this.radioPage = __WEBPACK_IMPORTED_MODULE_10__radio_radio__["a" /* RadioPage */];
    }
    ParamPage.prototype.ngOnInit = function () {
    };
    ParamPage.prototype.goMail = function (ev) {
        ev.preventDefault();
        this.navCtrl.push(this.mailPage);
    };
    ParamPage.prototype.goSms = function (ev) {
        ev.preventDefault();
        this.navCtrl.push(this.smsPage);
    };
    ParamPage.prototype.goLangue = function (ev) {
        ev.preventDefault();
        this.navCtrl.push(this.languePage);
    };
    ParamPage.prototype.goPassword = function (ev) {
        ev.preventDefault();
        this.navCtrl.push(this.passwordPage);
    };
    ParamPage.prototype.goAnnonce = function (ev) {
        ev.preventDefault();
        this.navCtrl.push(this.annoncePage);
    };
    ParamPage.prototype.goRobotBehaviour = function (ev) {
        ev.preventDefault();
        this.navCtrl.push(this.behaviourPage);
    };
    ParamPage.prototype.goRadio = function (ev) {
        ev.preventDefault();
        this.navCtrl.push(this.radioPage);
    };
    ParamPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-param',template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\tourneeapp\src\pages\param\param.html"*/'<!-- param page html template -->\n\n<ion-header no-border>\n\n  <headpage pageName="param"></headpage>\n\n</ion-header>\n\n\n\n\n\n<ion-content padding>\n\n  <ion-card text-center class="big_card">\n\n    <ion-grid class="heightstyle">\n\n\n\n      <ion-list>\n\n        <button ion-item class="btnscss" (mouseup)="goRobotBehaviour($event)">\n\n          <ion-icon class="iconscss" name="outlet" item-start></ion-icon>{{param.datatext.robotBehaviour}}<ion-icon\n\n            class="iconscss" name="outlet" item-end></ion-icon>\n\n        </button>\n\n        <button ion-item class="btnscss" (mouseup)="goMail($event)" >\n\n          <ion-icon class="iconscss" name="at" item-start></ion-icon>{{param.datatext.mails}}<ion-icon class="iconscss"\n\n            name="at" item-end></ion-icon>\n\n        </button>\n\n        <button ion-item class="btnscss" (mouseup)="goAnnonce($event)">\n\n          <ion-icon class="iconscss" name="chatboxes" item-start></ion-icon>{{param.datatext.announcement}}<ion-icon\n\n            class="iconscss" name="chatboxes" item-end></ion-icon>\n\n        </button>\n\n        <button ion-item class="btnscss" (mouseup)="goRadio($event)">\n\n          <ion-icon class="iconscss" name="radio" item-start></ion-icon>{{param.datatext.radio}}<ion-icon\n\n            class="iconscss" name="radio" item-end></ion-icon>\n\n        </button>\n\n        <!-- <button ion-item class="btnscss" (click)="goSms()"><ion-icon  class="iconscss" name="chatboxes" item-start></ion-icon>{{param.datatext.sms}}<ion-icon  class="iconscss" name="chatboxes" item-end></ion-icon></button> -->\n\n        <button ion-item class="btnscss" (mouseup)="goLangue($event)" >\n\n          <ion-icon class="iconscss" name="globe" item-start></ion-icon>{{param.datatext.langage}}<ion-icon\n\n            class="iconscss" name="globe" item-end></ion-icon>\n\n        </button>\n\n        <button ion-item class="btnscss" (mouseup)="goPassword($event)">\n\n          <ion-icon class="iconscss" name="key" item-start></ion-icon>{{param.datatext.editpswd1}}<ion-icon\n\n            class="iconscss" name="key" item-end></ion-icon>\n\n        </button>\n\n      </ion-list>\n\n\n\n    </ion-grid>\n\n\n\n  </ion-card>\n\n\n\n\n\n</ion-content>'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\tourneeapp\src\pages\param\param.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__services_api_service__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_7__services_param_service__["a" /* ParamService */]])
    ], ParamPage);
    return ParamPage;
}());

//# sourceMappingURL=param.js.map

/***/ }),

/***/ 232:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SMSPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_param_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SMSPage = /** @class */ (function () {
    function SMSPage(param, toastCtrl) {
        this.param = param;
        this.toastCtrl = toastCtrl;
    }
    SMSPage.prototype.ngOnInit = function () {
    };
    SMSPage.prototype.errorToast = function (m) {
        var toast = this.toastCtrl.create({
            message: m,
            duration: 3000,
            position: 'middle',
            cssClass: "toast"
        });
        toast.present();
    };
    SMSPage.prototype.okToast = function (m) {
        var toast = this.toastCtrl.create({
            message: m,
            duration: 3000,
            position: 'middle',
            cssClass: "toastok"
        });
        toast.present();
    };
    SMSPage.prototype.removeNum = function (ev, m) {
        ev.preventDefault();
        var index = this.param.phonenumberlist.indexOf(m);
        if (index !== -1) {
            this.param.phonenumberlist.splice(index, 1);
            this.param.deletePhone(m);
            this.okToast(this.param.datatext.mailRemove);
        }
    };
    SMSPage.prototype.is_a_num = function (m) {
        var numformat = "^[0-9]{10}$";
        return m.match(numformat);
    };
    SMSPage.prototype.changeNum = function (m) {
        return "+33" + m.substring(1);
    };
    SMSPage.prototype.displayNum = function (m) {
        return "0" + m.substring(3);
    };
    SMSPage.prototype.addNum = function (ev, m) {
        ev.preventDefault();
        if (this.is_a_num(m)) {
            var index = this.param.phonenumberlist.indexOf(this.changeNum(m));
            if (index !== -1) {
                this.errorToast(this.param.datatext.numexist);
            }
            else if (this.param.phonenumberlist.length > 5) {
                this.errorToast(this.param.datatext.deletenum);
            }
            else {
                this.param.phonenumberlist.push(this.changeNum(m));
                this.param.addPhone(this.changeNum(m));
                this.okToast(this.param.datatext.numadd);
            }
        }
        else {
            this.errorToast(this.param.datatext.numincorrect);
        }
    };
    SMSPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-sms',template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\tourneeapp\src\pages\sms\sms.html"*/'<!-- mail page html template -->\n\n<ion-header no-border>\n\n  <headpage pageName="sms"></headpage>\n\n</ion-header>\n\n\n\n\n\n<ion-content padding >\n\n  <ion-card text-center class="big_card" >\n\n    <ion-grid class="heightstyle">\n\n     \n\n        <ion-item >\n\n          <ion-label class="txt" stacked>{{param.datatext.newnum}}</ion-label>\n\n          <ion-input #tel  inputmode="tel" placeholder="0630854706"></ion-input>\n\n          <button *ngIf="is_a_num(tel.value)" class="btn-add" (mouseup)="addNum($event,tel.value)" item-end>\n\n            <ion-icon  color="light" name="add"></ion-icon>\n\n          </button>\n\n          <button *ngIf="!(is_a_num(tel.value))" class="mailnotvalid" (mouseup)="addNum($event,tel.value)" item-end>\n\n            <ion-icon  color="light" name="add"></ion-icon>\n\n          </button>\n\n        </ion-item>\n\n       \n\n      \n\n        <ion-list >\n\n          <ion-item *ngFor="let m of this.param.phonenumberlist">\n\n            <ion-icon class="iconscss" name="chatboxes" item-start color="primary"></ion-icon>{{displayNum(m)}}\n\n            <button class="btn-trash" item-end (mouseup)="removeNum($event,m)">\n\n              <ion-icon class="btn-trash" name="trash"></ion-icon>\n\n            </button>\n\n          </ion-item>\n\n        </ion-list>\n\n    \n\n    </ion-grid> \n\n\n\n</ion-card>\n\n \n\n\n\n</ion-content>\n\n\n\n\n\n'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\tourneeapp\src\pages\sms\sms.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_param_service__["a" /* ParamService */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* ToastController */]])
    ], SMSPage);
    return SMSPage;
}());

//# sourceMappingURL=sms.js.map

/***/ }),

/***/ 233:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LanguePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_param_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var LanguePage = /** @class */ (function () {
    function LanguePage(param, loadingCtrl) {
        this.param = param;
        this.loadingCtrl = loadingCtrl;
        this.monitor = this.param.langage;
    }
    LanguePage.prototype.ngOnInit = function () {
    };
    LanguePage.prototype.monitorHandler = function () {
        this.loading = this.loadingCtrl.create({});
        this.loading.present(); //loading animation display
        this.param.updateRobot();
        setTimeout(function () {
            document.location.reload();
        }, 1000);
    };
    LanguePage.prototype.onSliderRelease = function (ev, id) {
        ev.preventDefault();
        id.open();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("select1"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* Select */])
    ], LanguePage.prototype, "select1", void 0);
    LanguePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-langue',template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\tourneeapp\src\pages\langue\langue.html"*/'<!-- langue page html template -->\n\n<ion-header no-border>\n\n  <headpage pageName="langue"></headpage>\n\n</ion-header>\n\n<ion-content padding >\n\n  <ion-grid class="main_ion_grid">\n\n    <div style="background-color: rgb(255, 255, 255); border-radius: 15px; height: 40%; width: 60%; ">\n\n      <ion-row style="height: 25%; color: rgb(0, 0, 0);justify-content: center!important; align-items: center!important; font-size: 4em;">\n\n        {{param.datatext.langage}}\n\n      </ion-row>\n\n      <ion-row style="height: 75%;vertical-align: middle;\n\n      justify-content: center;\n\n      display: flex!important;\n\n      align-items: center!important">\n\n       \n\n        <ion-select #select1 [(ngModel)]="param.robot.langage" (ionChange)="monitorHandler()" style="width: 70% !important; font-size: x-large;background-color:rgba(26, 156, 195, 0.199);" interface="popover" (mouseup)="onSliderRelease($event,select1)">\n\n          <ion-option value="de-DE">Deutsch</ion-option>\n\n          <ion-option value="en-GB">English</ion-option> \n\n          <ion-option value="el-GR">Ελληνικά</ion-option>\n\n          <ion-option value="fr-FR">Français</ion-option>\n\n          <ion-option value="it-IT">Italiano</ion-option>\n\n          <ion-option value="es-ES">Spanish</ion-option>\n\n         \n\n        </ion-select>\n\n     \n\n      </ion-row>\n\n    </div>\n\n   </ion-grid>\n\n\n\n</ion-content>\n\n\n\n'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\tourneeapp\src\pages\langue\langue.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_param_service__["a" /* ParamService */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* LoadingController */]])
    ], LanguePage);
    return LanguePage;
}());

//# sourceMappingURL=langue.js.map

/***/ }),

/***/ 234:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PasswordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_param_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_popup_service__ = __webpack_require__(31);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var PasswordPage = /** @class */ (function () {
    function PasswordPage(navCtrl, popup, param) {
        this.navCtrl = navCtrl;
        this.popup = popup;
        this.param = param;
        this.newpassword = "";
        this.warn = false;
    }
    PasswordPage.prototype.ngOnInit = function () {
    };
    PasswordPage.prototype.editpassword = function (ev) {
        ev.preventDefault();
        if (this.password === atob(this.param.robot.password)) {
            if (this.newpassword.length >= 2) {
                this.popup.showToast(this.param.datatext.pswdsaved, 3000, "middle");
                this.param.robot.password = btoa(this.newpassword);
                this.param.updateRobot();
                this.password = "";
                this.newpassword = "";
                this.navCtrl.pop();
            }
        }
        else {
            this.warn = true;
        }
    };
    PasswordPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-password',template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\tourneeapp\src\pages\password\password.html"*/'<!-- password page html template -->\n\n<ion-header no-border>\n\n    <headpage pageName="password"></headpage>\n\n  </ion-header>\n\n  \n\n  \n\n  <ion-content padding >\n\n    <ion-grid class="main_ion_grid">\n\n        <div style="background-color: rgb(255, 255, 255); border-radius: 15px; height: 70%; width: 60%; ">\n\n    \n\n        <ion-row style="height: 25%;justify-content: center!important; align-items: center!important;">\n\n          <ion-item style="width: 90%;">\n\n          <ion-label style="font-size: x-large; min-width: 30% !important;\n\n          max-width: 30% !important;">{{param.datatext.currentpswd}}</ion-label>\n\n          <ion-input minlength=2 maxlength=15 [(ngModel)]="password" style="font-size: xx-large; color: rgb(26, 156, 195);"  required></ion-input>\n\n       </ion-item>\n\n          \n\n        </ion-row>\n\n        \n\n    \n\n        <ion-row style="height: 25%;justify-content: center!important; align-items: center!important;">\n\n          <ion-item style="width: 90%;">\n\n          <ion-label style="font-size: x-large; min-width: 30% !important;\n\n          max-width: 30% !important;">{{param.datatext.newpswd}}</ion-label>\n\n          <ion-input [(ngModel)]="newpassword" minlength=2 maxlength=15 style="font-size: xx-large; color: rgb(26, 156, 195);" required></ion-input>\n\n          </ion-item>\n\n          \n\n        </ion-row>\n\n       <ion-row style="height: 20%; justify-content: center!important; ">\n\n          <p *ngIf="warn" style="color: red; font-size: large;">** {{param.datatext.wrongpass}} **</p>\n\n        </ion-row>\n\n        <ion-row style="height: 25%;justify-content: center!important; align-items: center!important;">\n\n          <button [disabled]="2>this.newpassword.length" ion-button solid large class="btn_login" (mouseup)="editpassword($event)"> \n\n            <!-- the text of the button depends on the page you are on -->\n\n            <font class="font_menu_size" >{{param.datatext.save}}</font>\n\n          </button>\n\n     \n\n        </ion-row>\n\n        </div>\n\n       </ion-grid>\n\n  \n\n  </ion-content>\n\n  '/*ion-inline-end:"C:\Users\laele\Documents\KomApp\tourneeapp\src\pages\password\password.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__services_popup_service__["a" /* PopupService */], __WEBPACK_IMPORTED_MODULE_1__services_param_service__["a" /* ParamService */]])
    ], PasswordPage);
    return PasswordPage;
}());

//# sourceMappingURL=password.js.map

/***/ }),

/***/ 235:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RobotBehaviourPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_param_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_popup_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_api_service__ = __webpack_require__(24);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var RobotBehaviourPage = /** @class */ (function () {
    function RobotBehaviourPage(param, api, loadingCtrl, popup, toastCtrl) {
        this.param = param;
        this.api = api;
        this.loadingCtrl = loadingCtrl;
        this.popup = popup;
        this.toastCtrl = toastCtrl;
    }
    RobotBehaviourPage.prototype.ngOnInit = function () {
        this.param.getDataPatrol();
    };
    RobotBehaviourPage.prototype.update_fall_detection = function () {
        if (this.param.patrolInfo.round_fall_detection == 1) {
            this.param.patrolInfo.round_fall_detection = 0;
        }
        else {
            this.param.patrolInfo.round_fall_detection = 1;
        }
        this.param.updatePatrol();
    };
    RobotBehaviourPage.prototype.update_mask_detection = function () {
        if (this.param.patrolInfo.round_mask_detection == 1) {
            this.param.patrolInfo.round_mask_detection = 0;
            this.api.startMaskDetection(false);
        }
        else {
            this.api.startMaskDetection(true);
            this.param.patrolInfo.round_mask_detection = 1;
        }
        this.param.updatePatrol();
    };
    RobotBehaviourPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-robotBehaviour',template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\tourneeapp\src\pages\robotBehaviour\robotBehaviour.html"*/'<!-- mail page html template -->\n\n<ion-header no-border>\n\n    <headpage pageName="behaviour"></headpage>\n\n  </ion-header>\n\n  \n\n  \n\n  <ion-content padding >\n\n    <ion-card class="big_card" >\n\n      <ion-grid class="heightstyle">\n\n        <ion-row class="center">\n\n            <ion-col col-2></ion-col>\n\n            <ion-col text-left col-7>\n\n              <h2 class="titlescss">{{param.datatext.fallDetectionActivation}}</h2>\n\n            </ion-col>\n\n            <ion-col col-3>\n\n              <ion-toggle\n\n                      [checked]="param.patrolInfo.round_fall_detection == 1"\n\n                      (ionChange)="update_fall_detection()"\n\n                      item-end\n\n                  ></ion-toggle>\n\n            </ion-col>\n\n          </ion-row>\n\n         \n\n\n\n          <ion-row class="center">\n\n            <ion-col col-2></ion-col>\n\n            <ion-col text-left col-7>\n\n              <h2 class="titlescss">{{param.datatext.maskDetectionActivation}}</h2>\n\n            </ion-col>\n\n            <ion-col col-3>\n\n              <ion-toggle\n\n                      [checked]="param.patrolInfo.round_mask_detection == 1"\n\n                      (ionChange)="update_mask_detection()"\n\n                      item-end\n\n                  ></ion-toggle>\n\n            </ion-col>\n\n          </ion-row>\n\n      \n\n      </ion-grid> \n\n  \n\n  </ion-card>\n\n   \n\n  \n\n  </ion-content>'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\tourneeapp\src\pages\robotBehaviour\robotBehaviour.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_param_service__["a" /* ParamService */], __WEBPACK_IMPORTED_MODULE_4__services_api_service__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_3__services_popup_service__["a" /* PopupService */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* ToastController */]])
    ], RobotBehaviourPage);
    return RobotBehaviourPage;
}());

//# sourceMappingURL=robotBehaviour.js.map

/***/ }),

/***/ 236:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RadioPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_param_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_popup_service__ = __webpack_require__(31);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var RadioPage = /** @class */ (function () {
    function RadioPage(param, popup, toastCtrl) {
        this.param = param;
        this.popup = popup;
        this.toastCtrl = toastCtrl;
        this.inputurl = "";
        this.inputname = "";
    }
    RadioPage.prototype.ngOnInit = function () {
    };
    RadioPage.prototype.errorToast = function (m) {
        var toast = this.toastCtrl.create({
            message: m,
            duration: 3000,
            position: 'middle',
            cssClass: "toast"
        });
        toast.present();
    };
    RadioPage.prototype.okToast = function (m) {
        var toast = this.toastCtrl.create({
            message: m,
            duration: 3000,
            position: 'middle',
            cssClass: "toastok"
        });
        toast.present();
    };
    RadioPage.prototype.removeRadio = function (ev, m) {
        ev.preventDefault();
        this.param.deleteRadio(m.idRadio);
        var index = this.param.radiolist.indexOf(m);
        console.log(index);
        if (index !== -1) {
            this.param.radiolist.splice(index, 1);
            this.okToast(this.param.datatext.radioRemove);
        }
    };
    RadioPage.prototype.is_a_url = function (url) {
        var urlformat = "^(http|https)://.*$";
        return url.match(urlformat);
    };
    RadioPage.prototype.addRadio = function (ev) {
        var _this = this;
        ev.preventDefault();
        if (this.is_a_url(this.inputurl) && this.inputname.length > 1) {
            if (this.param.radiolist.find(function (x) { return x.nameRadio == _this.inputname; })) {
                this.errorToast(this.param.datatext.radioexist);
            }
            else {
                this.param.addRadio(this.inputname, this.inputurl);
                this.okToast(this.param.datatext.radioadd);
                this.inputname = "";
                this.inputurl = "";
                setTimeout(function () {
                    _this.content1.scrollToBottom();
                }, 2000);
                setTimeout(function () {
                    _this.param.getradio();
                }, 1000);
            }
        }
        else {
            this.errorToast(this.param.datatext.radioincorrect);
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("content1"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* Content */])
    ], RadioPage.prototype, "content1", void 0);
    RadioPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-radio',template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\tourneeapp\src\pages\radio\radio.html"*/'<!-- mail page html template -->\n\n<ion-header no-border>\n\n  <headpage pageName="radio"></headpage>\n\n</ion-header>\n\n\n\n\n\n<ion-content class="big-ion-content" padding >\n\n  <ion-card text-center class="big_card" >\n\n    <ion-grid class="heightstyle">\n\n\n\n        <ion-item class="itemail">\n\n          <ion-label class="txt" stacked>{{param.datatext.newradio}}</ion-label>\n\n          <ion-input inputmode="nomRadio" [(ngModel)]="inputname" placeholder="Radio Name"></ion-input>\n\n          <ion-input inputmode="urlRadio" [(ngModel)]="inputurl" placeholder="Radio URL"></ion-input>\n\n          <button *ngIf="is_a_url(inputurl)" class="btn-add" (mouseup)="addRadio($event)" item-end>\n\n            <ion-icon  color="light" name="add"></ion-icon>\n\n          </button>\n\n          <button *ngIf="!(is_a_url(inputurl))" class="urlnotvalid" (mouseup)="addRadio($event)" item-end>\n\n            <ion-icon  color="light" name="add"></ion-icon>\n\n          </button>\n\n        </ion-item>\n\n        \n\n        <ion-content #content1 style="height: 80%; width: 100%; background-color: transparent;">\n\n        \n\n          <ion-item *ngFor="let m of this.param.radiolist">\n\n            <ion-icon class="iconscss" name="radio" item-start color="primary"></ion-icon>{{m.nameRadio}}\n\n            <button class="btn-trash" item-end (mouseup)="removeRadio($event,m)">\n\n              <ion-icon class="btn-trash" name="trash"></ion-icon>\n\n            </button>\n\n          </ion-item>\n\n        \n\n        </ion-content>\n\n    \n\n    </ion-grid> \n\n\n\n</ion-card>\n\n\n\n</ion-content>\n\n\n\n'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\tourneeapp\src\pages\radio\radio.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_param_service__["a" /* ParamService */], __WEBPACK_IMPORTED_MODULE_3__services_popup_service__["a" /* PopupService */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* ToastController */]])
    ], RadioPage);
    return RadioPage;
}());

//# sourceMappingURL=radio.js.map

/***/ }),

/***/ 237:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AutoLaunchPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_param_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_api_service__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(5);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AutoLaunchPage = /** @class */ (function () {
    function AutoLaunchPage(param, api, toastCtrl, loadingCtrl) {
        this.param = param;
        this.api = api;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this.newMission = {};
        this.selection = "today";
        this.todayDate = new Date().toLocaleDateString(this.param.langage) + " " + new Date().toLocaleTimeString('fr-FR').substring(0, 5);
        this.minDate = new Date().toISOString().substring(0, 10) + "T" + new Date().toLocaleTimeString();
        this.newMission.date = this.minDate;
        this.newMission.map = this.api.id_current_map;
        this.selectMap = {
            title: this.param.datatext.map,
        };
        this.selectRound = {
            title: this.param.datatext.roundSelection,
        };
        this.selectDuration = {
            title: this.param.datatext.duration,
        };
        this.selectType = {
            title: this.param.datatext.type,
        };
        this.pickerOptions = {
            title: this.param.datatext.date,
        };
    }
    AutoLaunchPage.prototype.logForm = function () {
        var _this = this;
        var lengthMissionList = this.param.missionsList.length;
        this.param.addMission(this.newMission);
        this.param.getDataReservationPatrol(this.api.rounddata);
        this.loading = this.loadingCtrl.create({});
        this.loading.present();
        this.wait = setInterval(function () { return _this.checkNewLengthAdd(lengthMissionList); }, 1000);
    };
    AutoLaunchPage.prototype.ngOnInit = function () {
        this.param.getDataReservationPatrol(this.api.rounddata);
        this.api.getAllMapsHttp();
    };
    AutoLaunchPage.prototype.checkNewLengthAdd = function (oldLength) {
        var _this = this;
        this.param.getDataReservationPatrol(this.api.rounddata);
        console.log(this.param.missionsList.length, oldLength);
        if (this.param.missionsList.length > oldLength) {
            this.loading.dismiss();
            this.api.addMission = !this.api.addMission;
            this.okToast(this.param.datatext.missionAdded);
            setTimeout(function () {
                if (_this.selection == "all")
                    _this.content1.scrollToBottom();
                else
                    _this.content2.scrollToBottom();
            }, 300);
            clearInterval(this.wait);
        }
    };
    AutoLaunchPage.prototype.checkNewLengthDelete = function (oldLength) {
        this.param.getDataReservationPatrol(this.api.rounddata);
        if (this.param.missionsList.length < oldLength) {
            this.loading.dismiss();
            this.okToast(this.param.datatext.mailRemove);
            clearInterval(this.wait);
        }
    };
    AutoLaunchPage.prototype.removeMission = function (ev, mission) {
        var _this = this;
        ev.preventDefault();
        var oldLength = this.param.missionsList.length;
        this.param.deleteMission(mission.id);
        this.param.getDataReservationPatrol(this.api.rounddata);
        this.loading = this.loadingCtrl.create({});
        this.loading.present();
        this.wait = setInterval(function () { return _this.checkNewLengthDelete(oldLength); }, 500);
    };
    AutoLaunchPage.prototype.okToast = function (m) {
        var toast = this.toastCtrl.create({
            message: m,
            duration: 3000,
            position: 'middle',
            cssClass: "toastok"
        });
        toast.present();
    };
    AutoLaunchPage.prototype.createMission = function (ev) {
        ev.preventDefault();
        this.todayDate = new Date().toLocaleDateString(this.param.langage) + " " + new Date().toLocaleTimeString('fr-FR').substring(0, 5);
        this.minDate = new Date().toISOString().substring(0, 10) + "T" + new Date().toLocaleTimeString();
        this.newMission.date = this.minDate;
        this.api.addMission = !this.api.addMission;
    };
    AutoLaunchPage.prototype.updateRoundList = function () {
        this.newMission.idRound = undefined;
        this.api.getRoundListAutoLaunch(this.newMission.map);
    };
    AutoLaunchPage.prototype.onSliderRelease = function (ev, id) {
        ev.preventDefault();
        id.open();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("select1"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["j" /* Select */])
    ], AutoLaunchPage.prototype, "select1", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("select2"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["j" /* Select */])
    ], AutoLaunchPage.prototype, "select2", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("select3"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["j" /* Select */])
    ], AutoLaunchPage.prototype, "select3", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("select4"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["j" /* Select */])
    ], AutoLaunchPage.prototype, "select4", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("content1"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["c" /* Content */])
    ], AutoLaunchPage.prototype, "content1", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])("content2"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["c" /* Content */])
    ], AutoLaunchPage.prototype, "content2", void 0);
    AutoLaunchPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-autolaunch',template:/*ion-inline-start:"C:\Users\laele\Documents\KomApp\tourneeapp\src\pages\autolaunch\autolaunch.html"*/'<!-- mail page html template -->\n\n<ion-header no-border>\n\n    <headpage pageName="autolaunch"></headpage>\n\n  </ion-header>\n\n  \n\n  \n\n<ion-content class="big-ion-content" padding>\n\n  <ion-card [hidden]="api.addMission" class="big_card">\n\n\n\n    <ion-row [hidden]="api.addMission">\n\n      <ion-col text-right>\n\n        <button ion-button class="btn-add" (mouseup)="createMission($event)">\n\n          <ion-icon name="add" class="iconscss" style="color: white;"></ion-icon>\n\n        </button>\n\n      </ion-col>\n\n    </ion-row>\n\n\n\n    <ion-row [hidden]="api.addMission" class="heightstyle80">\n\n      <ion-col class="heightstyle">\n\n        <ion-segment class="heightstyle10" id="test" [(ngModel)]="selection">\n\n          <ion-segment-button value="today" style="font-size: x-large">\n\n            {{param.datatext.today}}\n\n          </ion-segment-button>\n\n          <ion-segment-button value="all" style="font-size: x-large">\n\n            {{param.datatext.all}}\n\n          </ion-segment-button>\n\n\n\n        </ion-segment>\n\n\n\n\n\n        <ion-list [hidden]="this.selection === \'today\'" class="autolaunchionlist">\n\n          <ion-content #content1 style="height: 100%; width: 100%;">\n\n            <ion-item class="autolaunchitem" *ngFor="let mission of this.param.missionsDisplay;">\n\n              <ion-row>\n\n                <ion-col col-2>\n\n                  {{mission.date}}\n\n                </ion-col>\n\n                <ion-col col-2>\n\n                  {{mission.time}}\n\n                </ion-col>\n\n                <ion-col col-4>\n\n                  {{mission.name}}\n\n                </ion-col>\n\n                <ion-col col-2>\n\n                  {{mission.tpsReserv}}min\n\n                </ion-col>\n\n                <ion-col col-1>\n\n                  {{mission.type}}\n\n                </ion-col>\n\n                <ion-col col-1>\n\n                  <button class="btn-trash" (mouseup)="removeMission($event,mission)">\n\n                    <ion-icon class="iconscss" name="trash"></ion-icon>\n\n                  </button>\n\n                </ion-col>\n\n              </ion-row>\n\n            </ion-item>\n\n          </ion-content>\n\n        </ion-list>\n\n\n\n        <ion-list [hidden]="this.selection == \'all\'" class="autolaunchionlist">\n\n          <ion-content #content2 style="height: 100%; width: 100%;">\n\n            <ion-item class="autolaunchitem" *ngFor="let mission of this.param.missionsDisplayToday;">\n\n              <ion-row>\n\n                <ion-col col-1>\n\n                  {{mission.time}}\n\n                </ion-col>\n\n                <ion-col col-6>\n\n                  {{mission.name}}\n\n                </ion-col>\n\n                <ion-col col-2>\n\n                  {{mission.tpsReserv}}min\n\n                </ion-col>\n\n                <ion-col col-2>\n\n                  {{mission.type}}\n\n                </ion-col>\n\n                <ion-col col-1>\n\n                  <button class="btn-trash" (mouseup)="removeMission($event,mission)">\n\n                    <ion-icon class="iconscss" name="trash"></ion-icon>\n\n                  </button>\n\n                </ion-col>\n\n              </ion-row>\n\n            </ion-item>\n\n          </ion-content>\n\n        </ion-list>\n\n\n\n      </ion-col>\n\n    </ion-row>\n\n  </ion-card>\n\n\n\n\n\n  <ion-card *ngIf="api.addMission" class="big_card2">\n\n\n\n    <div style="background-color: rgb(255, 255, 255); height: 90%; width: 70%; padding: 2%; text-align: center;">\n\n      <ion-row style="justify-content: center;">\n\n        <h1 class="titlescss">{{param.datatext.createMission}}</h1>\n\n      </ion-row>\n\n      <form (ngSubmit)="logForm()">\n\n        <ion-item>\n\n\n\n          <ion-label>{{param.datatext.date}} :</ion-label>\n\n\n\n\n\n          <ion-datetime [pickerOptions]="pickerOptions" id="datetime" placeholder="{{todayDate}}"\n\n            displayFormat="DD/MM/YYYY HH:mm" pickerFormat="DD-MM-YYYYTHH:mm" [(ngModel)]="newMission.date" name="date"\n\n            class="selectForm" min="{{minDate}}" max="2100" doneText={{this.param.datatext.btn_ok}}\n\n            cancelText={{this.param.datatext.btn_cancel}} showDefaultTitle></ion-datetime>\n\n\n\n        </ion-item>\n\n        <ion-item>\n\n\n\n          <ion-label>{{param.datatext.map}} :</ion-label>\n\n\n\n\n\n          <ion-select #select1 [selectOptions]="selectMap" okText={{this.param.datatext.btn_ok}}\n\n            cancelText={{this.param.datatext.btn_cancel}} placeholder="{{param.datatext.mapSelect}}"\n\n            [(ngModel)]="newMission.map" (ionChange)="updateRoundList()" (mouseup)="onSliderRelease($event,select1)"\n\n            class="selectForm" name="map">\n\n            <ion-option *ngFor="let map of this.api.all_maps" value="{{map.Id}}">{{map.Name}}</ion-option>\n\n          </ion-select>\n\n\n\n        </ion-item>\n\n        <ion-item>\n\n\n\n          <ion-label>{{param.datatext.roundSelection}} :</ion-label>\n\n\n\n\n\n          <ion-select #select2 class="myCustomSelect" [selectOptions]="selectRound" [(ngModel)]="newMission.idRound"\n\n            okText={{this.param.datatext.btn_ok}} cancelText={{this.param.datatext.btn_cancel}}\n\n            placeholder="{{param.datatext.roundSelect}}" (mouseup)="onSliderRelease($event,select2)" name="round"\n\n            class="selectForm" [disabled]="!newMission.map">\n\n            <ion-option *ngFor="let round of this.api.round_map_selected" value="{{round.Id}}">{{round.Name}}\n\n            </ion-option>\n\n          </ion-select>\n\n\n\n        </ion-item>\n\n        <ion-item>\n\n\n\n          <ion-label>{{param.datatext.duration}} :</ion-label>\n\n\n\n\n\n          <ion-select #select3 class="myCustomSelect" [selectOptions]="selectDuration"\n\n            [(ngModel)]="newMission.tpsReserv" okText={{this.param.datatext.btn_ok}}\n\n            cancelText={{this.param.datatext.btn_cancel}} placeholder="{{param.datatext.durationSelect}}"\n\n            (mouseup)="onSliderRelease($event,select3)" name="duration" class="selectForm" required>\n\n            <ion-option value=10>10 min</ion-option>\n\n            <ion-option value=20>20 min</ion-option>\n\n            <ion-option value=30>30 min</ion-option>\n\n            <ion-option value=40>40 min</ion-option>\n\n            <ion-option value=50>50 min</ion-option>\n\n            <ion-option value=60>60 min</ion-option>\n\n            <ion-option value=90>1h30</ion-option>\n\n            <ion-option value=120>2h</ion-option>\n\n            <ion-option value=300>5h</ion-option>\n\n            <ion-option value=420>7h</ion-option>\n\n          </ion-select>\n\n\n\n        </ion-item>\n\n        <ion-item>\n\n\n\n          <ion-label>{{param.datatext.type}} :</ion-label>\n\n\n\n          <ion-select #select4 class="myCustomSelect" [selectOptions]="selectType" [(ngModel)]="newMission.type"\n\n            okText={{this.param.datatext.btn_ok}} cancelText={{this.param.datatext.btn_cancel}}\n\n            placeholder="{{param.datatext.typeSelect}}" (mouseup)="onSliderRelease($event,select4)" name="type"\n\n            class="selectForm" required>\n\n            <ion-option value=1>{{param.datatext.adhoc}}</ion-option>\n\n            <ion-option value=2>{{param.datatext.daily}}</ion-option>\n\n            <ion-option value=3>{{param.datatext.weekly}}</ion-option>\n\n          </ion-select>\n\n\n\n        </ion-item>\n\n\n\n\n\n        <button ion-button\n\n          [disabled]="!newMission.date || !newMission.map || !newMission.idRound || !newMission.tpsReserv || !newMission.type"\n\n          type="submit" class="btnscss">\n\n          {{param.datatext.create}}\n\n        </button>\n\n\n\n\n\n      </form>\n\n\n\n    </div>\n\n\n\n\n\n  </ion-card>\n\n</ion-content>'/*ion-inline-end:"C:\Users\laele\Documents\KomApp\tourneeapp\src\pages\autolaunch\autolaunch.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__services_param_service__["a" /* ParamService */], __WEBPACK_IMPORTED_MODULE_2__services_api_service__["a" /* ApiService */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["k" /* ToastController */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["g" /* LoadingController */]])
    ], AutoLaunchPage);
    return AutoLaunchPage;
}());

//# sourceMappingURL=autolaunch.js.map

/***/ }),

/***/ 238:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(239);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(259);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 24:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export Battery */
/* unused harmony export Statistics */
/* unused harmony export Anticollision */
/* unused harmony export Iostate */
/* unused harmony export Docking */
/* unused harmony export Navigation */
/* unused harmony export Differential */
/* unused harmony export Round */
/* unused harmony export Localization */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApiService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__alert_service__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__param_service__ = __webpack_require__(10);
// Allows Http communication with komnav
// cf Marc komnav_http_manual for more information
// https://drive.google.com/drive/u/3/folders/1-18kyKv6Ep-fzPU1F-dp0tQ1r8usyuZ7
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






var Battery = /** @class */ (function () {
    function Battery() {
    }
    return Battery;
}());

var Statistics = /** @class */ (function () {
    function Statistics() {
    }
    return Statistics;
}());

var Anticollision = /** @class */ (function () {
    function Anticollision() {
    }
    return Anticollision;
}());

var Iostate = /** @class */ (function () {
    function Iostate() {
    }
    return Iostate;
}());

var Docking = /** @class */ (function () {
    function Docking() {
    }
    return Docking;
}());

/*
{ Docking status
  "Unknown    ",
  "Undocked   ",
  "Docking    ",
  "Undocking  ",
  "Error      "
};*/
var Navigation = /** @class */ (function () {
    function Navigation() {
    }
    return Navigation;
}());

// The status can have the following values:
// • 0 - Waiting: the robot is ready for a new destination speciﬁed with a PUT /navigation/destination request.
// • 1 - Following: the robot is following the trajectory computed to join the destination.
// • 2 - Aiming: the robot has completed the trajectory and is now rotating to aim the destination accurately
// • 3 - Translating: the robot has ﬁnished aiming and is now translating to reach the destination
// • 4 - Rotating: this is the last rotation, executed only if the destination was speciﬁed with “Rotate” : true
// • 5-Error: indicatesthattherobothasencounteredanerrorthatpreventedittojointherequireddestination.
// This can be cleared by sending a new destination.
var Differential = /** @class */ (function () {
    function Differential() {
    }
    return Differential;
}());

var Round = /** @class */ (function () {
    function Round() {
    }
    return Round;
}());

var Localization = /** @class */ (function () {
    function Localization() {
    }
    return Localization;
}());

var ApiService = /** @class */ (function () {
    function ApiService(toastCtrl, httpClient, app, alert, param) {
        this.toastCtrl = toastCtrl;
        this.httpClient = httpClient;
        this.app = app;
        this.alert = alert;
        this.param = param;
        this.falldetected = false;
        this.persondetected = false;
        this.facedetected = false;
        this.maskdetected = false;
        this.popupFall = false;
        this.pauselookfall = 20;
        this.stoplookFall = 0;
        this.stoplookPerson = 0;
        this.all_maps = [];
        this.socketok = false;
        this.is_connected = false; // to know if we are connected to the robot
        this.is_background = false;
        //this.mapLoaded=false;
        this.is_localized = false;
        this.roundActive = false; // to know if the round is in process
        this.towardDocking = false; //to know if the robot is moving toward the docking
        this.start = false;
        this.fct_startRound = false;
        this.fct_onGo = false;
        this.statusRobot = 0;
        this.cpt_jetsonOK = 0;
        this.appOpened = false;
        this.close_app = false;
        this.is_blocked = false;
        this.is_high = false;
        this.robotok = false;
        this.sos_pressed = false;
        this.addMission = false;
    }
    ApiService.prototype.stopLookFall = function () {
        this.stoplookFall = this.stoplookFall + 1;
        if (this.stoplookFall > this.pauselookfall) {
            clearInterval(this.fallinterv);
            this.stoplookFall = 0;
        }
    };
    ApiService.prototype.instanciate = function () {
        this.socketok = true;
        var apiserv = this;
        //this.startDetectionHttp();
        ////////////////////// localization socket
        this.localization_status = new Localization();
        this.localization_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/localization/socket", "json");
        this.localization_socket.onerror = function (event) {
            console.log("error localization socket");
        };
        this.localization_socket.onopen = function (event) {
            console.log("localization socket opened");
            this.onmessage = function (event) {
                var test = JSON.parse(event.data);
                apiserv.localization_status.positionx = test["Pose"]["X"];
                apiserv.localization_status.positiony = test["Pose"]["Y"];
                apiserv.localization_status.positiont = test["Pose"]["T"];
            };
            this.onclose = function () {
                console.log("localization socket closed");
            };
        };
        ////////////////////// docking socket
        this.docking_status = new Docking();
        this.docking_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/docking/socket", "json");
        this.docking_socket.onerror = function (event) {
            console.log("error docking socket");
        };
        this.docking_socket.onopen = function (event) {
            console.log("docking socket opened");
            this.onmessage = function (event) {
                var test = JSON.parse(event.data);
                apiserv.docking_status.status = test["Status"];
                apiserv.docking_status.detected = test["Detected"];
            };
            this.onclose = function () {
                console.log("docking socket closed");
            };
        };
        ////////////////////// differential socket
        this.differential_status = new Differential();
        this.differential_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/differential/socket", "json");
        this.differential_socket.onerror = function (event) {
            console.log("error differential socket");
        };
        this.differential_socket.onopen = function (event) {
            console.log("differential socket opened");
            this.onmessage = function (event) {
                var test = JSON.parse(event.data);
                apiserv.differential_status.status = test["Status"];
            };
            this.onclose = function () {
                console.log("differential socket closed");
            };
        };
        ////////////////////// navigation socket
        this.navigation_status = new Navigation();
        this.navigation_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/navigation/socket", "json");
        this.navigation_socket.onerror = function (event) {
            console.log("error navigation socket");
        };
        this.navigation_socket.onopen = function (event) {
            console.log("navigation socket opened");
            this.onmessage = function (event) {
                var test = JSON.parse(event.data);
                apiserv.navigation_status.status = test["Status"];
                apiserv.navigation_status.avoided = test["Avoided"];
            };
            this.onclose = function () {
                console.log("navigation socket closed");
            };
        };
        ////////////////////// anticollision socket
        this.anticollision_status = new Anticollision();
        this.anticollision_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/anticollision/socket", "json");
        this.anticollision_socket.onerror = function (event) {
            console.log("error anticollision socket");
        };
        this.anticollision_socket.onopen = function (event) {
            console.log("anticollision socket opened");
            this.onmessage = function (event) {
                var test = JSON.parse(event.data);
                apiserv.anticollision_status.timestamp = test["Timestamp"];
                apiserv.anticollision_status.enabled = test["Enabled"];
                apiserv.anticollision_status.locked = test["Locked"];
                apiserv.anticollision_status.forward = test["Forward"];
                apiserv.anticollision_status.right = test["Right"];
                apiserv.anticollision_status.left = test["Left"];
            };
            this.onclose = function () {
                console.log("anticollision socket closed");
            };
        };
        ////////////////////// statistics socket
        this.statistics_status = new Statistics();
        this.statistics_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/statistics/socket", "json");
        this.statistics_socket.onerror = function (event) {
            console.log("error statistic socket");
        };
        this.statistics_socket.onopen = function (event) {
            console.log("statistic socket opened");
            this.onmessage = function (event) {
                var test = JSON.parse(event.data);
                apiserv.statistics_status.totalTime = test["TotalTime"];
                apiserv.statistics_status.totalDistance = test["TotalDistance"];
                apiserv.statistics_status.timestamp = test["Timestamp"];
            };
            this.onclose = function () {
                console.log("statistic socket closed");
            };
        };
        ///////////////////////// battery socket
        this.battery_status = new Battery();
        this.battery_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/battery/socket", "json");
        this.battery_socket.onerror = function (event) {
            console.log("error battery socket");
        };
        this.battery_socket.onopen = function (event) {
            console.log("battery socket opened");
            this.onmessage = function (event) {
                var test = JSON.parse(event.data);
                apiserv.battery_status.autonomy = test["Autonomy"];
                apiserv.battery_status.current = test["Current"];
                apiserv.battery_status.remaining = test["Remaining"];
                apiserv.battery_status.status = test["Status"];
                apiserv.battery_status.timestamp = test["Timestamp"];
                apiserv.battery_status.voltage = test["Voltage"];
            };
            this.onclose = function () {
                console.log("battery socket closed");
            };
        };
        //////////////////////// round socket
        this.round_status = new Round();
        this.round_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/rounds/socket", "json");
        this.round_socket.onerror = function (event) {
            console.log("error round socket");
        };
        this.round_socket.onopen = function (event) {
            console.log("round socket opened");
            this.onmessage = function (event) {
                var test = JSON.parse(event.data);
                apiserv.round_status.round = test["Round"];
                apiserv.round_status.acknowledge = test["Acknowledge"];
                apiserv.round_status.abort = test["Abort"];
                apiserv.round_status.pause = test["Pause"];
                apiserv.round_status.status = test["Status"];
            };
            this.onclose = function () {
                console.log("round socket closed");
            };
        };
        //////////////////////////////// io socket
        this.b_pressed = false;
        this.btnPush = false;
        this.io_status = new Iostate();
        this.io_socket = new WebSocket(this.wsskomnav + this.param.localhost + "/api/io/socket", "json");
        this.io_socket.onerror = function (event) {
            console.log("error iosocket");
        };
        this.io_socket.onopen = function (event) {
            console.log("io socket opened");
            this.onclose = function () {
                console.log("io socket closed");
            };
        };
        this.io_socket.onmessage = function (event) {
            var test = JSON.parse(event.data);
            apiserv.io_status.timestamp = test["Timestamp"];
            apiserv.io_status.dIn = test["DIn"];
            apiserv.io_status.aIn = test["AIn"];
            if (apiserv.io_status.dIn[13] && apiserv.stoplookFall === 0) {
                console.log("fall");
                apiserv.stoplookFall = 1;
                apiserv.falldetected = true;
                apiserv.fallinterv = setInterval(function () { return apiserv.stopLookFall(); }, 1000);
            }
            if (apiserv.io_status.dIn[2] || // btn barre
                apiserv.io_status.dIn[0] || // btn backtab
                apiserv.io_status.dIn[6] || //sos
                apiserv.io_status.dIn[3] ||
                apiserv.io_status.dIn[8]) {
                if (apiserv.io_status.dIn[6]) {
                    apiserv.sos_pressed = true;
                }
                //smart button and button A of the game pad
                if (!apiserv.b_pressed) {
                    apiserv.btnPush = true;
                    apiserv.b_pressed = true;
                }
            }
            else {
                if (apiserv.b_pressed) {
                    apiserv.b_pressed = false;
                }
            }
        };
        //////////////////////////////// ROS TOPIC connection
        this.ros = new ROSLIB.Ros({
            url: this.wssros + this.param.robot.rosip
        });
        this.ros.on('connection', function () {
            console.log('Connected to ros websocket server.');
        });
        this.ros.on('error', function (error) {
            console.log('Error connecting to ros websocket server: ', error);
        });
        this.ros.on('close', function () {
            console.log('Connection to ros websocket server closed.');
        });
        //////////////////////////////// ROS TOPIC mask
        this.listener_mask = new ROSLIB.Topic({
            ros: this.ros,
            name: '/mask_detection/status',
            messageType: 'std_msgs/Bool'
        });
        // var listener_face = new ROSLIB.Topic({
        //   ros : this.ros,
        //   name : '/face_detection/status',
        //   messageType : 'std_msgs/Bool'
        // });
        // listener_face.subscribe((message) => {
        //   console.log('Received message on ' + listener_face.name + ': ' + message.data);
        //   this.facedetected=message.data;
        // });
        //////////////////////////////// ROS TOPIC person and fall 
        this.listener_person = new ROSLIB.Topic({
            ros: this.ros,
            name: '/people_detection/status',
            messageType: 'std_msgs/Bool'
        });
        this.listener_fall = new ROSLIB.Topic({
            ros: this.ros,
            name: '/fall_detection/status',
            messageType: 'std_msgs/Bool'
        });
    };
    ApiService.prototype.ngOnInit = function () { };
    ApiService.prototype.mailAddInformationBasic = function () {
        var now = new Date().toLocaleString("en-GB", {
            day: "numeric",
            month: "numeric",
            year: "numeric",
            hour: "numeric",
            minute: "numeric",
            second: "numeric",
        });
        return ("<br> Time : " +
            now +
            "<br> Serial number : " +
            this.param.serialnumber +
            "<br> Battery remaining: " +
            this.battery_status.remaining +
            "%" +
            "<br> Battery state : " +
            this.battery_status.status +
            "<br> Battery voltage : " +
            this.battery_status.voltage +
            "<br> Battery current : " +
            this.battery_status.current +
            "<br> Battery autonomy : " +
            this.battery_status.autonomy +
            "<br> Docking state : " +
            this.docking_status.status +
            "<br> Status Forward : " +
            this.anticollision_status.forward +
            "<br> Status Right : " +
            this.anticollision_status.right +
            "<br> Status Left : " +
            this.anticollision_status.left +
            "<br> Status Reverse : " +
            this.anticollision_status.reverse +
            "<br> Navigation state : " +
            this.navigation_status.status +
            "<br> Differential state : " +
            this.differential_status.status +
            "<br> Odometer : " +
            this.statistics_status.totalDistance +
            "<br> Position X : " +
            this.localization_status.positionx +
            "<br> Position Y : " +
            this.localization_status.positiony +
            "<br> Position T : " +
            this.localization_status.positiont +
            "<br> Application : 2" +
            "<br>");
    };
    ApiService.prototype.mailAddInformationRound = function () {
        var now = new Date().toLocaleString("en-GB", {
            day: "numeric",
            month: "numeric",
            year: "numeric",
            hour: "numeric",
            minute: "numeric",
            second: "numeric",
        });
        var info;
        if (this.round_status.round) {
            info =
                "<br> Round id : " +
                    this.round_status.round["Id"] +
                    "<br> Round name : " +
                    this.round_status.round["Name"] +
                    "<br> Going to POI : " +
                    this.round_status.round["Locations"][0]["Location"]["Name"];
        }
        else {
            info =
                "<br> Round id : undefined" +
                    "<br> Round name : undefined" +
                    "<br> Going to POI : undefined";
        }
        return ("<br> Time : " +
            now +
            "<br> Serial number : " +
            this.param.serialnumber +
            "<br> Battery remaining: " +
            this.battery_status.remaining +
            "%" +
            "<br> Battery state : " +
            this.battery_status.status +
            "<br> Battery voltage : " +
            this.battery_status.voltage +
            "<br> Battery current : " +
            this.battery_status.current +
            "<br> Battery autonomy : " +
            this.battery_status.autonomy +
            "<br> Docking state : " +
            this.docking_status.status +
            "<br> Status Forward : " +
            this.anticollision_status.forward +
            "<br> Status Right : " +
            this.anticollision_status.right +
            "<br> Status Left : " +
            this.anticollision_status.left +
            "<br> Status Reverse : " +
            this.anticollision_status.reverse +
            "<br> Navigation state : " +
            this.navigation_status.status +
            "<br> Differential state : " +
            this.differential_status.status +
            "<br> Odometer : " +
            this.statistics_status.totalDistance +
            "<br> Position X : " +
            this.localization_status.positionx +
            "<br> Position Y : " +
            this.localization_status.positiony +
            "<br> Position T : " +
            this.localization_status.positiont +
            "<br> Application : 2" +
            "<br> Round state : " +
            this.round_status.status +
            info);
    };
    ApiService.prototype.joystickHttp = function (lin, rad) {
        var _this = this;
        var cmd = { Enable: true, TargetLinearSpeed: lin, TargetAngularSpeed: rad };
        var body = JSON.stringify(cmd);
        var headers = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]({ "Content-Type": "application/json" });
        var options = { headers: headers };
        return new Promise(function (resolve) {
            _this.httpClient
                .put(_this.httpskomnav + _this.param.localhost + "/api/differential/command", body, options)
                .subscribe(function (resp) {
                //console.log(resp);
                console.log("joystickOK");
            }, function (error) {
                console.log(error);
                console.log("joystickPBM");
            });
        });
    };
    ApiService.prototype.checkrobot = function () {
        var _this = this;
        this.httpClient
            .get(this.httpskomnav + this.param.localhost + "/api/battery/state")
            .subscribe(function (data) {
            _this.robotok = true;
        }, function (err) {
            console.log(err);
            _this.robotok = false;
        });
    };
    ApiService.prototype.jetsonOK = function () {
        var _this = this;
        this.checkInternet();
        this.alert.checkwifi(this.wifiok);
        this.httpClient
            .get(this.httpskomnav + this.param.localhost + "/api/battery/state", {
            observe: "response",
        })
            .subscribe(function (resp) {
            if (_this.statusRobot === 2) {
                _this.deleteEyesHttp(23);
                _this.alert.appError(_this.mailAddInformationBasic());
                document.location.reload(); //if error then refresh the page and relaunch websocket
            }
            if (resp.status === 200) {
                _this.cpt_jetsonOK = 0;
                _this.statusRobot = 0; // everything is ok
                _this.connect();
            }
            else {
                _this.cpt_jetsonOK += 1;
                if (_this.cpt_jetsonOK > 5) {
                    _this.statusRobot = 2; //no connection
                    _this.connectionLost();
                }
            }
        }, function (err) {
            console.log(err); //no conection
            _this.cpt_jetsonOK += 1;
            if (_this.cpt_jetsonOK > 10) {
                _this.statusRobot = 2;
                _this.connectionLost();
            }
        });
    };
    ApiService.prototype.okToast = function (m) {
        var toast = this.toastCtrl.create({
            message: m,
            duration: 3000,
            position: "middle",
            cssClass: "toastok",
        });
        toast.present();
    };
    ApiService.prototype.isConnectedInternet = function () {
        return __awaiter(this, void 0, void 0, function () {
            var response, text, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        return [4 /*yield*/, fetch('http://localhost/ionicDB/internetping.php')];
                    case 1:
                        response = _a.sent();
                        return [4 /*yield*/, response.text()];
                    case 2:
                        text = _a.sent();
                        //console.log(text);
                        // Analyse du texte pour déterminer la connectivité Internet
                        return [2 /*return*/, text.trim() === 'true']; // Renvoie true si le texte est 'true', sinon false
                    case 3:
                        error_1 = _a.sent();
                        console.error('Error checking internet connectivity:', error_1);
                        return [2 /*return*/, false];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    ApiService.prototype.checkInternet = function () {
        var _this = this;
        this.isConnectedInternet().then(function (connecte) {
            if (connecte) {
                //console.log("L'ordinateur est connecté à Internet");
                _this.wifiok = true;
            }
            else {
                //console.log("L'ordinateur n'est pas connecté à Internet");
                _this.wifiok = false;
            }
        });
    };
    ApiService.prototype.pauseHttp = function (isblocked) {
        var _this = this;
        // suspend the round
        //var t0 = performance.now()
        this.httpClient
            .put(this.httpskomnav + this.param.localhost + "/api/rounds/current/pause", {
            observe: "response",
        })
            .subscribe(function (resp) {
            //var t1 = performance.now()
            //this.okToast("Took " + (t1 - t0) + " ms.");
            console.log(resp);
            console.log("pauseOK");
            if (!isblocked) {
                _this.roundActive = false;
            }
        }, function (err) {
            console.log(err);
            console.log("pausePBM");
        });
    };
    ApiService.prototype.connectHttp = function () {
        this.httpClient
            .put(this.httpskomnav + this.param.localhost + "/api/docking/connect", {
            observe: "response",
        })
            .subscribe(function (resp) {
            console.log(resp);
            console.log("connectOK");
        }, function (err) {
            console.log(err);
            console.log("connectPBM");
        });
    };
    ApiService.prototype.eyesHttp = function (id) {
        this.httpClient
            .put(this.httpskomnav + this.param.localhost + "/api/eyes?id=" + id, {
            observe: "response",
        })
            .subscribe(function (resp) {
            console.log(resp);
            console.log("eyesOK");
        }, function (err) {
            console.log(err);
            console.log("eyesPBM");
        });
    };
    ApiService.prototype.deleteEyesHttp = function (id) {
        this.httpClient
            .delete(this.httpskomnav + this.param.localhost + "/api/eyes?id=" + id, {
            observe: "response",
        })
            .subscribe(function (resp) {
            console.log(resp);
            console.log("deleteeyesOK");
        }, function (err) {
            console.log(err);
            console.log("deleteeyesPBM");
        });
    };
    ApiService.prototype.disconnectHttp = function () {
        this.httpClient
            .put(this.httpskomnav + this.param.localhost + "/api/docking/disconnect", {
            observe: "response",
        })
            .subscribe(function (resp) {
            console.log(resp);
            console.log("disconnectOK");
        }, function (err) {
            console.log(err);
            console.log("disconnectPBM");
        });
    };
    ApiService.prototype.abortDockingHttp = function () {
        this.httpClient
            .put(this.httpskomnav + this.param.localhost + "/api/docking/abort", {
            observe: "response",
        })
            .subscribe(function (resp) {
            console.log(resp);
            console.log("abortdockingOK");
        }, function (err) {
            console.log(err);
            console.log("abortdockingPBM");
        });
    };
    ApiService.prototype.reachHttp = function (poiname) {
        this.httpClient
            .put(this.httpskomnav +
            this.param.localhost +
            "/api/navigation/destination/name/" +
            poiname, { observe: "response" })
            .subscribe(function (resp) {
            console.log(resp);
            console.log("reachOK");
        }, function (err) {
            console.log(err);
            console.log("reachPBM");
        });
    };
    ApiService.prototype.abortHttp = function () {
        // stop the round
        this.start = false;
        this.httpClient
            .put(this.httpskomnav + this.param.localhost + "/api/rounds/current/abort", {
            observe: "response",
        })
            .subscribe(function (resp) {
            console.log(resp);
            console.log("abortOK");
        }, function (err) {
            console.log(err);
            console.log("abortPBM");
        });
    };
    ApiService.prototype.abortNavHttp = function () {
        // stop the navigation
        this.httpClient
            .put(this.httpskomnav + this.param.localhost + "/api/navigation/abort", {
            observe: "response",
        })
            .subscribe(function (resp) {
            console.log(resp);
            console.log("abortNavOK");
        }, function (err) {
            console.log(err);
            console.log("abortNavPBM");
        });
    };
    ApiService.prototype.resumeHttp = function () {
        // resume round
        this.httpClient
            .put(this.httpskomnav + this.param.localhost + "/api/rounds/current/resume", {
            observe: "response",
        })
            .subscribe(function (resp) {
            console.log(resp);
            console.log("resumeOK");
        }, function (err) {
            console.log(err);
            console.log("resumePBM");
        });
    };
    ApiService.prototype.skipHttp = function () {
        this.httpClient
            .put(this.httpskomnav + this.param.localhost + "/api/rounds/current/skip", {
            observe: "response",
        })
            .subscribe(function (resp) {
            console.log(resp);
            console.log("skipOK");
        }, function (err) {
            console.log(err);
            console.log("skipPBM");
        });
    };
    ApiService.prototype.acknowledgeHttp = function () {
        //go to next poi
        this.httpClient
            .put(this.httpskomnav + this.param.localhost + "/api/rounds/current/acknowledge", { observe: "response" })
            .subscribe(function (resp) {
            console.log(resp);
            console.log("acknowledgeOK");
        }, function (err) {
            console.log(err);
            console.log("acknowledgePBM");
        });
    };
    ApiService.prototype.startRoundHttp = function (idRound) {
        // start the round
        this.httpClient
            .put(this.httpskomnav + this.param.localhost + "/api/rounds/current/id/" + idRound, { observe: "response" })
            .subscribe(function (resp) {
            console.log(resp);
            console.log("startRoundOK");
        }, function (err) {
            console.log(err);
            console.log("startRoundPBM");
        });
    };
    ApiService.prototype.getCurrentMap = function () {
        var _this = this;
        this.httpClient
            .get(this.httpskomnav + this.param.localhost + "/api/maps/current/properties")
            .subscribe(function (data) {
            //console.log(data);
            _this.mapdata = data;
            //console.log(this.mapdata.Id);
            if (_this.mapdata) {
                _this.id_current_map = _this.mapdata.Id;
            }
        }, function (err) {
            console.log(err);
        });
    };
    ApiService.prototype.getAllMapsHttp = function () {
        var _this = this;
        this.httpClient
            .get(this.httpskomnav + this.param.localhost + "/api/maps/list")
            .subscribe(function (data) {
            console.log(data);
            console.log("getAllMapsOK");
            _this.all_maps = data;
            //console.log(this.mapdata.Id);
            // if(this.all_maps!=undefined){
            // }
        }, function (err) {
            console.log(err);
        });
    };
    ApiService.prototype.getRoundList = function () {
        var _this = this;
        this.httpClient
            .get(this.httpskomnav + this.param.localhost + "/api/rounds/list")
            .subscribe(function (data) {
            _this.rounddata = data;
            if (_this.id_current_map && _this.rounddata) {
                _this.round_current_map = _this.rounddata.filter(function (round) { return round.Map === _this.id_current_map; });
            }
            console.log(_this.round_current_map);
            _this.round_map_selected = _this.round_current_map;
        }, function (err) {
            console.log(err);
        });
    };
    ApiService.prototype.getRoundListAutoLaunch = function (map_id) {
        var _this = this;
        this.httpClient
            .get(this.httpskomnav + this.param.localhost + "/api/rounds/list")
            .subscribe(function (data) {
            _this.rounddata = data;
            //console.log(this.rounddata);
            if (map_id && _this.rounddata) {
                _this.round_map_selected = _this.rounddata.filter(function (round) { return round.Map == map_id; });
            }
        }, function (err) {
            console.log(err);
        });
    };
    ApiService.prototype.connect = function () {
        this.is_connected = true;
    };
    ApiService.prototype.connectionLost = function () {
        this.is_connected = false;
    };
    ApiService.prototype.background = function () {
        this.is_background = true;
    };
    ApiService.prototype.foreground = function () {
        this.is_background = false;
    };
    ApiService.prototype.getImgDetectionHttp = function () {
        return this.httpClient.get(this.httpskomnav + this.param.localhost + "/detections/detection_image.png", { responseType: "blob" });
    };
    ApiService.prototype.startPeopleDetection = function (start) {
        var _this = this;
        var servicePeopleDetection = new ROSLIB.Service({
            ros: this.ros,
            name: '/toggle_fall_detection_node',
            serviceType: 'std_srvs/SetBool'
        });
        var request = new ROSLIB.ServiceRequest({
            data: start,
        });
        servicePeopleDetection.callService(request, function (result) {
            console.log("*********service peopledetect****************");
            console.log(result);
        });
        if (start) {
            this.listener_fall.subscribe(function (message) {
                //console.log('Received message on ' + this.listener_fall.name + ': ' + message.data);
                _this.fdetected = message.data;
                if (message.data && _this.stoplookFall === 0 && _this.param.patrolInfo.round_fall_detection == 1) {
                    console.log("fall");
                    _this.stoplookFall = 1;
                    _this.falldetected = true;
                    _this.fallinterv = setInterval(function () { return _this.stopLookFall(); }, 1000);
                }
            });
            this.listener_person.subscribe(function (message) {
                //console.log('Received message on ' + this.listener_person.name + ': ' + message.data);
                _this.pdetected = message.data;
            });
        }
        else {
            this.listener_person.unsubscribe();
            this.listener_fall.unsubscribe();
        }
    };
    ApiService.prototype.startMaskDetection = function (start) {
        var _this = this;
        var serviceMaskDetection = new ROSLIB.Service({
            ros: this.ros,
            name: '/toggle_mask_detection_node',
            serviceType: 'std_srvs/SetBool'
        });
        var request = new ROSLIB.ServiceRequest({
            data: start,
        });
        serviceMaskDetection.callService(request, function (result) {
            console.log("*********service mask****************");
            console.log(result);
        });
        if (start) {
            this.listener_mask.subscribe(function (message) {
                //console.log('Received message on ' + this.listener_mask.name + ': ' + message.data);
                _this.maskdetected = message.data;
                // if (this.maskdetected){
                //   console.log("alert mask");
                // }
            });
        }
        else {
            this.listener_mask.unsubscribe();
        }
    };
    ApiService.prototype.getImgMaskHttp = function () {
        return this.httpClient.get(this.httpskomnav + this.param.localhost + "/detections/mask_detection_image.png", { responseType: "blob" });
    };
    ApiService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_3__alert_service__["a" /* AlertService */],
            __WEBPACK_IMPORTED_MODULE_4__param_service__["a" /* ParamService */]])
    ], ApiService);
    return ApiService;
}());

//# sourceMappingURL=api.service.js.map

/***/ }),

/***/ 259:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(18);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(213);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__components_components_module__ = __webpack_require__(308);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_component__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_home_home__ = __webpack_require__(115);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__components_headpage_headpage__ = __webpack_require__(219);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__services_api_service__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__services_alert_service__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_common_http__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_background_mode__ = __webpack_require__(360);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_tuto_tuto__ = __webpack_require__(220);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__services_popup_service__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__services_param_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__services_speech_service__ = __webpack_require__(51);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_param_param__ = __webpack_require__(231);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_langue_langue__ = __webpack_require__(233);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_sms_sms__ = __webpack_require__(232);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_mail_mail__ = __webpack_require__(120);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_first_first__ = __webpack_require__(218);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_annonce_annonce__ = __webpack_require__(116);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_password_password__ = __webpack_require__(234);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_robotBehaviour_robotBehaviour__ = __webpack_require__(235);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_autolaunch_autolaunch__ = __webpack_require__(237);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_radio_radio__ = __webpack_require__(236);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



























var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_21__pages_first_first__["a" /* FirstPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_tuto_tuto__["a" /* TutoPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_langue_langue__["a" /* LanguePage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_mail_mail__["a" /* MailPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_sms_sms__["a" /* SMSPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_annonce_annonce__["a" /* AnnoncePage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_password_password__["a" /* PasswordPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_param_param__["a" /* ParamPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_robotBehaviour_robotBehaviour__["a" /* RobotBehaviourPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_autolaunch_autolaunch__["a" /* AutoLaunchPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_radio_radio__["a" /* RadioPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_5__components_components_module__["a" /* ComponentsModule */],
                __WEBPACK_IMPORTED_MODULE_11__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */], {}, {
                    links: []
                }),
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_6__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_21__pages_first_first__["a" /* FirstPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_tuto_tuto__["a" /* TutoPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_param_param__["a" /* ParamPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_langue_langue__["a" /* LanguePage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_mail_mail__["a" /* MailPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_sms_sms__["a" /* SMSPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_annonce_annonce__["a" /* AnnoncePage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_password_password__["a" /* PasswordPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_robotBehaviour_robotBehaviour__["a" /* RobotBehaviourPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_autolaunch_autolaunch__["a" /* AutoLaunchPage */],
                __WEBPACK_IMPORTED_MODULE_8__components_headpage_headpage__["a" /* HeadpageComponent */],
                __WEBPACK_IMPORTED_MODULE_26__pages_radio_radio__["a" /* RadioPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_12__ionic_native_background_mode__["a" /* BackgroundMode */],
                __WEBPACK_IMPORTED_MODULE_9__services_api_service__["a" /* ApiService */],
                __WEBPACK_IMPORTED_MODULE_10__services_alert_service__["a" /* AlertService */],
                __WEBPACK_IMPORTED_MODULE_14__services_popup_service__["a" /* PopupService */],
                __WEBPACK_IMPORTED_MODULE_15__services_param_service__["a" /* ParamService */],
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_16__services_speech_service__["a" /* SpeechService */],
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicErrorHandler */] },
                { provide: Window, useValue: window },
            ],
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 308:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_component__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__headpage_headpage__ = __webpack_require__(219);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ComponentsModule = /** @class */ (function () {
    function ComponentsModule() {
    }
    ComponentsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_3__headpage_headpage__["a" /* HeadpageComponent */]],
            imports: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_2__app_app_component__["a" /* MyApp */])],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_2__app_app_component__["a" /* MyApp */]
            ],
            exports: [__WEBPACK_IMPORTED_MODULE_3__headpage_headpage__["a" /* HeadpageComponent */]]
        })
    ], ComponentsModule);
    return ComponentsModule;
}());

//# sourceMappingURL=components.module.js.map

/***/ }),

/***/ 31:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PopupService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__alert_service__ = __webpack_require__(35);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__api_service__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__param_service__ = __webpack_require__(10);
// to send mail and sms alert
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};






var PopupService = /** @class */ (function () {
    function PopupService(app, alert, param, api, alertCtrl, toastCtrl) {
        this.app = app;
        this.alert = alert;
        this.param = param;
        this.api = api;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
    }
    PopupService.prototype.onSomethingHappened1 = function (fn) {
        this.accessparam = fn;
    };
    PopupService.prototype.onSomethingHappened2 = function (fn) {
        this.addMail = fn;
    };
    PopupService.prototype.onSomethingHappened3 = function (fn) {
        this.accessautolaunch = fn;
    };
    PopupService.prototype.ngOnInit = function () { };
    PopupService.prototype.presentAlert = function () {
        var _this = this;
        // pop up if the robot is in the docking when you start the round
        var alert = this.alertCtrl.create({
            title: this.param.datatext.presentAlert_title,
            message: this.param.datatext.presentAlert_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: false,
            buttons: [
                {
                    text: this.param.datatext.btn_cancel,
                    cssClass: "cancelpopup",
                    role: "cancel",
                    handler: function () {
                        console.log("cancel clicked");
                    },
                },
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        console.log("Yes clicked");
                        _this.api.roundActive = true;
                        _this.api.fct_startRound = true;
                        setTimeout(function () {
                            _this.alert.roundLaunched(_this.api.mailAddInformationBasic());
                        }, 3000);
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.presentConfirm = function () {
        var _this = this;
        // pop up code to resume or stop the round
        var alert = this.alertCtrl.create({
            title: this.param.datatext.presentConfirm_title,
            message: this.param.datatext.presentConfirm_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_no,
                    role: "cancel",
                    handler: function () {
                        console.log("Non clicked");
                    },
                },
                {
                    text: this.param.datatext.btn_yes,
                    handler: function () {
                        console.log("Oui clicked");
                        _this.api.fct_onGo = true;
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.RemoteConfirm = function () {
        var _this = this;
        // pop up code to resume or stop the round
        var alert = this.alertCtrl.create({
            title: this.param.datatext.RemoteConfirm_title,
            message: this.param.datatext.RemoteConfirm_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_no,
                    role: "cancel",
                    handler: function () {
                        console.log("No clicked");
                    },
                },
                {
                    text: this.param.datatext.btn_yes,
                    handler: function () {
                        console.log("Yes clicked");
                        _this.api.fct_onGo = true;
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.blockedAlert = function () {
        this.alert_blocked = this.alertCtrl.create({
            title: this.param.datatext.blockedAlert_title,
            message: this.param.datatext.blockedAlert_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        console.log("Oui clicked");
                    },
                },
            ],
        });
        this.alert_blocked.present();
    };
    PopupService.prototype.StartBlockedAlert = function () {
        this.alert_blocked = this.alertCtrl.create({
            title: this.param.datatext.startblockedAlert_title,
            message: this.param.datatext.startblockedAlert_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        console.log("Oui clicked");
                    },
                },
            ],
        });
        this.alert_blocked.present();
    };
    PopupService.prototype.relocAlert = function () {
        this.alert_blocked = this.alertCtrl.create({
            title: this.param.datatext.relocAlert_title,
            message: this.param.datatext.relocAlert_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: false,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        console.log("Oui clicked");
                        window.close();
                    },
                },
            ],
        });
        this.alert_blocked.present();
    };
    PopupService.prototype.startFailedAlert = function () {
        this.alert_blocked = this.alertCtrl.create({
            title: this.param.datatext.error,
            message: this.param.datatext.cantstart,
            cssClass: "alertstyle",
            enableBackdropDismiss: false,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        console.log("Oui clicked");
                        window.close();
                    },
                },
            ],
        });
        this.alert_blocked.present();
    };
    PopupService.prototype.lostAlert = function () {
        this.alert_blocked = this.alertCtrl.create({
            title: this.param.datatext.lostAlert_title,
            message: this.param.datatext.lostAlert_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        console.log("Oui clicked");
                    },
                },
            ],
        });
        this.alert_blocked.present();
    };
    PopupService.prototype.errorlaunchAlert = function () {
        this.alert_blocked = this.alertCtrl.create({
            title: this.param.datatext.errorlaunchAlert_title,
            message: this.param.datatext.errorlaunchAlert_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        console.log("Oui clicked");
                    },
                },
            ],
        });
        this.alert_blocked.present();
    };
    PopupService.prototype.errorNavAlert = function () {
        this.alert_blocked = this.alertCtrl.create({
            title: this.param.datatext.errorNavAlert_title,
            message: this.param.datatext.errorNavAlert_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        console.log("Oui clicked");
                    },
                },
            ],
        });
        this.alert_blocked.present();
    };
    PopupService.prototype.robotmuststayondocking = function () {
        // pop up if the robot is in the docking when you start the round
        this.alert_blocked = this.alertCtrl.create({
            title: this.param.datatext.robotmuststayondocking_title,
            message: this.param.datatext.robotmuststayondocking_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        console.log("Oui clicked");
                    },
                },
            ],
        });
        this.alert_blocked.present();
    };
    PopupService.prototype.quitConfirm = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: this.param.datatext.quitConfirm_title,
            cssClass: "alertstyle",
            enableBackdropDismiss: false,
            buttons: [
                {
                    text: this.param.datatext.btn_no,
                    role: "cancel",
                    handler: function () {
                        console.log("Non clicked");
                    },
                },
                {
                    text: this.param.datatext.btn_yes,
                    handler: function () {
                        console.log("Oui clicked");
                        _this.api.close_app = true;
                        _this.alert.appClosed(_this.api.mailAddInformationBasic());
                        //stop the round and quit the app
                        _this.api.abortHttp();
                        _this.api.deleteEyesHttp(23);
                        setTimeout(function () {
                            window.close();
                        }, 1000);
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.goDockingConfirm = function () {
        var _this = this;
        // pop up to ask if the robot must go to the docking
        var alert = this.alertCtrl.create({
            title: this.param.datatext.goDockingConfirm_title +
                this.api.battery_status.remaining +
                "%",
            message: this.param.datatext.goDockingConfirm_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: false,
            buttons: [
                {
                    text: this.param.datatext.btn_no,
                    role: "cancel",
                    handler: function () {
                        console.log("Non clicked");
                        _this.api.towardDocking = false;
                    },
                },
                {
                    text: this.param.datatext.btn_yes,
                    handler: function () {
                        console.log("Oui clicked");
                        _this.api.towardDocking = true;
                        _this.api.reachHttp("docking");
                        _this.alert.askCharge(_this.api.mailAddInformationBasic());
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.lowBattery = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: this.param.datatext.lowBattery_title,
            message: this.param.datatext.lowBattery_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: false,
            buttons: [
                {
                    text: this.param.datatext.btn_no,
                    role: "cancel",
                    handler: function () {
                        console.log("Non clicked");
                        _this.api.towardDocking = false;
                    },
                },
                {
                    text: this.param.datatext.btn_yes,
                    handler: function () {
                        console.log("Oui clicked");
                        _this.api.towardDocking = true;
                        //console.log(this.api.towardDocking=true);
                        _this.api.reachHttp("docking");
                        _this.alert.askCharge(_this.api.mailAddInformationBasic());
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.errorBlocked = function () {
        var alert = this.alertCtrl.create({
            title: this.param.datatext.errorBlocked_title,
            message: this.param.datatext.errorBlocked_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        console.log("OK clicked");
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.statusRedPresent = function () {
        var alert = this.alertCtrl.create({
            title: this.param.datatext.statusRedPresent_title,
            message: this.param.datatext.statusRedPresent_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        console.log("OK clicked");
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.statusGreenPresent = function () {
        var alert = this.alertCtrl.create({
            title: this.param.datatext.statusGreenPresent_title,
            message: this.param.datatext.statusGreenPresent_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        console.log("OK clicked");
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.leaveDockingConfirm = function () {
        var _this = this;
        // pop up to ask if the robot must leave the docking
        var alert = this.alertCtrl.create({
            title: this.param.datatext.leaveDockingConfirm_title +
                this.api.battery_status.remaining +
                "%",
            message: this.param.datatext.leaveDockingConfirm_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_no,
                    role: "cancel",
                    handler: function () {
                        console.log("Non clicked");
                    },
                },
                {
                    text: this.param.datatext.btn_yes,
                    handler: function () {
                        console.log("Oui clicked");
                        _this.alert.leaveDocking(_this.api.mailAddInformationBasic());
                        _this.api.disconnectHttp();
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.askpswd = function (pageName) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: this.param.datatext.password,
            message: this.param.datatext.enterPassword,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            inputs: [
                {
                    name: "password",
                    placeholder: this.param.datatext.password,
                    type: "password",
                },
            ],
            buttons: [
                {
                    text: this.param.datatext.btn_cancel,
                    role: "cancel",
                    handler: function (data) {
                        console.log("Cancel clicked");
                    },
                },
                {
                    text: this.param.datatext.btn_save,
                    role: "backdrop",
                    handler: function (data) {
                        if (data.password === atob(_this.param.robot.password)) {
                            if (pageName == "autolaunch")
                                _this.accessautolaunch();
                            else
                                _this.accessparam();
                        }
                        else {
                            _this.wrongPassword(pageName);
                        }
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.wrongPassword = function (pageName) {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: this.param.datatext.password,
            message: this.param.datatext.wrongPassword,
            cssClass: "alertstyle_wrongpass",
            enableBackdropDismiss: true,
            inputs: [
                {
                    name: "password",
                    placeholder: this.param.datatext.password,
                    type: "password",
                },
            ],
            buttons: [
                {
                    text: this.param.datatext.btn_cancel,
                    role: "cancel",
                    handler: function (data) {
                        console.log("Cancel clicked");
                    },
                },
                {
                    text: this.param.datatext.btn_save,
                    role: "backdrop",
                    handler: function (data) {
                        if (data.password === atob(_this.param.robot.password)) {
                            if (pageName == "autolaunch")
                                _this.accessautolaunch();
                            else
                                _this.accessparam();
                        }
                        else {
                            _this.wrongPassword(pageName);
                        }
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.displayrgpd = function (ev) {
        var _this = this;
        ev.preventDefault();
        var alert = this.alertCtrl.create({
            title: "RGPD",
            message: this.param.datatext.rgpd_txt,
            cssClass: "alertstyle",
            enableBackdropDismiss: true,
            buttons: [
                {
                    text: this.param.datatext.btn_cancel,
                    role: "cancel",
                    handler: function (data) {
                        console.log("Cancel clicked");
                    },
                },
                {
                    text: this.param.datatext.btn_save,
                    role: "backdrop",
                    handler: function (data) {
                        _this.addMail(ev);
                    }
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.showToast = function (msg, duration, position) {
        return __awaiter(this, void 0, void 0, function () {
            var toast;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtrl.create({
                            message: msg,
                            duration: duration,
                            position: position,
                            cssClass: "toastok"
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    PopupService.prototype.FallConfirm = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: this.param.datatext.FallConfirm_title,
            message: this.param.datatext.FallConfirm_message,
            cssClass: "alertstyle",
            enableBackdropDismiss: false,
            buttons: [
                {
                    text: this.param.datatext.btn_ok,
                    handler: function () {
                        console.log("OK clicked");
                        _this.api.popupFall = false;
                    },
                },
            ],
        });
        alert.present();
    };
    PopupService.prototype.showToastRed = function (msg, duration, position) {
        return __awaiter(this, void 0, void 0, function () {
            var toast;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastCtrl.create({
                            message: msg,
                            duration: duration,
                            position: position,
                            cssClass: "toast"
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    PopupService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_2__alert_service__["a" /* AlertService */],
            __WEBPACK_IMPORTED_MODULE_4__param_service__["a" /* ParamService */],
            __WEBPACK_IMPORTED_MODULE_3__api_service__["a" /* ApiService */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* ToastController */]])
    ], PopupService);
    return PopupService;
}());

//# sourceMappingURL=popup.service.js.map

/***/ }),

/***/ 314:
/***/ (function(module, exports) {

module.exports = {"quit":"QUITTER","battery":"BATTERIE","return":"RETOUR","tutorial":"TUTORIEL","param":"PARAMÈTRES","announceEdit":"EDITEUR D'ANNONCES","receivesms":"RECEVOIR DES ALERTES SMS","receivemail":"RECEVOIR DES ALERTES PAR MAIL","changelangage":"CHANGER LA LANGUE","godocking":"EN ROUTE VERS LA STATION DE CHARGE","round":"TOURNÉE DE DIVERTISSEMENT","patrol":"Patrouille","patrolInProgress":"PATROUILLE EN COURS","roundInProgress":"TOURNÉE DE DIVERTISSEMENT EN COURS","moving":"DÉPLACEMENT EN COURS VERS :","charging_remaining":"ROBOT EN CHARGE - BATTERIE: ","walkInProgress":"MARCHEZ A VOTRE RYTHME SANS POUSSER","notpush":"INSTALLEZ VOUS COMME SUR LA PHOTO ET APPUYEZ SUR 'ALLONS-Y'","mails":"Mails","sms":"SMS","langage":"Langue","goto":"Se rendre au :","inProgress":"EN COURS","post":"Poste","sentinel":"Sentinelle","btn_go":"LANCER","btn_help":"Aide","btn_walk":"Allons-y !","btn_stop":"STOP","btn_charge":"CHARGER","btn_cancel":"Annuler","btn_ok":"OK","btn_yes":"OUI","btn_no":"NON","numexist":"Ce numéro existe déjà !","deletenum":"Veuillez supprimer un numéro d'abord","numadd":"Numéro ajouté","numincorrect":"Numéro invalide","newnum":"Nouveau numéro","mailexist":"Cette adresse existe déjà ! ","deletemail":"Veuillez supprimer une adresse d'abord","mailadd":"Adresse mail ajoutée !","mailincorrect":"Adresse mail invalide","newmail":"Nouvelle adresse","error":"Erreur","cantstart":"Impossible de démarrer l'application. Vérifiez que le robot est allumé correctement.","nointernet":"Pas d'internet","btn_save":"Sauver","receivepic":"Recevoir des photos","editpswd":"CHANGER LE MOT DE PASSE","currentpswd":"Mot de passe actuel :","newpswd":"Nouveau mot de passe :","save":"Sauvegarder","success":"C'est Fait !","pswdsaved":"Mot de passe sauvegardé","langage1":"Changer la langue","editpswd1":"Changer le mot de passe","wrongpass":"Mauvais mot de passe","smsSOS":"Quelqu'un a appuyé sur le bouton SOS du robot","mailSOS_suj":"SOS","mailSOS_body":"Une personne a besoin d'aide","mailBattery_suj":"Batterie Faible","mailBattery_body":"Le robot a besoin d'etre chargé. Veuillez le mettre sur sa station de charge.","mailBlocked_suj":"Robot bloqué","mailBlocked_body":"Le robot est bloqué","mailFall_body":"Le robot détecte une personne à terre","mailFall_suj":"CHUTE détectée","mailPerson_suj":"Personne détectée","mailPerson_body":"Le robot a détecté une personne","mailRemove":"Suppression réussie","presentAlert_title":"Robot en charge","AlertConnectedToI":"Le robot est connecté à internet","presentAlert_message":"Attention je vais reculer !","presentConfirm_title":"Ronde suspendue","presentConfirm_message":"Reprendre la ronde ?","FallConfirm_title":"Chute détectée !","FallConfirm_message":"Le robot a détecté une chute","RemoteConfirm_title":"Ronde arrêtée à distance","RemoteConfirm_message":"Reprendre la ronde ?","blockedAlert_title":"Robot bloqué !","blockedAlert_message":"Veuillez enlever l'obstacle ou me déplacer","goDockingConfirm_title":"Batterie: ","goDockingConfirm_message":"Aller en station de charge ?","lowBattery_title":"Batterie faible","lowBattery_message":"Aller en station de charge ?","errorlaunchAlert_title":"Une erreur est survenue","errorlaunchAlert_message":"Veuillez rententer de lancer la ronde","robotmuststayondocking_title":"Batterie faible","robotmuststayondocking_message":"Le robot doit rester sur la docking","errorNavAlert_title":"Une erreur est survenue","errorNavAlert_message":"Veuillez appeler le support technique si le problème persiste","lostAlert_title":"Robot perdu !","lostAlert_message":"Veuillez appeler le support technique","quitConfirm_title":"Quitter l'application ?","errorBlocked_title":"Robot bloqué","errorBlocked_message":"Veuillez vérifier que le robot est bien dégagé","statusRedPresent_title":"Erreur","statusRedPresent_message":"Veuillez me redémarrer OU appeler mon fabricant si le problème persiste","statusGreenPresent_title":"Statut vert","statusGreenPresent_message":"Le robot est en bonne santé !","leaveDockingConfirm_title":"Batterie: ","leaveDockingConfirm_message":"Sortir de la station de charge ?","leaveDockingAlert_title":"Robot en charge","leaveDockingAlert_message":"Veuillez me retirer de la station de charge","askHelp_title":"Aide demandée","askHelp_message":"Quelqu'un va venir vous aider","startblockedAlert_title":"Obstacle détecté à l'avant","startblockedAlert_message":"Veuillez reculer le robot avant de relancer la ronde.","freq":"Fréquence","freqAnnounce":"Fréquence des annonces","every30sec":"Toutes les 30 secondes","every1mn":"Toutes les minutes","every2mn":"Toutes les 2 min","every3mn":"Toutes les 3 min","every4mn":"Toutes les 4 min","every5mn":"Toutes les 5 min","weather":"Météo","date":"Date","newAnnouncement":"Nouvelle Annonce","addLoading":"Ajout en cours ...","addDone":"Annonce ajoutée !","selectRound":"Choisir une ronde","roundDuration":"Durée de la ronde","rounds":"Rondes","announce1":"Bonjour ! Aujourd'hui, nous sommes le ","announce2":" et il est ","announcement":"Annonces","announce3":"Bonjour, voici la prévision météo du jour : ","announce4":" avec une température de ","announce5":"degrés","relocAlert_title":"Robot perdu","relocAlert_message":"Veuillez relocaliser le robot","password":"Mot de passe","enterPassword":"Nécessaire pour accéder à cette page.","wrongPassword":"Mot de passe incorrect. Veuillez réessayer.","autolaunch":"Lancement auto de la mission : ","URL_tourneeapp":"assets/pdf/tuto_FR.pdf","rgpd_txt":"Conformément à la loi du 6 janvier 1978 modifiée et au « Règlement Général de Protection des Données », vos données personnelles de contact sont collectées afin que vous puissiez recevoir les alertes du robot Kompai (personne détectée, robot bloqué, batterie faible..).  Vos données seront traitées par Korian et Kompai pendant la durée requise pour l’utilisation de la plateforme. Vous pouvez, notamment, vous opposez au traitement de vos données,  obtenir une copie de vos données, les rectifier ou les supprimer en écrivant au DPO et en justifiant votre identité à l’adresse : rgpd@kompai.com ou KOMPAI Robotics, Technopole d'Izarbel - 97 allée Théodore Monod 64210 Bidart - France.","autolaunchHeadPage":"LANCEMENT AUTO","createMission":"CRÉER UN ÉVÉNEMENT","map":"Carte","roundSelection":"Ronde","duration":"Durée","type":"Type","adhoc":"ponctuel","daily":"quotidien","weekly":"hebdomadaire","create":"CRÉER","today":"Aujourd'hui","all":"Tous","missionAdded":"Mission programée","dailyLetter":"q","weeklyLetter":"h","mapSelect":"Choisir une carte","durationSelect":"Choisir une durée","typeSelect":"Choisir un type","roundSelect":"Choisir une ronde","pleasemask":"Veuillez mettre un masque !","speechmask":"Bonjour, veuillez mettre un masque s'il vous plait !","robotBehaviour":"Comportement du robot","fallDetectionActivation":"Activer la détection de chute","maskDetectionActivation":"Activer la détection du non port de masque","radio":"Radio","newradio":"Nouvelle radio","radioRemove":"Suppression réussie","radioexist":"Cette radio existe déjà !","radioadd":"Radio ajoutée","radioincorrect":"Radio invalide !","radioHeadPage":"Éditeur radio","NoRadio":"Pas de radio","mp3":"MP3","folder":"Dossier","music":"Musique","NoMusic":"Pas de musique"}

/***/ }),

/***/ 315:
/***/ (function(module, exports) {

module.exports = {"quit":"LEAVE","battery":"BATTERY","return":"RETURN","tutorial":"TUTORIAL","param":"SETTINGS","announceEdit":"EDITOR OF ANNOUNCEMENT","receivesms":"RECEIVE ALERTS BY SMS","receivemail":"RECEIVE ALERTS BY EMAIL","changelangage":"CHANGE THE LANGUAGE","godocking":"ON THE WAY TO THE CHARGING STATION","round":"ENTERTAINMENT TOUR","patrol":"Patrol","patrolInProgress":"PATROLLING","roundInProgress":"ENTERTAINMENT TOUR IN PROGRESS","moving":"TRAVELING IN PROGRESS TO :","charging_remaining":"ROBOT IN CHARGE - BATTERY: ","walkInProgress":"WALK AT YOUR OWN PACE WITHOUT PUSHING","notpush":"INSTALL YOURSELF AS IN THE PHOTO AND PRESS 'LET'S GO'","mails":"Emails","sms":"SMS","langage":"Language","distance_covered":"Distance travelled","goto":"Go to :","inProgress":"IN PROGRESS","post":"Position","sentinel":"Sentry","morningRound":"Morning tour","eveningRound":"Evening tour","btn_go":"GO","btn_help":"Help","btn_walk":"Let's go !","btn_stop":"STOP","btn_charge":"LOAD","btn_cancel":"Cancel","btn_ok":"OK","btn_yes":"YES","btn_no":"NO","numexist":"This phone number already exists !","deletenum":"Please delete a phone number first","numadd":"Phone number added","numincorrect":"Invalid phone number","newnum":"New phone number","mailexist":"This address already exists ! ","deletemail":"Please delete an address first","mailadd":"Email address added!","mailincorrect":"Invalid email address","newmail":"New address","error":"Error","cantstart":"Application launching failed. Check that the robot is correctly switched on.","nointernet":"No internet access","btn_save":"Save","receivepic":"Receive pictures","editpswd":"EDIT PASSWORD","currentpswd":"Current password :","newpswd":"New password :","save":"Save","success":"Success","pswdsaved":"Password saved","langage1":"Langage","editpswd1":"Edit password","wrongpass":"Wrong password","smsSOS":"Someone pressed the robot's SOS button","mailSOS_suj":"SOS","mailSOS_body":"<br> A person needs help","mailBattery_suj":"Low Battery","mailBattery_body":"<br> The robot needs to be charged. Please put it on its charging station.","mailBlocked_suj":"Robot blocked","mailBlocked_body":"<br> The robot is blocked <br>","mailFall_body":"The robot detects a person ashore <br>","mailFall_suj":"FALL detected","mailPerson_suj":"Person detected","mailPerson_body":"The robot detected a person <br>","mailRemove":"Deletion done","presentAlert_title":"Robot in charge","AlertConnectedToI":"The robot is connected to internet","presentAlert_message":"I'm going to back up !","presentConfirm_title":"Round Suspended","presentConfirm_message":"Continue the round ?","FallConfirm_title":"Fall detected !","FallConfirm_message":"The robot has detected a fall","RemoteConfirm_title":"Remote stop round","RemoteConfirm_message":"Continue the round ?","blockedAlert_title":"Robot blocked !","blockedAlert_message":"Please remove the obstacle or move me","goDockingConfirm_title":"Battery: ","goDockingConfirm_message":"Go to the charging station ?","lowBattery_title":"Low Battery","lowBattery_message":"Go to the charging station ?","errorlaunchAlert_title":"A mistake has occured","errorlaunchAlert_message":"Please try again to start the round","robotmuststayondocking_title":"Low Battery","robotmuststayondocking_message":"The robot must stay on the docking","errorNavAlert_title":"A mistake has occured","errorNavAlert_message":"Please call technical support if the problem persists","lostAlert_title":"Robot lost !","lostAlert_message":"Please call technical support","quitConfirm_title":"Leave the application ?","errorBlocked_title":"Robot blocked","errorBlocked_message":"Please check that the robot is clear","statusRedPresent_title":"Error","statusRedPresent_message":"Please restart me OR call my manufacturer if the problem persists","statusGreenPresent_title":"Green status","statusGreenPresent_message":"The robot is healthy !","leaveDockingConfirm_title":"Battery: ","leaveDockingConfirm_message":"Get out of the charging station ?","leaveDockingAlert_title":"Robot in charge","leaveDockingAlert_message":"Please, remove me from the charging station","askHelp_title":"Help requested","askHelp_message":"Someone is going to come and help you","startblockedAlert_title":"Obstacle detected ahead","startblockedAlert_message":"Please back up the robot before restarting the round.","freq":"Frequency","freqAnnounce":"Announcement frequency","every30sec":"Every 30 seconds","every1mn":"Every minute","every2mn":"Every 2 min","every3mn":"Every 3 min","every4mn":"Every 4 min","every5mn":"Every 5 min","weather":"Weather","date":"Date","newAnnouncement":"New announcement","addLoading":"Adding ...","addDone":"Announcement added !","selectRound":"Select a round","roundDuration":"Round duration","rounds":"Rounds","announce1":"Hello ! Today is ","announce2":" and it's ","announcement":"Announcements","announce3":"Hello, here is the weather forecast for today : ","announce4":" with a temperature of ","announce5":"degrees","relocAlert_title":"Robot Lost","relocAlert_message":"Please relocate the robot","password":"Password","enterPassword":"Required to access this page.","wrongPassword":"Wrong password. Please try again.","autolaunch":"Auto launch of the mission : ","URL_tourneeapp":"assets/pdf/tuto_EN.pdf","rgpd_txt":"In accordance with the amended law of 6 January 1978 and the «General Data Protection Regulations», your personal contact data are collected so that you can receive alerts from the Kompai robot (person detected, robot blocked, low battery, etc.). Your data will be processed by Korian and Kompai for the time required to use the platform. You can, in particular, oppose the processing of your data, obtain a copy of your data, rectify or delete them by writing to the DPO and justifying your identity at: rgpd@kompai.com or KOMPAI Robotics, Technopole d 'Izarbel - 97 allée Théodore Monod 64210 Bidart - France.","autolaunchHeadPage":"AUTO LAUNCH","createMission":"NEW EVENT","map":"Map","roundSelection":"Round","duration":"Duration","type":"Type","adhoc":"ad hoc","daily":"daily","weekly":"weekly","create":"CREATE","today":"Today","all":"All","missionAdded":"Mission scheduled","dailyLetter":"d","weeklyLetter":"w","mapSelect":"Select a map","durationSelect":"Select a duration","roundSelect":"Select a round","typeSelect":"Select a type","pleasemask":"Please wear a mask !","speechmask":"Hello, please put on a mask!","robotBehaviour":"Robot's behaviour","fallDetectionActivation":"Enable fall detection","maskDetectionActivation":"Enable mask detection","radio":"Radio","newradio":"New radio station","radioRemove":"Successful deletion","radioexist":"This radio already exists!","radioadd":"Added Radio","radioincorrect":"Invalid radio!","radioHeadPage":"Radio editor","NoRadio":"No radio","mp3":"MP3","folder":"Folder","music":"Music","NoMusic":"No music"}

/***/ }),

/***/ 316:
/***/ (function(module, exports) {

module.exports = {"quit":"SALIR","battery":"BATERÍA","return":"REGRESAR","tutorial":"TUTORIAL","param":"PARÁMETROS","announceEdit":"EDITOR DE ANUNCIOS","receivesms":"RECIBIR ALERTAS POR SMS","receivemail":"RECIBIR ALERTAS POR CORREO ELECTRÓNICO","changelangage":"CAMBIAR EL IDIOMA","godocking":"EN CAMINO A LA ESTACÍON DE CARGA","round":"RECORRIDO DE ENTRETENIMIENTO","patrol":"Patrulla","patrolInProgress":"PATRULLANDO","roundInProgress":"RECORRIDO DE ENTRETENIMIENTO EN CURSO","moving":"EN MOVIMIENTO PARA :","charging_remaining":"ROBOT EN CARGA - BATERÍA: ","walkInProgress":"CAMINE A SU PROPIO RITMO SIN EMPUJAR","notpush":"COLÓQUESE COMO EN LA FOTO Y PRESIONE «EMPEZAR»","mails":"Correos electrónicos","sms":"SMS","langage":"Idioma","distance_covered":"Distancia recorrida","goto":"Ir a :","inProgress":"EN CURSO","post":"Posición","sentinel":"Centinela","morningRound":"Ronda de la mañana","eveningRound":"Ronda de la noche","btn_go":"INICIAR","btn_help":"Ayuda","btn_walk":"Empezar","btn_stop":"ALTO","btn_charge":"CARGAR","btn_cancel":"Cancelar","btn_ok":"OK","btn_yes":"SÍ","btn_no":"NO","numexist":"¡ Este número ya existe !","deletenum":"Por favor, borre un número primero","numadd":"Número añadido","numincorrect":"Número inválido","newnum":"Nuevo número","mailexist":"¡ Esta dirección de correo electrónico ya existe ! ","deletemail":"Por favor, elimine primero una dirección de correo electrónico","mailadd":"¡ Dirección de correo electrónico añadida !","mailincorrect":"Dirección de correo electrónico inválida","newmail":"Nueva dirección de correo electrónico","error":"Error","cantstart":"No se puede iniciar la aplicación. Compruebe si el robot está bien encendido.","nointernet":"Sin acceso a internet","btn_save":"Guardar","receivepic":"Recibir fotos","editpswd":"EDITAR CONTRASEÑA","currentpswd":"Contraseña actual :","newpswd":"Nueva contraseña :","save":"Guardar","success":"Está hecho !","pswdsaved":"Contraseña guardada","langage1":"Idioma","editpswd1":"Editar contraseña","wrongpass":"Contraseña incorrecta","smsSOS":"Alguien ha pulsado el botón SOS del robot","mailSOS_suj":"SOS","mailSOS_body":"<br> Una persona necesita ayuda","mailBattery_suj":"Batería Baja","mailBattery_body":"<br> Hay que cargar al robot. Por favor, pongalo en su estación de carga.","mailBlocked_suj":"Robot bloqueado","mailBlocked_body":"<br> El robot está bloqueado <br>","mailFall_body":"El robot detecta a una persona en el suelo <br>","mailFall_suj":"CAÍDA detectada","mailPerson_suj":"Persona detectada","mailPerson_body":"El robot detectó a una persona <br>","mailRemove":"Supresión realizada","presentAlert_title":"Robot en carga","AlertConnectedToI":"El robot está conectado a internet","presentAlert_message":"¡ Voy a ir hacia atrás !","presentConfirm_title":"Ronda suspendida","presentConfirm_message":"¿ Volver a la ronda ?","FallConfirm_title":"¡ Caída detectada !","FallConfirm_message":"El robot detectó a una caída","RemoteConfirm_title":"Ronda parada a distancia","RemoteConfirm_message":"¿ Volver a la ronda ?","blockedAlert_title":"¡ Robot bloqueado !","blockedAlert_message":"Por favor, quite el obstáculo o muevame","goDockingConfirm_title":"Batería: ","goDockingConfirm_message":"¿ Ir a la estación de carga ?","lowBattery_title":"Batería Baja","lowBattery_message":"¿ Ir a la estación de carga ?","errorlaunchAlert_title":"Se ha producido un error","errorlaunchAlert_message":"Intente volver a empezar la ronda, por favor","robotmuststayondocking_title":"Batería Baja","robotmuststayondocking_message":"El robot debe quedarse en el atraque","errorNavAlert_title":"Se ha producido un error","errorNavAlert_message":"Por favor, llame al soporte técnico si el problema persiste","lostAlert_title":"¡ Robot perdido !","lostAlert_message":"Por favor, llame al soporte técnico","quitConfirm_title":"¿ Salir de la aplicación ?","errorBlocked_title":"Robot bloqueado","errorBlocked_message":"Compruebe si el robot está desbloqueado","statusRedPresent_title":"Error","statusRedPresent_message":"Por favor, reinicie me o llame a mi fabricante si el problema persiste","statusGreenPresent_title":"Situación verde","statusGreenPresent_message":"¡ El robot está sano !","leaveDockingConfirm_title":"Batería: ","leaveDockingConfirm_message":"¿ Salir de la estación de carga ?","leaveDockingAlert_title":"Robot cargando","leaveDockingAlert_message":"Por favor, retireme de la estación de carga","askHelp_title":"Ayuda solicitada","askHelp_message":"Alguien vendrá a ayudarte","startblockedAlert_title":"Obstáculo detectado adelante","startblockedAlert_message":"Mueva el robot hacia atrás antes de reiniciar la ronda.","freq":"Frecuencia","freqAnnounce":"Frecuencia de los anuncios","every30sec":"Cada 30 segundos","every1mn":"Cada minuto","every2mn":"Cada 2 min","every3mn":"Cada 3 min","every4mn":"Cada 4 min","every5mn":"Cada 5 min","weather":"Tiempo","date":"Fecha","newAnnouncement":"Nuevo anuncio","addLoading":"Añadiendo ...","addDone":"Anuncio añadido !","selectRound":"Elegir una ronda","roundDuration":"Duración de la ronda","rounds":"Rondas","announce1":"Hola ! Hoy, es el ","announce2":" y son las ","announcement":"Anuncios","announce3":"Hola, este es la previsión meteo del día : ","announce4":" con una temperatura de ","announce5":"grados","relocAlert_title":"Robot perdido","relocAlert_message":"Reubique el robot","password":"Contraseña","enterPassword":"Requerido para acceder a está página.","wrongPassword":"Contraseña incorrecta. Por favor, intente de nuevo.","autolaunch":"Lanzamiento automático de la misión : ","URL_tourneeapp":"assets/pdf/tuto_EN.pdf","rgpd_txt":"De acuerdo con la ley modificada del 6 de enero de 1978 y el «Reglamento general de protección de datos», sus datos de contacto personales se recopilan para que pueda recibir alertas del robot Kompai (persona detectada, robot bloqueado, batería baja, etc.). Korian y Kompai procesarán sus datos durante el tiempo necesario para utilizar la plataforma. Puede, en particular, oponerse al tratamiento de sus datos, obtener una copia de sus datos, rectificarlos o suprimirlos escribiendo al DPO y justificando su identidad en: rgpd@kompai.com o KOMPAI Robotics, Technopole d 'Izarbel - 97 allée Théodore Monod 64210 Bidart - Francia.","autolaunchHeadPage":"INICIO AUTO","createMission":"NUEVA MISIÓN","map":"Mapa","roundSelection":"Ronda","duration":"Duración","type":"Tipo","adhoc":"puntual","daily":"diario","weekly":"semanal","create":"CREAR","today":"Hoy","all":"Todo","missionAdded":"Misión programada","dailyLetter":"d","weeklyLetter":"s","mapSelect":"Elegir una mapa","roundSelect":"Elegir una ronda","durationSelect":"Elegir una duración","typeSelect":"Elegir un tipo","pleasemask":"Por favor ponte una máscara !","speechmask":"Hola, por favor ponte una máscara.","robotBehaviour":"Comportamiento del robot","fallDetectionActivation":"Activar la detección de caída","maskDetectionActivation":"Activar detección de máscara","radio":"Radio","newradio":"Nueva radio","radioRemove":"Supresión realizada","radioexist":"¡Esta radio ya existe!","radioadd":"Radio añadida","radioincorrect":"¡Radio inválida!","radioHeadPage":"Editor de radio","NoRadio":"Sin radio","mp3":"MP3","folder":"Carpeta","music":"Música","NoMusic":"Sin musica"}

/***/ }),

/***/ 317:
/***/ (function(module, exports) {

module.exports = {"quit":"BEENDEN","battery":"BATTERIE","return":"ZURÜCK","tutorial":"ANLEITUNG","param":"EINSTELLUNGEN","announceEdit":"BEARBEITER DER ANSAGEN","receivesms":"BENACHRICHTIGUNG ALS SMS ERHALTEN","receivemail":"BENACHRICHTIGUNG ALS E-MAIL ERHALTEN","changelangage":"SPRACHE ÄNDERN","godocking":"AUF DEM WEG ZUR LADESTATION","round":"ENTERTAINMENT RUNDE","patrol":"Kontrollfahrt","patrolInProgress":"KONTROLLFAHRT WIRD DURCHGEFÜHRT","roundInProgress":"ENTERTAINMENT RUNDE WIRD DURCHGEFÜHRT","moving":"AUF DEM WEG ZU :","charging_remaining":"ROBOTER LÄDT AUF - BATTERIE: ","walkInProgress":"GEHE IN DEINEM EIGENEN TEMPO OHNE ANSCHIEBEN","notpush":"STELLE DICH AUF, WIE AUF DEM FOTO ZU SEHEN, UND DRÜCKE «LOS GEHT'S","mails":"E-Mails","sms":"SMS","langage":"Sprache","distance_covered":"Zurückgelegte Strecke","goto":"Gehe zu:","inProgress":"WIRD DURCHGEFÜHRT","post":"Position","sentinel":"Überwachung","morningRound":"Morgen-Runde","eveningRound":"Abend-Runde","btn_go":"LOS","btn_help":"Hilfe","btn_walk":"Los geht's!","btn_stop":"STOP","btn_charge":"LADEN","btn_cancel":"Abbrechen","btn_ok":"OK","btn_yes":"JA","btn_no":"NEIN","numexist":"Diese Telefonnummer existiert bereits!","deletenum":"Bitte lösche zuerst eine Telefonnummer","numadd":"Telefonnummer hinzugefügt","numincorrect":"Ungültige Telefonnummer","newnum":"Neue Telefonnummer","mailexist":"Diese Adresse  existiert bereits !","deletemail":"Bitte lösche zuerst eine Adresse","mailadd":"E-Mail Adresse hinzugefügt!","mailincorrect":"Ungültige E-Mail Adresse","newmail":"Neue Adresse","error":"Fehler","cantstart":"Start der Anwendung ist fehlgeschlagen. Prüfe, ob der Roboter korrekt eingeschaltet ist.","nointernet":"Keine Internetverbindung","btn_save":"Sichern","receivepic":"Fotos erhalten","editpswd":"PASSWORT BEARBEITEN","currentpswd":"Aktuelles Passwort :","newpswd":"Neues Kennwort :","save":"Speichern","success":"Erfolg","pswdsaved":"Passwort gespeichert","langage1":"Sprache","editpswd1":"Passwort bearbeiten","wrongpass":"Falsches Passwort","smsSOS":"Der SOS Knopf wurde gedrückt","mailSOS_suj":"SOS","mailSOS_body":"<br> Eine Person benötigt Hilfe","mailBattery_suj":"Batterie schwach","mailBattery_body":"<br> Der Roboter muss aufgeladen werden. Bitte stelle ihn auf die Ladestation.","mailBlocked_suj":"Roboter ist blockiert","mailBlocked_body":"<br> Der Roboter ist blockiert <br>","mailFall_body":"Der Roboter erkennt eine Person <br>","mailFall_suj":"STURZ erkannt","mailPerson_suj":"Person erkannt","mailPerson_body":"Der Roboter hat eine Person erkannt <br>","mailRemove":"Löschen erledigt","presentAlert_title":"Roboter lädt auf","AlertConnectedToI":"Der Roboter ist mit dem Internet verbunden","presentAlert_message":"Ich gehe zurück !","presentConfirm_title":"Runde unterbrochen","presentConfirm_message":"Runde fortführen?","FallConfirm_title":"STURZ erkannt !","FallConfirm_message":"Der Roboter hat einen Sturz erkannt","RemoteConfirm_title":"Ferngesteuert Runde stoppen","RemoteConfirm_message":"Runde fortführen?","blockedAlert_title":"Roboter ist blockiert !","blockedAlert_message":"Bitte entferne das Hindernis oder bewege mich","goDockingConfirm_title":"Batterie: ","goDockingConfirm_message":"Zur Ladestation gehen ?","lowBattery_title":"Batterie schwach","lowBattery_message":"Zur Ladestation gehen ?","errorlaunchAlert_title":"Ein Fehler ist aufgetreten","errorlaunchAlert_message":"Bitte versuche erneut die Runde zu starten","robotmuststayondocking_title":"Batterie schwach","robotmuststayondocking_message":"Der Roboter muss auf der Ladestation bleiben","errorNavAlert_title":"Ein Fehler ist aufgetreten","errorNavAlert_message":"Bitte kontaktiere den technischen Support wenn das Problem weiterhni besteht","lostAlert_title":"Roboter hat sich verirrt !","lostAlert_message":"Bitte kontaktiere den technischen Support","quitConfirm_title":"Die Anwendung beenden ?","errorBlocked_title":"Roboter ist blockiert","errorBlocked_message":"Bitte prüfe, ob der Roboter frei ist","statusRedPresent_title":"Fehler","statusRedPresent_message":"Bitte starte mich neu ODER kontaktiere meinen Hersteller wenn das Problem weiterhin besteht","statusGreenPresent_title":"Grüner Status","statusGreenPresent_message":"Der Roboter ist in Ordnung !","leaveDockingConfirm_title":"Batterie: ","leaveDockingConfirm_message":"Die Ladestation verlassen ?","leaveDockingAlert_title":"Roboter lädt auf","leaveDockingAlert_message":"Bitte entferne mich von der Ladestation","askHelp_title":"Hilfe ist angefordert","askHelp_message":"Jemand kommt um zu helfen","startblockedAlert_title":"Hindernis voraus erkannt","startblockedAlert_message":"Bitte sichern Sie den Roboter, bevor Sie die Runde neu starten.","freq":"Häufigkeit","freqAnnounce":"Häufigkeit der Ansagen","every30sec":"Alle 30 Sekunden","every1mn":"Jede Minute","every2mn":"Alle 2 min","every3mn":"Alle 3 min","every4mn":"Alle 4 min","every5mn":"Alle 5 min","weather":"Wetter","date":"Datum","newAnnouncement":"Neue Ansage","addLoading":"Hinzufügen ...","addDone":"Ansage hinzugefügt !","selectRound":"Runde auswählen","roundDuration":"Dauer der Runde","rounds":"Runden","announce1":"Hallo ! Heute ist ","announce2":" und es ist ","announcement":"Ansagen","announce3":"Hallo, die Wettervorhersage für heute ist: ","announce4":" wir haben eine Temeperatur von ","announce5":"Grad","relocAlert_title":"Verlorener Roboter","relocAlert_message":"Bitte versetzen Sie den Roboter","password":"Passwort","enterPassword":"Erforderlich für den Zugriff auf die Einstellungen.","wrongPassword":"Falsches Passwort. Bitte versuche es erneut.","autolaunch":"Automatischer Start der Mission : ","URL_tourneeapp":"assets/pdf/tuto_EN.pdf","rgpd_txt":"In Übereinstimmung mit dem geänderten Gesetz vom 6. Januar 1978 und der « Allgemeinen Datenschutzverordnung » werden Ihre persönlichen Kontaktdaten gesammelt, damit Sie Benachrichtigungen vom Kompai-Roboter erhalten können (Person erkannt, Roboter blockiert, schwache Batterie usw.). Ihre Daten werden von Korian und Kompai für den Zeitraum verarbeitet, der für die Nutzung der Plattform erforderlich ist. Sie können sich insbesondere der Verarbeitung Ihrer Daten widersetzen, eine Kopie Ihrer Daten erhalten, diese berichtigen oder löschen lassen, indem Sie sich schriftlich und unter Angabe Ihrer Identität an den Datenschutzbeauftragten wenden: rgpd@kompai.com or KOMPAI Robotics, Technopole d 'Izarbel - 97 allée Théodore Monod 64210 Bidart - France.","autolaunchHeadPage":"AUTOMATISCHER START","createMission":"NEUE VERANSTALTUNG","map":"Karte","roundSelection":"Rund","duration":"Dauer","type":"Typ","adhoc":"ad hoc","daily":"täglich","weekly":"wöchentlich","create":"ERSTELLEN","today":"Heute","all":"Alle","missionAdded":"Geplante Mission","dailyLetter":"t","weeklyLetter":"w","mapSelect":"Wählen Sie eine Karte","roundSelect":"Wählen Sie eine Runde","durationSelect":"Wählen Sie eine Dauer","typeSelect":"Wählen Sie eine Typ","pleasemask":"Bitte tragen Sie eine Maske !","speechmask":"Hallo, bitte setzen Sie eine Maske auf!","robotBehaviour":"Das Verhalten des Roboters","fallDetectionActivation":"Aktivieren der Sturzerkennung","maskDetectionActivation":"Aktivieren Sie die Maskenerkennung","radio":"Radio","newradio":"Neuer Radiosender","radioRemove":"Erfolgreiche Löschung","radioexist":"Dieses Radio existiert bereits!","radioadd":"Radio hinzugefügt","radioincorrect":"Radio ungültig!","radioHeadPage":"Radioredakteur","NoRadio":"Kein Radio","mp3":"MP3","folder":"Ordner","music":"Musik","NoMusic":"keine Musik"}

/***/ }),

/***/ 318:
/***/ (function(module, exports) {

module.exports = {"quit":"ΕΞΟΔΟΣ","battery":"ΜΠΑΤΑΡΙΑ","return":"ΕΠΙΣΤΡΟΦΗ","tutorial":"ΑΡΧΙΚΗ ΕΠΕΞΗΓΗΣΗ","param":"ΡΥΘΜΙΣΕΙΣ","announceEdit":"ΕΠΕΞΕΡΓΑΣΙΑ ΑΝΑΚΟΙΝΩΣΗΣ","receivesms":"ΛΗΨΗ ΕΙΔΟΠΟΙΗΣΗΣ ΜΕ SMS","receivemail":"ΛΗΨΗ ΕΙΔΟΠΟΙΗΣΗΣ ΜΕ E-MAIL","changelangage":"ΑΛΛΑΓΗ ΓΛΩΣΣΑΣ","godocking":"ΚΑΘ΄ΟΔΟΝ ΓΙΑ ΤΟ ΣΤΑΘΜΟ ΦΟΡΤΙΣΗΣ","round":"ΨΥΧΑΓΩΓΙΚΗ ΠΕΡΙΗΓΗΣΗ","patrol":"ΠΕΡΙΠΟΛΙΑ","patrolInProgress":"ΣΕ ΠΕΡΙΠΟΛΙΑ","roundInProgress":"ΠΕΡΙΗΓΗΣΗ ΨΥΧΑΓΩΓΙΑΣ ΣΕ ΕΞΕΛΙΞΗ","moving":"ΣΕ ΕΞΕΛΙΞΗ ΜΕΤΑΦΟΡΑ ΣΕ :","charging_remaining":"ΡΟΜΠΟΤ ΣΕ ΦΟΡΤΙΣΗ - ΜΠΑΤΑΡΙΑ: ","walkInProgress":"ΠΕΡΠΑΤΗΣΤΕ ΜΕ ΤΟΝ ΡΥΘΜΟ ΣΑΣ ΧΩΡΙΣ ΝΑ ΣΠΡΩΧΝΕΣΤΕ","notpush":"ΤΟΠΟΘΕΤΗΣΤΕ ΤΟΝ ΕΑΥΤΟ ΣΑΣ ΟΠΩΣ ΣΤΗ ΦΩΤΟΓΡΑΦΙΑ ΚΑΙ ΠΑΤΗΣΤΕ 'ΠΑΜΕ'","mails":"E-MAILS","sms":"SMS","langage":"Γλώσσα","distance_covered":"Απόσταση που διανύθηκε","goto":"Πήγαινε σε :","inProgress":"ΣΕ ΕΞΕΛΙΞΗ","post":"Θέση","sentinel":"Φρουρός","morningRound":"Πρωινή Περιήγηση","eveningRound":"Απογευματινή Περιήγηση","btn_go":"ΠΑΜΕ","btn_help":"Βοήθεια","btn_walk":"Πάμε!","btn_stop":"ΣΤΑΜΑΤΑ","btn_charge":"ΦΟΡΤΙΣΗ","btn_cancel":"Ακύρωση","btn_ok":"ΟΚ","btn_yes":"ΝΑΙ","btn_no":"ΟΧΙ","numexist":"Αυτός ο αριθμός τηλεφώνου υπάρχει ήδη !","deletenum":"Παρακαλώ διαγράψτε έναν αριθμό τηλεφώνου πρώτα","numadd":"Ο αριθμός τηλεφώνου προστέθηκε","numincorrect":"Μη έγκυρος αριθμός τηλεφώνου","newnum":"Νέος αριθμός τηλεφώνου","mailexist":"Αυτή η διεύθυνση υπάρχει ήδη ! ","deletemail":"Παρακαλώ διαγράψτε μία διεύθυνση πρώτα","mailadd":"Η διεύθυνση ηλεκτρονικού ταχυδρομείου προστέθηκε!","mailincorrect":"Μη έγκυρη διεύθυνση ηλεκτρονικού ταχυδρομείου","newmail":"Νέα διεύθυνση","error":"Σφάλμα","cantstart":"Η έναρξη της εφαρμογής απέτυχε. Ελέγξτε ότι το ρομπότ λειτουργεί κανονικά.","nointernet":"Μη διαθέσιμη σύνδεση στο ίντερνετ","btn_save":"Αποθήκευση","receivepic":"Λήψη φωτογραφιών","editpswd":"ΕΠΕΞΕΡΓΑΣΙΑ ΚΩΔΙΚΟΥ","currentpswd":"Τρέχων κωδικός :","newpswd":"Νέος κωδικός :","save":"Αποθήκευση","success":"Επιτυχία","pswdsaved":"Ο κωδικός αποθηκεύτηκε","langage1":"Γλώσσα","editpswd1":"Επεξεργασία Κωδικού","wrongpass":"Λάθος Κωδικός","smsSOS":"Κάποιος πάτησε το κουμπί έκτακτης ανάγκης του ρομπότ","mailSOS_suj":"SOS","mailSOS_body":"<br> Κάποιο άτομο χρειάζεται βοήθεια","mailBattery_suj":"Χαμηλή Μπαταρία","mailBattery_body":"<br> Το ρομπότ χρειάζεται φόρτιση. Παρακαλώ τοποθετήστε το στον σταθμό φόρτισης.","mailBlocked_suj":"Το ρομπότ εμποδίζεται","mailBlocked_body":"<br> Το ρομπότ εμποδίζεται <br>","mailFall_body":"Το ρομπότ εντόπισε άτομο <br>","mailFall_suj":"Ανιχνεύθηκε ΠΤΩΣΗ","mailPerson_suj":"Άτομο εντοπίστηκε","mailPerson_body":"Το ρομπότ εντόπισε άτομο<br>","mailRemove":"Διαγραφή ολοκληρώθηκε","presentAlert_title":"Το ρομπότ βρίσκεται σε φόρτιση","AlertConnectedToI":"Το ρομπότ είναι συνδεδεμένο στο διαδίκτυο","presentAlert_message":"Προσοχή, θα κάνω όπισθεν !","presentConfirm_title":"Ο γύρος αναβλήθηκε","presentConfirm_message":"Συνέχεια του γύρου?","FallConfirm_title":"Ανιχνεύθηκε πτώση ! ","FallConfirm_message":"Το ρομπότ εντόπισε πτώση","RemoteConfirm_title":"Διακοπή γύρου από απόσταση","RemoteConfirm_message":"Συνέχεια του γύρου ?","blockedAlert_title":"Το ρομπότ εμποδίζεται !","blockedAlert_message":"Παρακαλώ αφαιρέστε το εμπόδιο ή μεταφέρετέ με","goDockingConfirm_title":"Μπαταρία: ","goDockingConfirm_message":"Μεταφορά στο σταθμό φόρτισης ?","lowBattery_title":"Χαμηλή Μπαταρία","lowBattery_message":"Μεταφορά στο σταθμό φόρτισης ?","errorlaunchAlert_title":"Παρουσιάστηκε σφάλμα","errorlaunchAlert_message":"Παρακαλώ δοκιμάστε ξανά να ξεκινήσω τον γύρο","robotmuststayondocking_title":"Χαμηλή Μπαταρία","robotmuststayondocking_message":"Το ρομπότ πρέπει να παραμείνει συνδεδεμένο","errorNavAlert_title":"Παρουσιάστηκε σφάλμα","errorNavAlert_message":"Παρακαλώ επικοινωνήστε με την τεχνική υποστήριξη αν το σφάλμα παραμένει","lostAlert_title":"Το ρομπότ χάθηκε !","lostAlert_message":"Παρακαλώ επικοινωνήστε με την τεχνική υποστήριξη","quitConfirm_title":"Έξοδος από την εφαρμογή ?","errorBlocked_title":"Το ρομπότ εμποδίζεται","errorBlocked_message":"Παρακαλώ ελέξτε ότι το ρομπότ είναι καθαρό","statusRedPresent_title":"Σφάλμα","statusRedPresent_message":"Παρακαλώ κάντε μου επανεκκίνηση Ή καλέστε τον κατασκευαστή αν το σφάλμα παραμένει","statusGreenPresent_title":"Πράσινη κατάσταση","statusGreenPresent_message":"Το ρομπότ είναι υγιές !","leaveDockingConfirm_title":"Μπαταρία: ","leaveDockingConfirm_message":"Έξοδος από το σταθμό φόρτισης ?","leaveDockingAlert_title":"Το ρομπότ βρίσκεται σε φόρτιση","leaveDockingAlert_message":"Παρακαλώ, αφαιρέστε με από τον σταθμό φόρτισης","askHelp_title":"Ζητείται βοήθεια","askHelp_message":"Κάποιος θα έρθει να σας βοηθήσει","startblockedAlert_title":"Εντοπίστηκε εμπόδιο μπροστά","startblockedAlert_message":"Δημιουργήστε αντίγραφα ασφαλείας του ρομπότ πριν ξεκινήσετε ξανά τον γύρο","freq":"Συχνότητα","freqAnnounce":"Συχνότητα Ανακοίνωσης","every30sec":"Κάθε 30 δευτερόλεπτα","every1mn":"Κάθε λεπτό","every2mn":"Κάθε 2 λεπτά","every3mn":"Κάθε 3 λεπτά","every4mn":"Κάθε 4 λεπτά","every5mn":"Κάθε 5 λεπτά","weather":"Καιρός","date":"Ημερομηνία","newAnnouncement":"Νέα Ανακοίνωση","addLoading":"Προστίθεται ... ","addDone":"Προστέθηκε Ανακοίνωση !","selectRound":"Επιλέξτε γύρο","roundDuration":"Διάρκεια γύρου","rounds":"Γύροι","announce1":"Γεια σας ! Σήμερα έχουμε ","announce2":" και είναι ","announcement":"Ανακοινώσεις","announce3":"Γεια σας, ακολουθεί η πρόγνωση καιρού για σήμερα : ","announce4":" με θερμοκρασία ","announce5":"βαθμούς","relocAlert_title":"Το ρομπότ χάθηκε","relocAlert_message":"Παρακαλώ επανατοποθετήστε το ρομπότ","password":"Κωδικός","enterPassword":"Απατείται για πρόσβαση στη σελίδα","wrongPassword":"Λάθος κωδικός. Παρακαλώ προσπαθήστε ξανά.","autolaunch":"Αυτόματη εκκίνηση της αποστολής : ","URL_tourneeapp":"assets/pdf/tuto_EN.pdf","rgpd_txt":"Σύμφωνα με τον τροποποιημένο νόμο της 6ης Ιανουαρίου 1978 και τους Γενικούς Κανονισμούς Προστασίας Δεδομένων, τα προσωπικά δεδομένα επικοινωνίας σας θα συλλέγονται ώστε να μπορείτε να λαμβάνετε ειδοποιήσεις από το ρομπότ Kompai (εντοπισμός ατόμου, παρεμπόδιση ρομπότ, χαμηλή μπαταρία κλπ). Τα δεδομένα σας θα επεξεργαστούν από την Korian και την Kompai για το χρονικό διάστημα που απαιτείται για τη χρήση της πλατφόρμας. Μπορείτε, ειδικότερα, να αντιταχθείτε στην επεξεργασία των δεδομένων σας, να λάβετε αντίγραφο των δεδομένων σας, να τα διορθώσετε ή να τα διαγράψετε, απευθυνόμενοι γραπτώς στο αντίστοιχο τμήμα και αιτιολογώντας την ταυτότητά σας στη διεύθυνση: rgpd@kompai.com ή KOMPAI Robotics, Technopole d' Izarbel - 97 allée Théodore Monod 64210 Bidart - Γαλλία.","autolaunchHeadPage":"ΑΥΤΟΜΑΤΗ ΕΚΚΙΝΗΣΗ","createMission":"ΝΕΟ ΣΥΜΒΑΝ","map":"Χάρτης","roundSelection":"Γύρος","duration":"Διάρκεια","type":"Τύπος","adhoc":"Στιγμιαία","daily":"Ημερήσια","weekly":"Εβδομαδιαία","create":"ΔΗΜΙΟΥΡΓΙΑ","today":"Σήμερα","all":"ΟΛΑ","missionAdded":"Η αποστολή προγραμματίστηκε","dailyLetter":"η","weeklyLetter":"ε","mapSelect":"Διαλέξτε ένα χάρτη","durationSelect":"Διαλέξτε διάρκεια","roundSelect":"Διαλέξτε γύρο","typeSelect":"Διαλέξτε τύπο","pleasemask":"Παρακαλώ φορέστε μάσκα !","speechmask":"Γεια σας, παρακαλώ φορέστε τη μάσκα σας!","robotBehaviour":"Συμπεριφορά του ρομπότ","fallDetectionActivation":"Ενεργοποίηση ανίχνευσης πτώσεων","maskDetectionActivation":"Ενεργοποίηση ανίχνευσης μάσκας","radio":"Ραδιόφωνο","newradio":"Νέο ραδιόφωνο","radioRemove":"Επιτυχής διαγραφή","radioexist":"Αυτό το ραδιόφωνο υπάρχει ήδη!","radioadd":"Προστέθηκε ραδιόφωνο","radioincorrect":"Άκυρο ραδιόφωνο!","radioHeadPage":"Ραδιοφωνικός συντάκτης","NoRadio":"όχι ραδιόφωνο","mp3":"MP3","folder":"υπόθεση","music":"ΜΟΥΣΙΚΗ","NoMusic":"όχι μουσική"}

/***/ }),

/***/ 319:
/***/ (function(module, exports) {

module.exports = {"quit":"ESCI","battery":"BATTERIA","return":"INDIETRO","tutorial":"TUTORIAL","param":"IMPOSTAZIONI","announceEdit":"EDITOR DEGLI ANNUNCI","receivesms":"RICEVI ALLERTE VIA SMS","receivemail":"RICEVI ALLERTE VIA EMAIL","changelangage":"CAMBIA LA LINGUA","godocking":"VERSO LA STAZIONE DI RICARICA","round":"TOUR DI INTRATTENIMENTO","patrol":"Ricognizione","patrolInProgress":"IN RICOGNIZIONE","roundInProgress":"TOUR DI INTRATTENIMENTO IN CORSO","moving":"SPOSTAMENTO VERSO :","charging_remaining":"ROBOT IN CARICA - BATTERIA: ","walkInProgress":"CAMMINA AL TUO RITMO SENZA ACCELERARE","notpush":"METTITI COME NELLA FOTO E PRIMA 'VIA'","mails":"Emails","sms":"SMS","langage":"Lingua","distance_covered":"Distanza percorsa","goto":"Vai a :","inProgress":"IN CORSO","post":"Posizione","sentinel":"Sentinella","morningRound":"Tour mattutino","eveningRound":"Tour serale","btn_go":"VAI","btn_help":"AIUTO","btn_walk":"Andiamo!","btn_stop":"STOP","btn_charge":"CARICAMENTO","btn_cancel":"Annulla","btn_ok":"OK","btn_yes":"YES","btn_no":"NO","numexist":"Questo numero di telefono è già esistente!","deletenum":"Perfavore cancella prima un numero di telefono","numadd":"Numero di telefono salvato","numincorrect":"Numero di telefono non valido","newnum":"Nuovo numero di telefono","mailexist":"Indirizzo già esistente! ","deletemail":"Perfavore cancella prima un indirizzo","mailadd":"Indirizzo e-mail aggiunto!","mailincorrect":"Indirizzo e-mail non valido","newmail":"Nuovo indirizzo","error":"Error","cantstart":"Avvio dell'applicazione fallito. Controlla che il robot sia acceso...","nointernet":"Nessun accesso a internet","btn_save":"Salva","receivepic":"Ricevi immagini","editpswd":"CAMBIA PASSWORD","currentpswd":"Password attuale :","newpswd":"Nuova password :","save":"Salva","success":"Successo","pswdsaved":"Password salvata","langage1":"Lingua","editpswd1":"Modifica password","wrongpass":"Password errata","smsSOS":"Qualcuno ha premuto il bottone di SOS sul robot","mailSOS_suj":"SOS","mailSOS_body":"<br> Una persona ha bisogno di assistenza","mailBattery_suj":"Batteria scarica","mailBattery_body":"<br> Il robot ha bisogno di essere ricaricato. Collegarlo alla stazione di ricarica.","mailBlocked_suj":"Robot bloccato","mailBlocked_body":"<br> Il robot è bloccato <br>","mailFall_body":"Il robot ha rilevato una persona a terra <br>","mailFall_suj":"Caduta rilevata","mailPerson_suj":"Persona rilevata","mailPerson_body":"Persona rilevata <br>","mailRemove":"Cancellazione completata","presentAlert_title":"Robot in carica","AlertConnectedToI":"Il robot è connesso a internet","presentAlert_message":"Attenzione, sto andando indietro!","presentConfirm_title":"Giro sospeso","presentConfirm_message":"Continuare il giro ?","FallConfirm_title":"Caduta rilevata !","FallConfirm_message":"Il robot ha rilevato una caduta","patrolling":"Ricognizione","RemoteConfirm_title":"Giro fermato da remoto","RemoteConfirm_message":"Continuare il giro ?","blockedAlert_title":"Robot bloccato !","blockedAlert_message":"Perfavore rimuovere l'ostacolo oppure spostami","goDockingConfirm_title":"Batteria: ","goDockingConfirm_message":"Andare alla stazione di ricarica ?","lowBattery_title":"Batteria scarica","lowBattery_message":"Andare alla stazione di ricarica ?","errorlaunchAlert_title":"C'è stato un errore","errorlaunchAlert_message":"Prova a riavviare il giro","robotmuststayondocking_title":"Batteria scarica","robotmuststayondocking_message":"Il robot deve essere connesso alla docking","errorNavAlert_title":"C'è stato un errore","errorNavAlert_message":"Contattare il supporto tecnico se il problema persiste","lostAlert_title":"Robot perso !","lostAlert_message":"Contattare il supporto tecnico","quitConfirm_title":"Chiudere l'applicazione ?","errorBlocked_title":"Robot bloccato","errorBlocked_message":"Controllare che il robot sia pulito","statusRedPresent_title":"Errore","statusRedPresent_message":"Riavviami o chiama il produttore se il problema persiste","statusGreenPresent_title":"Verde","statusGreenPresent_message":"The robot è in salute !","leaveDockingConfirm_title":"Batteria: ","leaveDockingConfirm_message":"Allontanarsi dalla stazione di ricarica ?","leaveDockingAlert_title":"Robot in carica","leaveDockingAlert_message":"Perfavore, rimuovimi dalla stazione di ricarica","askHelp_title":"Richiesta di assistenza","askHelp_message":"Qualcuno arriverà ad aiutarti","startblockedAlert_title":"Ostacolo rilevato difronte","startblockedAlert_message":"Perfavore sposta il robot o rimuovi l'ostacolo prima di riavviare il giro.","freq":"Frequenza","freqAnnounce":"Frequenza degli annunci","every30sec":"Ogni 30 secondi","every1mn":"Ogni minuto","every2mn":"Ogni 2 min","every3mn":"Ogni 3 min","every4mn":"Ogni 4 min","every5mn":"Ogni 5 min","weather":"Tempo","date":"Data","newAnnouncement":"Nuovo annuncio","addLoading":"Caricamento ...","addDone":"Annuncio aggiunto !","selectRound":"Seleziona un giro","roundDuration":"Durata giro","rounds":"Percorsi","announce1":"Ciao ! Oggi è ","announce2":" ed è ","announcement":"Annunci","announce3":"Ciao, queste sono le previsioni del tempo per oggi : ","announce4":" con una temperatura di ","announce5":"gradi","relocAlert_title":"Robot perso","relocAlert_message":"Perfavore rilocalizza il robot","password":"Password","enterPassword":"Richiesta per accedere a questa pagina.","wrongPassword":"Password errata. Perfavore riprova.","autolaunch":"Auto avvio della missione : ","URL_tourneeapp":"assets/pdf/tuto_EN.pdf","rgpd_txt":"In conformità al «Regolamento Generale per la Protezione delle Informazioni», i tuoi contatti personali vengono memorizzati affinché tu riceva dei messaggi di allerta da Kompai (persona rilevata, robot bloccato, batteria scarica, etc.). I tuoi dati verranno processari da Korian e da Kompai per il tempo necessario all'utilizzo della piattaforma. Tu puoi, in particolare, opporti al trattamento dei dati, ottenere una copia delle tue informazioni, rettificarle o cancellarle scrivendo al DPO e dimostrando la tua identità a: rgpd@kompai.com or KOMPAI Robotics, Technopole d 'Izarbel - 97 allée Théodore Monod 64210 Bidart - France.","autolaunchHeadPage":"Auto AVVIO","createMission":"NUOVO EVENTO","map":"Mappa","roundSelection":"Percorso","duration":"Durata","type":"Tipo","adhoc":"ad hoc","daily":"Giornaliero","weekly":"Settimanale","create":"CREA","today":"Oggi","all":"Tutto","missionAdded":"Missione pianificata","dailyLetter":"d","weeklyLetter":"w","mapSelect":"Seleziona mappa","roundSelect":"Seleziona percorso","durationSelect":"Seleziona durata","typeSelect":"Seleziona tipo","pleasemask":"Perfavore indossa la mascherina !","speechmask":"Ciao, perfavore indossa la mascherina!","robotBehaviour":"Comportamento del robot","fallDetectionActivation":"Abilita la rilevazione delle cadute","maskDetectionActivation":"Abilita la rilevazione della mascherina","radio":"Radio","newradio":"Nuova radio","radioRemove":"Eliminazione riuscita","radioexist":"Questa radio esiste già!","radioadd":"Radio aggiunta","radioincorrect":"Radio non valida!","radioHeadPage":"Editore radiofonico","NoRadio":"nessuna radio","mp3":"MP3","folder":"Cartella","music":"Musica","NoMusic":"Nessuna musica"}

/***/ }),

/***/ 328:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 330:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 35:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AlertService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__param_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_common_http__ = __webpack_require__(50);
// to send mail and sms alert
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// function send mail in index.html
//declare var envoyerSMS;
// var in index.html
//declare var messageAenvoyer;
//declare var numTelToElement;
var AlertService = /** @class */ (function () {
    function AlertService(app, param, http) {
        this.app = app;
        this.param = param;
        this.http = http;
        this.videoWidth = 0;
        this.videoHeight = 0;
        this.constraints = {
            //audio: false,
            //video: {deviceId: "f9860b260e18d7fd1687d8567e21b49c4bb26f87606020741717e11f6179f409"},
            video: true,
            facingMode: "environment",
            width: { ideal: 1920 },
            height: { ideal: 1080 }
        };
        this.detect = false;
    }
    AlertService.prototype.ngOnInit = function () {
    };
    AlertService.prototype.checkwifi = function (bool) {
        this.allowmail = bool;
    };
    AlertService.prototype.handleError = function (error) {
        console.log('Error: ', error);
    };
    // function send mail without image
    // bod : corps du mail
    // suj : sujet du mail
    // dest : adresse mail du destinataire
    AlertService.prototype.sendmailPerso = function (bod, suj, dest, data, usern, passw, robotn) {
        var _this = this;
        //console.log("sendmail");
        var usermailData = {
            username: usern,
            email: data,
            bcc: dest,
            date: new Date(Date.now()).toDateString(),
            subject: suj,
            message: bod,
            password: passw,
            robotname: robotn
        };
        var link = "http://localhost/ionicDB/SendMail/sendmail.php";
        var postParams = JSON.stringify(usermailData);
        //console.log(postParams);
        if (this.allowmail) {
            this.http.post(link, postParams).subscribe(function (response) {
                //console.log(response);
                if (response == "OK") {
                    var s = _this.param.serialnumber + ' : Duration App';
                    if (suj == s) {
                        _this.param.updateDurationNS(_this.param.durationNS[0].id_duration);
                        _this.param.durationNS.shift();
                    }
                }
            }, function (error) {
                console.log(error);
            });
        }
    };
    AlertService.prototype.sendAlertData = function (bod, suj) {
        this.sendmailPerso(bod, suj, this.param.datamaillist, this.param.datamail, this.param.datamail, this.param.maildatapassw, this.param.datamail);
    };
    AlertService.prototype.sendAlertClient = function (bod, suj) {
        if (this.param.maillist.length) {
            this.sendmailPerso(bod, suj, this.param.maillist, this.param.datamail, this.param.robotmail, this.param.mailrobotpassw, this.param.name);
        }
    };
    // function send mail with image
    // bod : corps du mail
    // suj : sujet du mail
    // dest : adresse mail du destinataire
    // url : image attachment
    AlertService.prototype.sendmailImgperso = function (url, bod, suj, dest, data, usern, passw, robotn) {
        //console.log("sendmail");
        var usermailData = {
            username: usern,
            email: data,
            bcc: dest,
            date: new Date(Date.now()).toDateString(),
            subject: suj,
            message: bod,
            password: passw,
            robotname: robotn,
            url: url
        };
        var link = "http://localhost/ionicDB/SendMail/sendmailImg.php";
        var postParams = JSON.stringify(usermailData);
        console.log(postParams);
        if (this.allowmail) {
            this.http.post(link, postParams).subscribe(function (response) {
                console.log(response);
            });
        }
    };
    AlertService.prototype.sendAlertImgData = function (url, bod, suj) {
        this.sendmailImgperso(url, bod, suj, this.param.datamail, this.param.datamail, this.param.datamail, this.param.maildatapassw, this.param.datamail);
    };
    AlertService.prototype.sendAlertImgClient = function (url, bod, suj) {
        if (this.param.maillist.length) {
            this.sendmailImgperso(url, bod, suj, this.param.maillist, this.param.datamail, this.param.robotmail, this.param.mailrobotpassw, this.param.name);
        }
    };
    // function send sms
    AlertService.prototype.sendsmsperso = function (mess, num) {
        //messageAenvoyer = mess;
        //numTelToElement = num;
        //envoyerSMS();
    };
    AlertService.prototype.appError = function (info) {
        this.sendAlertData('<br> An error occurred, the round application has been reload automatically' +
            '<br> Code alert : 1' +
            info, this.param.serialnumber + ' : Error (App Round)');
    };
    AlertService.prototype.charging = function (info) {
        this.sendAlertData('<br> The robot has docked successfully' +
            '<br> Code alert : 2' +
            info, this.param.serialnumber + ' : Charging');
    };
    AlertService.prototype.appOpen = function (info) {
        this.sendAlertData('<br> Someone oppened the round app.' +
            '<br> Code alert : 24' +
            info, this.param.serialnumber + ' : App opened (Round)');
    };
    AlertService.prototype.lowBattery = function (info) {
        this.sendAlertData('<br> The robot must be sent to the docking station' +
            '<br> Code alert : 5' +
            info, this.param.serialnumber + ' : Battery Critical');
    };
    AlertService.prototype.remoteStop = function (info) {
        this.sendAlertData('<br> The round has been abort remotely' +
            '<br> Code alert : 14' +
            info, this.param.serialnumber + ' : Remote control');
    };
    AlertService.prototype.errorblocked = function (info) {
        this.sendAlertData('<br> The robot has been stopped because of a navigation error' +
            '<br> Code alert : 6' +
            info, this.param.serialnumber + ' : Navigation error');
    };
    AlertService.prototype.leaveDocking = function (info) {
        this.sendAlertData('<br> Kompai is no longer on docking' +
            '<br> Code alert : 11' +
            info, this.param.serialnumber + ' : Leave docking');
    };
    AlertService.prototype.roundCompleted = function (info) {
        this.sendAlertData('<br> The round is completed. The robot must move towards docking' +
            '<br> Code alert : 15' +
            info, this.param.serialnumber + ' : Round completed');
    };
    AlertService.prototype.displayTuto = function (info) {
        this.sendAlertData('<br> Someone is reading the tuto' +
            '<br> Code alert : 10' +
            info, this.param.serialnumber + ' : Tuto opened');
    };
    AlertService.prototype.noLongerBlocked = function (info) {
        this.sendAlertData('<br> The robot is no longer blocked' +
            '<br> Code alert : 3' +
            info, this.param.serialnumber + ' : Automatic release');
    };
    AlertService.prototype.manualintervention = function (info) {
        this.sendAlertData('<br> Someone unlocked the robot' +
            '<br> Code alert : 25' +
            info, this.param.serialnumber + ' : Manual release');
    };
    AlertService.prototype.naverror = function (info) {
        this.sendAlertData('<br> A navigation error has occurred. The robot did not manage to plan its route' +
            '<br> Code alert : 4' +
            info, this.param.serialnumber + ' : Navigation error');
    };
    AlertService.prototype.robotLost = function (info) {
        this.sendAlertData('<br> The robot is lost. It must be relocated' +
            '<br> Code alert : 7' +
            info, this.param.serialnumber + ' : Robot Lost');
    };
    AlertService.prototype.duration = function (olddata, info) {
        //console.log("duration alert");
        this.sendAlertData('<br> Here is a count of the duration of use of the apps in minutes' +
            '<br> Date duration : ' + olddata.date +
            '<br> Round duration : ' + olddata.round +
            '<br> Patrol duration : ' + olddata.patrol +
            '<br> Walk duration : ' + olddata.walk +
            '<br> Battery duration : ' + olddata.battery +
            '<br> Toolbox duration : ' + olddata.toolbox +
            '<br> Logistic duration : ' + olddata.logistic +
            '<br> Code alert : 26' +
            info, this.param.serialnumber + ' : Duration App');
    };
    AlertService.prototype.blockingdocking = function (info) {
        this.sendAlertData('<br> The robot encountered an obstacle while heading for the docking station' +
            '<br> Code alert : 8' +
            info, this.param.serialnumber + " : Can't reach docking");
    };
    AlertService.prototype.roundLaunched = function (info) {
        this.sendAlertData('<br> Somebody has started a round' +
            '<br> Code alert : 16' +
            info, this.param.serialnumber + ' : Round launched');
    };
    AlertService.prototype.roundFailed = function (info) {
        this.sendAlertData('<br> The round has not run correctly' +
            '<br> Code alert : 17' +
            info, this.param.serialnumber + ' : Round failed');
    };
    AlertService.prototype.roundPaused = function (info) {
        this.sendAlertData('<br> Somebody has paused the round in progress' +
            '<br> Code alert : 18' +
            info, this.param.serialnumber + ' : Round paused');
    };
    AlertService.prototype.askCharge = function (info) {
        this.sendAlertData('<br> The robot must move towards docking' +
            '<br> Code alert : 12' +
            info, this.param.serialnumber + ' : Charging request');
    };
    AlertService.prototype.appClosed = function (info) {
        this.sendAlertData('<br> Somebody has closed the round application' +
            '<br> Code alert : 13' +
            info, this.param.serialnumber + ' : App Round Closed');
    };
    AlertService.prototype.blocking = function (info) {
        this.sendAlertData('<br> The robot is blocked by an obstacle' +
            '<br> Code alert : 19' +
            info, this.param.serialnumber + ' : Blocking');
    };
    AlertService.prototype.SOS = function (info) {
        this.sendAlertData('<br> Somebody is asking for help' +
            '<br> Code alert : 9' +
            info, this.param.serialnumber + ' : SOS');
    };
    AlertService.prototype.fall = function (info) {
        this.sendAlertData('<br> The robot has detected a fall' +
            '<br> Code alert : 20' +
            info, this.param.serialnumber + ' : Fall detected');
    };
    /////////////////////////////////// Mail client
    AlertService.prototype.SOS_c = function () {
        this.sendAlertClient(this.param.datatext.mailSOS_body, this.param.datatext.mailSOS_suj);
    };
    AlertService.prototype.Battery_c = function () {
        this.sendAlertClient(this.param.datatext.mailBattery_body, this.param.datatext.mailBattery_suj);
    };
    AlertService.prototype.Blocked_c = function (url) {
        this.sendAlertImgClient(url, this.param.datatext.mailBlocked_body, this.param.datatext.mailBlocked_suj);
    };
    AlertService.prototype.Block_c = function () {
        this.sendAlertClient(this.param.datatext.mailBlocked_body, this.param.datatext.mailBlocked_suj);
    };
    AlertService.prototype.Fall_c = function () {
        this.sendAlertClient(this.param.datatext.mailFall_body, this.param.datatext.mailFall_suj);
    };
    AlertService.prototype.PicFall_c = function (url) {
        this.sendAlertImgClient(url, this.param.datatext.mailFall_body, this.param.datatext.mailFall_suj);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('video'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], AlertService.prototype, "videoElement", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('canvas'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */])
    ], AlertService.prototype, "canvas", void 0);
    AlertService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */], __WEBPACK_IMPORTED_MODULE_2__param_service__["a" /* ParamService */], __WEBPACK_IMPORTED_MODULE_3__angular_common_http__["a" /* HttpClient */]])
    ], AlertService);
    return AlertService;
}());

//# sourceMappingURL=alert.service.js.map

/***/ }),

/***/ 350:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 351:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 352:
/***/ (function(module, exports) {

/* (ignored) */

/***/ }),

/***/ 51:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SpeechService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__param_service__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(50);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SpeechService = /** @class */ (function () {
    function SpeechService(httpClient, param) {
        this.httpClient = httpClient;
        this.param = param;
        this.msg = new SpeechSynthesisUtterance();
        this.msg.volume = parseFloat("1");
        this.msg.rate = parseFloat("0.9");
        this.msg.pitch = parseFloat("1");
        this.msg.localService = true;
        this.synth = window.speechSynthesis;
        //this.msg.voice = speechSynthesis.getVoices().filter(function(voice) { return voice.name ; })[10];
    }
    SpeechService.prototype.getVoice = function () {
        var _this = this;
        if (this.param.robot.os == "linux") {
            this.msg.voice = speechSynthesis
                .getVoices()
                .filter(function (voice) {
                return voice.name.includes("pico-generic") && voice.name.includes(_this.param.langage);
            })[0];
        }
        else {
            var voices = speechSynthesis
                .getVoices()
                .filter(function (voice) {
                return voice.localService == true && voice.lang == _this.param.langage;
            });
            if (voices.length > 1) {
                this.msg.voice = voices[1];
            }
            else {
                this.msg.voice = voices[0];
            }
        }
    };
    SpeechService.prototype.getDataWeather = function () {
        var _this = this;
        this.httpClient
            .get("http://api.openweathermap.org/data/2.5/weather?lat=" +
            this.param.gpslat +
            "&lon=" +
            this.param.gpslong +
            "&units=metric&lang=" +
            this.param.weatherlang +
            "&appid=" +
            this.param.weatherapikey)
            .subscribe(function (data) {
            //console.log(data);
            _this.dataweather = data;
            //console.log(this.dataweather.weather[0].description);
            //console.log(Math.round(this.dataweather.main.temp));
            _this.speak(_this.param.datatext.announce3 +
                _this.dataweather.weather[0].description +
                _this.param.datatext.announce4 +
                Math.round(_this.dataweather.main.temp) +
                _this.param.datatext.announce5);
        }, function (err) {
            console.log(err);
        });
    };
    // Create a new utterance for the specified text and add it to
    // the queue.
    SpeechService.prototype.speak = function (text) {
        console.log(text);
        this.synth.cancel();
        this.msg.lang = this.param.langage;
        this.getVoice();
        // Create a new instance of SpeechSynthesisUtterance.
        // Set the text.
        //console.log(this.msg.name);
        this.msg.text = text;
        //console.log(this.msg);
        // Queue this utterance.
        this.synth.speak(this.msg);
    };
    SpeechService = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_common_http__["a" /* HttpClient */], __WEBPACK_IMPORTED_MODULE_0__param_service__["a" /* ParamService */]])
    ], SpeechService);
    return SpeechService;
}());

//# sourceMappingURL=speech.service.js.map

/***/ })

},[238]);
//# sourceMappingURL=main.js.map